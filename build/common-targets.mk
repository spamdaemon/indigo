# these targets generate products
# projects: builds the dependent projects
.PHONY: projects

# build the subprojects that this project depends on
# we need to filter the current project, to avoid recursion in it
# when a module refers to its project as dependency
projects: 
#	$(foreach P,$(filter-out $(PROJECT_DIR)%,$(PROJ_DIRS)),$(MAKE) -C $(P) all;)


