#include <indigo/ShapesEvent.h>


namespace indigo {
    
  ShapesEvent::ShapesEvent (const ::timber::Pointer<Node>& g,Type t, Index i) throws()
  : GfxEvent(g),_type(t),_index(i)
  {}
  
  ShapesEvent::~ShapesEvent() throws() {}
    
}
