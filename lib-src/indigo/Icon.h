#ifndef _INDIGO_ICON_H
#define _INDIGO_ICON_H

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_AFFINETRANSFORM2D_H
#include <indigo/AffineTransform2D.h>
#endif

namespace indigo {

   /**
    * An Icon enables rendering of axis-aligned images. Transformations
    * only apply to the origin of the image, but do not affect the pixels.
    * To render images affected by transformations, a Texture is needed.
    * The local icon coordinate system is always such that the image's top-left
    * corner is (0,0) and positive x-axis is towards the right edge of the image,
    * and the positive y-axis is towards the bottom edge of the image.
    */
   class Icon : public Node
   {
        CANOPY_BOILERPLATE_PREVENT_COPYING(Icon);

         /** The image pointer */
      public:
         typedef ::std::shared_ptr< ::timber::media::ImageBuffer> ImagePtr;

         /**
          * Create an empty image.
          */
      public:
         Icon()throws();

         /**
          * Create an icon with the specified image.
          * @param img an image
          */
      public:
         Icon(ImagePtr img)throws();

         /**
          * Create an icon with the specified image.
          * @param img an image
          * @param t an affine transform to be applied to the image
          */
      public:
         Icon(  ImagePtr img, const AffineTransform2D& t)throws();

         /**  Destroy this graphic */
      public:
         ~Icon()throws();

         /**
          * Set a new image for this icon.
          * @param img an image
          * @return the previous image
          */
      public:
         ImagePtr setImage(ImagePtr img)throws();

         /**
          * Get the icon's image data. The image may under no circumstances
          * be deleted.
          * @return the image or 0 if there is no image.
          */
      public:
         inline ImagePtr image() const throws()
         {  return _image;}

         /**
          * Get the image width.
          * @return the image width in pixels or 0 if no image is set
          */
      public:
         ::timber::UInt width() const throws();

         /**
          * Get the image height
          * @return the image height in pixels
          */
      public:
         ::timber::UInt height() const throws();

         /**
          * Set a local affine transform. When rendering the image, this transform is applied
          * first and then another affine transform is applied that move the image to the
          * appropriate location on the screen.
          * @note Some renderers may only support a subset of the affine transform.
          * @param t an affine transform
          */
      public:
         void setTransform(const AffineTransform2D& t)throws();

         /**
          * Get the current transform for the image.
          * @return the current image transform
          */
      public:
         inline const AffineTransform2D& transform() const throws()
         {  return _transform;}

         /**
          * Accept the specified visitor. Calls
          * the Node::acceptVisitor() method, followed
          * by dispatchVisitor();
          * @param v a visitor.
          */
      protected:
         void acceptVisitor(GfxVisitor& v) const throws();

         /** The image */
      private:
         ImagePtr _image;

         /** The optional affine transform */
      private:
         AffineTransform2D _transform;
   };
}
#endif
