#include <indigo/AntiAliasingModeNode.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

   AntiAliasingModeNode::AntiAliasingModeNode()
   throws() : _aaMode(AntiAliasingMode::NONE)
   {}

   AntiAliasingModeNode::AntiAliasingModeNode(AntiAliasingMode mode)
   throws()
   : _aaMode(mode)
   {}

   void AntiAliasingModeNode::setAntiAliasingMode(AntiAliasingMode newMode)
   throws()
   {
      if (_aaMode!=newMode) {
         _aaMode = newMode;
         notifyChange();
      }
   }

   bool AntiAliasingModeNode::equals(const AntiAliasingModeNode& m) const
   throws()
   {
      return _aaMode==m._aaMode;
   }

   Int AntiAliasingModeNode::hashValue() const
   throws()
   {  return static_cast<Int>(_aaMode);}

   AntiAliasingModeNode& AntiAliasingModeNode::operator=(const AntiAliasingModeNode& n)
   throws()
   {
      if (_aaMode!=n._aaMode) {
         _aaMode=n._aaMode;
         notifyChange();
      }
      return *this;
   }

   void AntiAliasingModeNode::acceptVisitor(GfxVisitor& v) const
throws()
{  v.visit(*this);}

}
