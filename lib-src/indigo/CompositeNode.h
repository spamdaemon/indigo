#ifndef _INDIGO_COMPOSITENODE_H
#define _INDIGO_COMPOSITENODE_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_GFXEVENT_H
#include <indigo/GfxEvent.h>
#endif

#include <vector>

namespace indigo {
   /**
    * This class is the base class for composite graphics.
    * It provides methods to insert child graphics and set
    * child graphics;
    */
   class CompositeNode : public Node
   {
         CompositeNode&operator=(const CompositeNode&);

         /**
          * A composite event.
          */
      public:
         class CompositeEvent : public GfxEvent
         {
               /** The event id */
            public:
               enum ID
               {
                  CLEARED, //< all children have been removed
                  REMOVED, //< a single child has been removed
                  ADDED, //< a single child has been added
                  PACKED,
               //< the node has been packed
               };

               /** Destroy this event */
            public:
               ~CompositeEvent()throws();

               /**
                * Create a new event for hierarchy changes.
                * @param i the type of event
                * @param p the composite parent
                * @param c the child that was involved
                * @param pos the position associated with the child
                */
            public:
               inline CompositeEvent(ID i, const ::timber::Pointer< Node>& p, const ::timber::Pointer< Node>& c,
                     size_t pos)throws()
               : GfxEvent(p), _id(i), _child(c), _pos(pos) {}

               /**
                * Create a new event for hierarchy changes.
                * @param i the type of event
                * @param p the composite parent
                */
            public:
               inline CompositeEvent(ID i, const ::timber::Pointer< Node>& p)throws()
               : GfxEvent(p), _id(i), _pos(invalidIndex()) {}

               /**
                * Get the event id.
                * @return the event id
                */
            public:
               inline ID id() const throws() {return _id;}

               /**
                * Get the object was added or removed. It can return 0 for
                * some types of events.
                * @return the object that was added or removed
                */
            public:
               inline ::timber::Pointer< Node> child() const throws() {return _child;}

               /**
                * Get the position of the child
                * @return the position of the child
                */
            public:
               inline size_t position() const throws() {return _pos;}

               /** The event id */
            private:
               ID _id;

               /** The child involved */
            private:
               ::timber::Pointer< Node> _child;

               /** The position of the child */
            private:
               size_t _pos;
         };

         /** Default constructor to create a non-separator node. */
      protected:
         inline CompositeNode()throws()
         : _separator(false)
         {}

         /**
          * Create a composite node.
          * @param separator true if the node is a separator node
          */
      protected:
         inline CompositeNode(bool separator)throws()
         : _separator(separator)
         {}

         /**
          * Create a composite node with the specified children.
          * @param l the left child
          * @param m the middle child
          * @param r the right child
          * @param separator true if the node is a separator node
          */
      protected:
         CompositeNode(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& m,
               const ::timber::Pointer< Node>& r, bool separator)throws();

         /**
          * Create a composite node with the specified children.
          * @param l the left child
          * @param r the right child
          * @param separator true if the node is a separator node
          */
      protected:
         CompositeNode(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& r, bool separator)throws();

         /**
          * Constructor that pre-allocates space.
          * @param n number of children to pre-allocate
          * @param separator true to make the node a separator node
          */
      protected:
         CompositeNode(size_t n, bool separator)throws();

         /**
          * Copy constructor that copies the handle,
          * but keeps the same implementation.
          */
      protected:
         CompositeNode(const CompositeNode& g)throws();

         /**  Destroy this node handle */
      public:
         ~CompositeNode()throws();

         /**
          * Test if this is a separator group. If this
          * is a separator group, then renderering the group
          * must preserve the current graphics state.
          * @return true if this is a separator group
          */
      public:
         inline bool isSeparator() const throws()
         {  return _separator;}

         /**
          * Get the number of child nodes.
          * @return the number of child nodes that not null
          */
      public:
         inline size_t nonNullNodeCount() const throws()
         {  return _nonNullChildren;}

         /**
          * Get the number of child nodes including null children.
          * @return the number of child nodes
          */
      public:
         inline size_t nodeCount() const throws()
         {  return _children.size();}

         /**
          * Test if the there is good child at the specified position.
          * @param pos a node position
          * @return true if there <code>node(pos)</code> would return non-zero.
          */
      public:
         inline bool hasNode(size_t pos) const throws()
         {
            return
            pos< _children.size()
            && _children[pos]!=nullptr;
         }

         /**
          * Get the specified graphic as a read-only handle.
          * @param pos a graphic position
          * @return the graphic at the specified position or 0
          * if there isn't one
          */
      public:
         inline ::timber::Pointer< Node> getNode(size_t pos) const throws()
         {  return ::timber::Pointer<Node>(_children[pos]);}

         /**
          * Get the index of the specified graphic.
          * @param g a graphic
          * @return the position of the node or invalidIndex()
          */
      public:
         size_t indexOf(const ::timber::Pointer< Node>& g) const throws();

         /**
          * Get the specified child.
          * @param pos a child
          * @return the child at the specified position or 0
          * if there isn't one
          */
      protected:
         inline ::timber::Pointer< Node> childAt(size_t pos) const throws()
         {  return _children[pos];}

         /**
          * Set the child at the specified position. If the new child
          * is 0, then the current child is only erased, and the
          * children next to it are not shifted. Calls fireEvent()
          * if a change was made.
          * @param g the new node or 0
          * @param pos a position
          * @pre REQUIRE_RANGE(pos,0,nodeCount())
          */
      protected:
         void setChildAt(const ::timber::Pointer< Node>& g, size_t pos)throws();

         /**
          * Insert a child at the specified position. Calls fireEvent()
          * if a change was made.
          * @param g the new graphic or 0
          * @param pos a position
          * @pre REQUIRE_RANGE(pos,0,nodeCount())
          */
      protected:
         void insertChildAt(const ::timber::Pointer< Node>& g, size_t pos)throws();

         /**
          * Add a child. Calls fireEvent().
          * @param g a new graphic
          */
      protected:
         inline void addChild(const ::timber::Pointer< Node>& g)throws()
         {  setChildAt(g,nodeCount());}

         /**
          * Remove the specified child. Calls fireEvent().
          * @param g the child to be removed
          * @return true if the child was removed, false if it was  not found
          */
      protected:
         bool removeChild(const ::timber::Pointer< Node>& g)throws();

         /**
          * Remove the child at the specified position. Calls fireEvent().
          * @param pos a position
          * @pre REQUIRE_RANGE(pos,0,nodeCount()-1)
          * @return the child at the position
          */
      protected:
         ::timber::Pointer< Node> removeChildAt(size_t pos)throws();

         /**
          * Remove all children.
          */
      protected:
         void removeChildren()throws();

         /**
          * Accept the specified visitor. Calls
          * the Node::acceptVisitor() method, followed
          * by dispatchVisitor();
          * @param v a visitor.
          */
      protected:
         void acceptVisitor(GfxVisitor& v) const throws();

         /**
          * Pack this composite. Removes all positions
          * where there are no children.
          */
      protected:
         void packChildren()throws();

      protected:
         void attachToSceneGraph(SceneGraph& sg)throws();
         void detachFromSceneGraph(SceneGraph& sg)throws();

         /** The children */
      private:
         ::std::vector< ::timber::Pointer< Node> > _children;

         /** The number of non-null children */
      private:
         size_t _nonNullChildren;

         /** True if this is a separator group */
      private:
         const bool _separator;
   };
}
#endif
