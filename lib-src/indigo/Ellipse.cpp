#include <indigo/Ellipse.h>

namespace indigo {
  
  Ellipse::Ellipse () throws()
  : _center(0,0),_major(1),_minor(1),_theta(0)
  {}
    
  Ellipse::Ellipse (const Point& c, double r1, double r2, double inclination) throws()
  : _center(c)
  {
    assert(r1>=0.0);
    assert(r2>=0.0);

    const double PI_2 = 1.57079632679489661923;
   if (r1>r2) {
      _major = r1;
      _minor = r2;
      _theta = inclination;
    }
    else if (r1<r2) {
      _major = r2;
      _minor = r1;

      // rotate by 90 degrees
      _theta = inclination + PI_2;
    }
    else {
      _major = _minor = r1;
      _theta = 0;
    }

    const double PI = 3.14159265358979323846;
    
    // normalize the angle to between -90;90 degrees
    while(_theta > PI_2) {
      _theta -= PI;
    }
    while(_theta < -PI_2) {
      _theta += PI;
    }
  }
  
  Ellipse Ellipse::createCovarianceEllipse(const Point& c, double xx, double yy, double xy, double stddev) throws(::std::exception)
  {
    const double det = xx*yy - xy*xy;

    if (det <= 0.0) {
      throw ::std::invalid_argument("Degenerate covariance");
    }

    double theta = ::std::atan2((2.0*xy),(xx-yy))/2.0;
    double cosTheta = ::std::cos(theta);
    double sinTheta = ::std::sin(theta);
    double cos2Theta = cosTheta*cosTheta;
    double sin2Theta = sinTheta*sinTheta;
    double sincosTheta = 2*sinTheta*cosTheta;

    // compute the major and minor axes
    double p1 = det/(yy * cos2Theta - xy*sincosTheta + xx*sin2Theta);
    double p2 = det/(yy * sin2Theta + xy*sincosTheta + xx*cos2Theta); 

    return Ellipse(c,stddev*::std::sqrt(p1),stddev*::std::sqrt(p2),theta);
  }

  
}
