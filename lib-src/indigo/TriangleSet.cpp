#include <indigo/TriangleSet.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  TriangleSet::TriangleSet (const Point& a, const Point& b, const Point& c) throws()
  : Shapes<Triangles>(Triangles(a,b,c)) {}
  
  TriangleSet::TriangleSet (const Triangles& p) throws()
  : Shapes<Triangles>(p) {}
  
  TriangleSet::TriangleSet (const TriangleSet& g) throws()
  : Shapes<Triangles>(g) {}
  
  TriangleSet::~TriangleSet () throws()
  {}
  
  void TriangleSet::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
