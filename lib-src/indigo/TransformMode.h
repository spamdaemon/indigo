#ifndef _INDIGO_TRANSFORMMODE_H
#define _INDIGO_TRANSFORMMODE_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

namespace indigo {

  /**
   * The transform mode defines whether or not transforms
   * following this node are to be concatenated or treated
   * as absolute transforms.
   */
  class TransformMode : public StateNode {
    TransformMode&operator=(const TransformMode&);
    TransformMode(const TransformMode&);

    /** The transform modes */
  public:
    enum Mode {
      CONCATENATE, ///< concatenate transforms from now on
      RESET        ///< reset the transforms from now on
    };

    /**
     * Default constructor. Creates a transform mode with FILL.
     */
  public:
    inline TransformMode () throws()
    {}
	
    /** 
     * Create an initialized transform.
     * @param m the transform mode 
     */
  public:
    inline TransformMode (Mode m) throws()
      : _mode(m) {}
      
    /**  Destroy this graphic handle */
  public:
    ~TransformMode () throws();

    /**
     * Get the transform mode.
     * @return the current transform render mode
     */
  public:
    inline Mode mode() const throws() { return _mode; }
      
    /**
     * Set the transform mode.
     * @param m the transform mode 
     */
  public:
    void setMode (Mode m) throws();
      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();

    /** The transform mode */
  private:
    Mode _mode;
  };
}
#endif
