#include <indigo/Coordinates3D.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  Coordinates3D::Coordinates3D (double x, double y) throws()
  { 
    _points.reserve(1);
    _points.push_back(Point(x,y)); 
  }
    
  Coordinates3D::Coordinates3D (const Point& sh) throws()
  { 
    _points.reserve(1);
    _points.push_back(Point(sh.x(),sh.y())); 
  }
    
  Coordinates3D::Coordinates3D (const Coordinates3D& g) throws()
  : Node(),_points(g._points) {}
    
  Coordinates3D::~Coordinates3D () throws()
  {}

  void Coordinates3D::set (const Point& p, size_t pos) throws()
  {
    assert(pos <= count());
    if (pos==count()) {
      _points.push_back(p);
    }
    else {
      _points[pos] = p;
    }
    notifyChanged(pos);
  }

  void Coordinates3D::insert (const Point& p, size_t pos) throws()
  {
    assert(pos <= count());
    if (pos==count()) {
      _points.push_back(p);
    }
    else {
      _points.insert(_points.begin()+pos,p);
    }
    notifyChanged(pos);
  }

  Point Coordinates3D::remove (size_t pos) throws()
  {
    Point p(point(pos));
    _points.erase(_points.begin()+pos);
    notifyChanged(pos);
    return p;
  }

  void Coordinates3D::clear () throws()
  {
    if (count()>0) {
      _points.clear();
      notifyChange();
    }
  }

  void Coordinates3D::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
