#ifndef _INDIGO_REGIONS_H
#define _INDIGO_REGIONS_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _TIKAL_REGION2D_H
#include <tikal/Region2D.h>
#endif

#include <vector>

namespace indigo {
  
  /**
   * This graphic is a region container. All regions
   * in this container a rendered with the same attributes.
   */
  class Regions : public Shapes< ::timber::Pointer< ::tikal::Region2D> >  {
    Regions&operator=(const Regions&);

    /** Default constructor */
  public:
    inline Regions () throws()
    {}
    
    /**
     * Create a regions object with a single region.
     * @param r a region or null
     */
  public:
    Regions (const ::timber::Reference< ::tikal::Region2D>& r) throws();
    
    /**
     * Create Regions with may regions.
     * @param regs a vector regions
     */
  public:
    Regions (const ::std::vector< ::timber::Reference< ::tikal::Region2D> >& regs) throws();
    
    /**
     * Create Regions with may regions.
     * @param regs a vector regions
     */
  public:
    Regions (const ::std::vector< ::timber::Pointer< ::tikal::Region2D> >& regs) throws();
    
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  private:
    Regions (const Regions& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~Regions () throws();
      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
