#include <indigo/Coordinates2D.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

  Coordinates2D::Coordinates2D (double x, double y) throws()
  { 
    _points.reserve(1);
    _points.push_back(Pt(x,y)); 
  }
	
  Coordinates2D::Coordinates2D (const Point& sh) throws()
  { 
    _points.reserve(1);
    _points.push_back(Pt(sh.x(),sh.y())); 
  }
  
  Coordinates2D::Coordinates2D (const Coordinates2D& g) throws()
  : StateNode(),_points(g._points) {}
  
  Coordinates2D::~Coordinates2D () throws()
  {}
  
  void Coordinates2D::set (const Point& p, size_t pos) throws()
  {
    assert(pos<=count());
    if (pos==count()) {
      _points.push_back(Pt(p.x(),p.y()));
    }
    else {
      _points[pos] = Pt(p.x(),p.y());
    }
    notifyChanged(pos);
  }

  void Coordinates2D::insert (const Point& p, size_t pos) throws()
  {
    assert(pos<=count());
    if (pos==count()) {
      _points.push_back(Pt(p.x(),p.y()));
    }
    else {
      _points.insert(_points.begin()+pos,Pt(p.x(),p.y()));
    }
    notifyChanged(pos);
  }
  Point Coordinates2D::remove (size_t pos) throws()
  {
    Point p(point(pos));
    _points.erase(_points.begin()+pos);
    notifyChanged(pos);
    return p;
  }

  void Coordinates2D::clear () throws()
  {
    if (count()>0) {
      _points.clear();
      notifyChange();
    }
  }

  void Coordinates2D::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
