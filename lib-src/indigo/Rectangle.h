#ifndef _INDIGO_RECTANGLE_H
#define _INDIGO_RECTANGLE_H

#ifndef _INDIGO_
#include <indigo/indigo.h>
#endif

namespace indigo {
  /**
   * This class is used to define a rectangle in terms
   * of its lower-left corner and its width and height.
   *
   */
  class Rectangle {
    /**
     * Default constructor. Creates rectangle 0,0,0,0
     */
  public:
    constexpr inline Rectangle () throws()
      : _x(0),_y(0),_w(0),_h(0) {}
      
    /**
     * Create a new rectangle object.
     * @param px the x coordinate of the lower-left corner
     * @param py the y coordinate of the lower-left corner
     * @param w the width
     * @param h the height
     */
  public:
    constexpr inline Rectangle (double px, double py, double w, double h) throws()
      : _x(px),_y(py), _w(w), _h(h) {}
      
    /**
     * Create a new rectangle object (0,0,w,h)
     * 
     */
  public:
    constexpr inline Rectangle (double w, double h) throws()
      : _x(0),_y(0),_w(w),_h(h) {}

    /**
     * Get the x location.
     * @return the x coordinate of the position
     */
  public:
    inline double x() const throws() { return _x; }

    /**
     * Get the y location.
     * @return the y coordinate of the position
     */
  public:
    inline double y() const throws() { return _y; }
      
    /**
     * Get the width 
     * @return the width
     */
  public:
    inline double width() const throws() { return _w; }

    /**
     * Get the height.
     * @return the height
     */
  public:
    inline double height () const throws() { return _h; }

    /**
     * Compute a hashvalue.
     * @return a hashvalue
     */
  public:
    inline Int hashValue () const throws()
    { return hash(_x) ^ hash(_y) ^ hash(_w) ^ hash(_h); }
      
    /**
     * Compare two rectangle.
     * @param b a rectangle object
     * @return true if this and b are equal
     */
  public:
    inline bool equals(const Rectangle& b) const throws()
    { return _x==b._x && _y==b._y && _w==b._w && _h==b._h; }
      
    /**
     * Compare two rectangle.
     * @param b a rectangle object
     * @return true if this and b are equal
     */
  public:
    inline bool operator==(const Rectangle& b) const throws()
    { return equals(b); }
      
    /**
     * Compare two rectangle.
     * @param b a rectangle object
     * @return true if this and b are equal
     */
  public:
    inline bool operator!=(const Rectangle& b) const throws()
    { return !equals(b); }

    /** The top-left corner and width and height */
  private:
    double _x,_y,_w,_h;
  };
}

#endif
