#ifndef _INDIGO_RASTER_H
#define _INDIGO_RASTER_H

#ifndef _TIMBER_MEDIA_IMAGEBUFFER_H
#include <timber/media/ImageBuffer.h>
#endif

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

namespace indigo {

   /**
    * A raster is an image whose corners and edges stretch over an oriented rectangle in world-coordinates. Thus,
    * the model view and local transforms affect the appearance of the raster.
    * @see ::indigo::Icon
    */
   class Raster : public Node
   {
         CANOPY_BOILERPLATE_PREVENT_COPYING(Raster);


         /** The image pointer */
      public:
         typedef ::std::shared_ptr< ::timber::media::ImageBuffer> ImagePtr;

         /**
          * Create an empty image.
          */
      public:
         Raster()throws();

         /**
          * Create an raster with the specified image.
          * @param img an image
          */
      public:
         Raster(ImagePtr img)throws();

         /**
          * Create an raster with the specified image.
          * @param img an image
          * @param tl the top-left corner point
          * @param tr the top-right corner point
          * @param bl the bottom-left corner point
          */
      public:
         Raster(ImagePtr img, const Point& tl, const Point& tr, const Point& bl)throws();

         /**  Destroy this graphic */
      public:
         ~Raster()throws();

         /**
          * Set a new image for this raster.
          * @param img an image
          * @return the previous image
          */
      public:
         ImagePtr setImage(ImagePtr img)throws();

         /**
          * Get the raster's image data. The image may under no circumstances
          * be deleted.
          * @return the image or 0 if there is no image.
          */
      public:
         inline ImagePtr image() const throws()
         {  return _image;}

         /**
          * Set a new set of corners.
          * @param tl the top-left corner point
          * @param tr the top-right corner point
          * @param bl the bottom-left corner point
          */
      public:
         void setPoints (const Point& tl, const Point& tr, const Point& bl) throws();

        /**
          * Get the top-left corner point.
          * @return the top-left corner point
          */
      public:
         const Point& topLeft() const throws() {return _corners[0];}

         /**
          * Get the top-left corner point.
          * @return the top-left corner point
          */
      public:
         const Point& topRight() const throws() {return _corners[1];}

         /**
          * Get the top-left corner point.
          * @return the top-left corner point
          */
      public:
         const Point& bottomRight() const throws() {return _corners[2];}

         /**
          * Get the top-left corner point.
          * @return the top-left corner point
          */
      public:
         const Point& bottomLeft() const throws() {return _corners[3];}

         /**
          * Accept the specified visitor. Calls
          * the Node::acceptVisitor() method, followed
          * by dispatchVisitor();
          * @param v a visitor.
          */
      protected:
         void acceptVisitor(GfxVisitor& v) const throws();

         /** The image */
      private:
         ImagePtr _image;

         /**
          * The four corner points (cw, starting with top-left corner) in model coordinates
          */
      private:
         Point _corners[4];
   };
}
#endif
