#include <indigo/ViewTransform2D.h>
#include <indigo/GfxVisitor.h>


namespace indigo {
  
  ViewTransform2D::~ViewTransform2D () throws()
  {}
  void ViewTransform2D::setTransform (const AffineTransform2D& t) throws(::std::exception)
  {
    try {
      t.inverse(); 
    }
    catch (const ::std::exception& e) {
      throw ::std::invalid_argument("ViewTransform must be invertible");
    }
    
    _transform = t;
    notifyChange();
  }
  
  Point ViewTransform2D::transformPoint (const Point& pt) const throws()
  { return _transform.transformPoint(pt); }
  
  void ViewTransform2D::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
