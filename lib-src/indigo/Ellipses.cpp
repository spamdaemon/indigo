#include <indigo/Ellipses.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  Ellipses::Ellipses (const Point& c, double r1, double r2, double inclination) throws()
  : Shapes<Ellipse>(Ellipse(c,r1,r2,inclination)) {}
  
  Ellipses::Ellipses (const Ellipse& p) throws()
  : Shapes<Ellipse>(p) {}
      
  Ellipses::Ellipses (const Ellipses& g) throws()
  : Shapes<Ellipse>(g) {}
  
  Ellipses::~Ellipses () throws()
  {}
  
  void Ellipses::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
