#ifndef _INDIGO_SWITCH_H
#define _INDIGO_SWITCH_H

#ifndef _INDIGO_GROUP_H
#include <indigo/Group.h>
#endif

namespace indigo {

  /**
   * This class is a special kind of group, where only one
   * child is visible at any time.
   * 
   * @date 31 Jul 2003
   */
  class Switch : public Group {
    Switch(const Switch&);
    Switch&operator=(const Switch&);
    
    /** Default constructor */
  public:
    Switch () throws();

    /** 
     * Constructor that pre-allocates space.
     * @param n number of children to pre-allocate
     * @param separator true if this switch is a separator switch
     */
  public:
    Switch (size_t n, bool separator) throws();

    /**  Destroy this graphic */
  public:
    ~Switch () throws();

    /**
     * Set the index of the child that is to be visible.
     * It is not required that the child index be valid
     * @param child an index of a child or an invalidIndex
     */
  public:
    virtual void setVisibleChild (size_t child) throws();

    /**
     * Get the currently visible child.
     * @return the index of the currently visible child
     * @note the result <em>must</em> be checked for invalidIndex()
     */
  public:
    inline size_t visibleChild () const throws()
    { return _visibleChild; }

    /**
     * Get the visible child node.
     * @return the child that is currently visible or null
     */
  public:
    ::timber::Pointer<Node> visibleNode() const throws();

    /**
     * Accept the specified visitor. Calls
     * the Node::acceptVisitor() method, followed
     * by dispatchVisitor();
     * @param v a visitor.
     */
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();

    /** True if this is a separator switch */
  private:
    size_t _visibleChild;
	
  };
}
#endif
