#ifndef _INDIGO_VIEW_VIEWPORT_H
#define _INDIGO_VIEW_VIEWPORT_H

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#ifndef _INDIGO_AFFINETRANSFORM2D_H
#include <indigo/AffineTransform2D.h>
#endif

#ifndef _INDIGO_VIEW_PIXEL_H
#include <indigo/view/Pixel.h>
#endif

#ifndef _INDIGO_VIEW_SIZE_H
#include <indigo/view/Size.h>
#endif

#ifndef _INDIGO_VIEW_BOUNDS_H
#include <indigo/view/Bounds.h>
#endif

#include <cstdint>

namespace indigo {
   namespace view {
      /**
       * The viewport is a glimpse of an infinite plane (referred to as the model) as seen through a rectangular window (referred to as the view)
       * that can be oriented arbitrarily on that plane. The window is simple represented by Size object and the
       * visibile region of the infinite plane is represented by a Bounds object. The class Point is used to refer to points in the model space,
       * and pixel is used to refer to points in the view, even though both use floating point numbers.<p>
       * No assumptions are made about the coordinate frame, other than that it be Cartesian.
       */
      class ViewPort
      {
            /** A point in model space */
         public:
            typedef ::indigo::Point Point;

            /** A point in view space */
         public:
            typedef ::indigo::view::Pixel Pixel;

            /** The view window */
         public:
            typedef ::indigo::view::Size Window;

            /** The bounds */
         public:
            typedef ::indigo::view::Bounds Bounds;

            /**
             * Create a viewport of size 1
             */
         public:
            ViewPort()throws();

            /**
             * Create a viewport with the given bounds and window.
             * @param bounds the bounds for this viewport.
             * @param view the view window
             * @throws invalid_argument if a scale cannot be computed.
             */
         public:
            ViewPort(const Bounds& bounds, const Window& view)
            throws ( ::std::invalid_argument);

            /** Destructor */
         public:
            virtual ~ViewPort()throws();

            /**
             * Copy operator. This method invokes <tt>setViewPort()</tt> to inform subclassers
             * that the view has been modified.
             * @param src another viewport
             * @return *this
             */
         protected:
            ViewPort& operator=(const ViewPort& src)throws();

            /**
             * Set the new bounds and size for this viewport. If overriden, subclasses must still
             * invoke this method. If an exception is thrown, then the viewport remains unchanged.
             * @param newBounds the new bounds for the visible region.s
             * @param newView the new view window
             * @throws ::std::invalid_argument if the a scale cannot be computed
             */
         protected:
            virtual void setViewPort(const Bounds& newBounds, const Window& newView)
            throws ( ::std::invalid_argument);

            /**
             * Set new bounds, but keep the current view window. This method affects the scale if the
             * size of the bounds are changed.
             * @param newBounds the new bounds
             * @throws ::std::invalid_argument if the a scale cannot be computed
             */
         public:
            inline void setBounds(const Bounds& newBounds)throws(::std::invalid_argument)
            {  setViewPort(newBounds,_window);
         }

      /**
       * Set a new view window, but keep the current bounds. This method primarily affects the scale.
       * @param newWindow the new view window
       * @throws ::std::invalid_argument if the a scale cannot be computed
       */
protected      :
      inline void setViewWindow (const Window& newWindow) throws(::std::invalid_argument)
      {  setViewPort(_bounds,newWindow);}

      /**
       * Get the bounds of the region in the graphics plane that is currently visible in this viewport.
       * @return the bounds of the visible region
       */
      public:
      const Bounds& bounds() const throws() {return _bounds;}

      /**
       * Get the current view window.
       * @return the view window
       */
      public:
      const Window& viewWindow() const throws() {return _window;}

      /**
       * Determine if the specified bounds and window makes a valid viewport. Subclasses may override this
       * method and provide restrict the sets of valid Bounds and Windows.
       * @param bounds a bounds object
       * @param window a view window
       * @return true if a viewport can be created without throwing an exception
       */
      public:
      virtual bool checkViewPort (const Bounds& newBounds, const Window& newView) const throws();

      /**
       * @name PointTransformations
       * By overriding these methods it possible to change the mapping between the model space and the view space.
       * @{
       */

      /**
       * Get the transform that maps points in the model's infinite plane into a coordinates relative
       * to frame defined by view window.
       * @return an transform from model space to view space
       */
      public:
      virtual AffineTransform2D getModelViewTransform() const throws();

      /**
       * Get the transform that maps points in the view window into the corresponding point in the model's infinite plane.<p>
       * The return transform is the inverse of getModelViewTransform()
       * @return an transform from view space to model space
       */
      public:
      virtual AffineTransform2D getViewModelTransform() const throws();

      /**
       * Transform the specified viewport pixel into a point.
       * @param px x-coordinate of a pixel
       * @param py y-coordinate of a pixel
       * @return a point
       */
      protected:
      virtual void pixel2point(double px, double py, double& outx, double& outy) const throws();

      /**
       * Transform the specified viewport pixel into a point.
       * @param px x-coordinate of a pixel
       * @param py y-coordinate of a pixel
       * @return a point
       */
      protected:
      virtual void point2pixel(double px, double py, double& outx, double& outy) const throws();

      /**
       * Transform the specified point into a pixel.
       * @param x x-coordinate of a point
       * @param y y-coordinate of a point
       * @return a pixel
       */
      public:
      inline Pixel getPixel(double x, double y) const throws()
      {
         double px,py;
         point2pixel(x,y,px,py);
         return Pixel(px,py);
      }

      /**
       * Transform the specified viewport pixel into a point.
       * @param px x-coordinate of a pixel
       * @param py y-coordinate of a pixel
       * @return a point
       */
      public:
      inline Point getPoint(double px, double py) const throws()
      {
         double x,y;
         pixel2point(px,py,x,y);
         return Point(x,y);
      }

      /**
       * Transform the specified viewport pixel into a point.
       * @param p a pixel
       * @return a point
       */
      public:
      inline Point getPoint(const Pixel& p) const throws()
      {  return getPoint(p.x(),p.y());}

      /**
       * Transform the specified point into a pixel.
       * @param p a point
       * @return a pixel
       */
      public:
      inline Pixel getPixel(const Point& p) const throws()
      {  return getPixel(p.x(),p.y());}

      /*@} */

      /**
       * @name ViewportProperties
       * @{
       */
      /**
       * Get the scale in x.
       * @return the <tt>size().width()/width()</tt>
       */
      public:
      double horizontalScale() const throws();

      /**
       * Get the scale in y
       * @return the <tt>size().height()/height()</tt>
       */
      public:
      double verticalScale() const throws();

      /**
       * @name ViewableGraphics.
       * The rectangle in the graphics plane that is visible through this viewport.
       * @{
       */

      /**
       * Get the point at the center of the viewport bounds.
       * @return the point at the center of the bounds.
       */
      public:
      virtual Point centerPoint() const throws();

      /**
       * Get the pixel at the center at the view window.
       * @return the pixel at the center of the view window
       */
      public:
      virtual Pixel centerPixel() const throws();

      /*@}*/

      /**
       * @name ViewWindowOperations
       * Operations that modify the view window and/or bounds.
       * @{
       */

      /**
       * Resize the view window in such a way as to keep the scale constant. This means that the bounds are adjusted as needed.
       * @param left the number of units to pull the view window to the left.
       * @param right the number of units to pull the view window to the left.
       * @param down the number of units to pull the view window down.
       * @param up the number of units to pull the view window to up
       * @throw invalid_argument if an invalid viewport would result
       */
      protected:
      virtual void resizeViewWindow(double left, double right, double down, double up) throws ( ::std::invalid_argument);

      /**
       * Resize the bounds such that the horizontal and vertical scales remain the same. This means that the size of the
       * window will need to be adjusted as well.
       *
       * @param left the number of units to pull the bounds locally to the left.
       * @param right the number of units to pull the bounds locally to the left.
       * @param down the number of units to pull the bounds locally down.
       * @param up the number of units to pull the bounds locally to up
       * @throw invalid_argument if an invalid viewport would result
       */
      protected:
      virtual void resizeBounds(double left, double right, double down, double up) throws ( ::std::invalid_argument);

      /*@}*/

      /**
       * @name IncrementalViewModifications.
       * Scale, rotate, and translate the visible area in the graphics plane. The size of the
       * viewport itself remains constant, but the visible area in the graphics plane is changed.
       * @{
       */

      /**
       * Translate the bounds until the specified point appears at the given pixel.
       * @param p a point
       * @param px a pixel
       * @throws ::std::invalid_argument if an invalid viewport would result
       */
      public:
      virtual void movePointToPixel(const Point& p, const Pixel& px)throws(::std::invalid_argument);

      /**
       * Translate the view window by a specific amount in x and y. This method moves the bounds by
       * an appropriate amount given the current scale.
       * @param dx the change in x
       * @param dy the change in y
       * @throws ::std::invalid_argument if an invalid viewport would result
       */
      public:
      virtual void translateViewWindow(double dx, double dy)throws(::std::invalid_argument);

      /**
       * Translate the bounds within its local coordinate frame.
       * @param dx the change in x
       * @param dy the change in y
       * @throws ::std::invalid_argument if an invalid viewport would result
       */
      public:
      virtual void translateBounds(double dx, double dy)throws(::std::invalid_argument);

      /**
       * Rotate the bounds around the specified point. <p>
       * The scale is such that before and after the operation, the point p maps into the same pixel.
       * @param phi the angle (in radians) around which to rotate
       * @param p the center of the rotation.
       * @throws ::std::invalid_argument if an invalid viewport would result
       */
      public:
      virtual void rotateBounds (double phi, Point p)throws(::std::invalid_argument);

      /**
       * Scale the bounds around the specified point. This method does not change the view window, which means the
       * scale is modified. A scale of <tt>s</tt> in either direction affects the viewport's corresponding scale value
       * by <tt>1/s</tt>. For example, increasing the size of the bounds by 2 reduces the scale by 0.5.<p>
       * The scale is such that before and after the operation, the point p maps into the same pixel.
       * @param sx the scale in x
       * @param sy the scale in y
       * @param p the center of the scale
       * @throws ::std::invalid_argument if an invalid viewport would result
       */
      public:
      virtual void scaleBounds (double sx, double sy, Point p)throws(::std::invalid_argument);
      /*@}*/

      /**
       * Resize the bounds to achieve the specified scale values.<p>
       * The scale is such that before and after the operation, the point p maps into the same pixel.
       * @param sx the desired horizontal scale value
       * @param sy the desired vertical scale value
       * @param p a point around which to adjust the bounds.
       */
      public:
      virtual void resizeBoundsToScale (double sx, double sy, Point p) throws(::std::invalid_argument);

      /** The size of the viewport in pixels */
      private:
      Window _window;

      /** The bounds */
      private:
      Bounds _bounds;

   };

}
}
/**
 * Print a string representation of a given pixel.
 * @param out an output stream
 * @param vp a view port
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::ViewPort& vp)
throws();

#endif
