#ifndef _INDIGO_VIEW_IPIXEL_H
#define _INDIGO_VIEW_IPIXEL_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#ifndef _INDIGO_VIEW_PIXEL_H
#include <indigo/view/Pixel.h>
#endif

#include <cmath>
#include <cstdint>

namespace indigo {
   namespace view {

      /**
       * A pixel is a point on a 2D rendering surface. A pixel is equivalent to a point. Normally, a pixel
       * is specified in integer coordinates, but the mapping from floating point coordinates to integer
       * coordinates may depend on an application.
       *
       */
      class IPixel
      {
            /** Create the pixel 0,0 */
         public:
            inline IPixel()throws()
            : _x(0),_y(0)
            {}

           /**
             * Create a new pixel.
             * @param fx x-coordinate of the pixel
             * @param fy y-coordinate of the pixel
             */
         public:
            inline IPixel(int32_t fx, int32_t fy) throws()
            :_x(fx),_y(fy)
            {}

            /**
             * Create an IPixel from a pixel.
             * This is not a cheap rounding operation as it may require applying proper rounding.
             * @param p a pixel
             */
         public:
            static IPixel fromPixel(const Pixel& p) throws();

            /**
             * Convert this pixel back into a normal floating point pixel.
             * @return a pixel
             */
         public:
            Pixel toPixel () const throws();

            /**
             * Get the x coordinate
             * @return the x-coordinate
             */
         public:
            inline int32_t x() const throws() {return _x;}

            /**
             * Get the y coordinate
             * @return the y-coordinate
             */
         public:
            inline int32_t y() const throws() {return _y;}

            /**
             * Compute the square of the distance between two pixels.
             * @param p a pixel
             * @return square of the distance between the two pixels
             */
         public:
            double sqrDistance(const IPixel& p) const throws();

            /**
             * Compute the distance between two pixels.
             * @param p a pixel
             * @return square of the distance between two pixels.
             */
         public:
            inline double distanceTo(const IPixel& p) const throws()
            {  return ::std::sqrt(sqrDistance(p));}

            /**
             * Compare two pixels.
             * @param pt a pixel
             * @return true if the pixel coordinates are exactly the same
             */
         public:
            bool equals(const IPixel& pt) const throws();

            /**
             * Compare two pixels.
             * @param pt a pixel
             * @param delta the square of a distance
             * @return true if pt is within delta of this pixel
             */
         public:
            inline bool equals(const IPixel& pt, double delta) const throws()
            {  return sqrDistance(pt) <= (delta*delta);}

            /**
             * Compare two pixels.
             */
         public:
            inline bool operator==(const IPixel& pt) const throws()
            {  return equals(pt);}

            /**
             * Compare two pixels.
             */
         public:
            inline bool operator!=(const IPixel& pt) const throws()
            {  return !equals(pt);}

            /**
             * Compute a hashvalue for this pixel.
             * @return a hashvalue
             */
         public:
            inline Int hashValue() const throws()
            {  return hash(_x) ^ hash(_y);}

            /** The pixel coordinates */
         private:
            int32_t _x, _y;
      };

   }
}
/**
 * Print a string representation of a given pixel.
 * @param out an output stream
 * @param b a pixel
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::IPixel& b)
throws();

#endif
