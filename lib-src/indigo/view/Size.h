#ifndef _INDIGO_VIEW_SIZE_H
#define _INDIGO_VIEW_SIZE_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#include <cstdint>

namespace indigo {
   namespace view {
      class Size
      {
            /** Default constructor (zero size) */
         public:
            inline Size()throws() :
            _width(0), _height(0)
            {
            }

            /**
             * Create a Size object
             * @param w the width
             * @param h the height
             * @throws ::std::invalid_argument w<0 && h<0
             */
         public:
            inline Size(double w, double h)throws(::std::invalid_argument) :
            _width(w), _height(h)
            {
               if (!(w>=0) || !(h>=0)) {
                  throw ::std::invalid_argument("Invalid size");
               }
            }

            /**
             * Compare two sizes.
             * @param sz a size
             * @return true if the width and height of both object are the same
             */
         public:
            bool equals(const Size& sz) const throws();

            /**
             * Compare two sizes.
             */
         public:
            inline bool operator==(const Size& pt) const throws()
            {  return equals(pt);}

            /**
             * Compare two sizes.
             */
         public:
            inline bool operator!=(const Size& pt) const throws()
            {  return !equals(pt);}

            /** Get the width
             * @return the width
             */
         public:
            inline double width() const throws() {return _width;}

            /** Get the height
             * @return the height
             */
         public:
            inline double height() const throws() {return _height;}

            /** The width of the canvas */
         private:
            double _width;

            /** The height of the canvas */
         private:
            double _height;
      };
   }
}

/**
 * Print a string representation of a given pixel.
 * @param out an output stream
 * @param b a pixel
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::Size& b)
throws();

#endif
