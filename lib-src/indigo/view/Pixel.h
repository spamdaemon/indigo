#ifndef _INDIGO_VIEW_PIXEL_H
#define _INDIGO_VIEW_PIXEL_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <iosfwd>
#include <cmath>
#include <cstdint>

namespace indigo {
   namespace view {

      /**
       * A pixel is a point on a 2D rendering surface. A pixel is equivalent to a point. Normally, a pixel
       * is specified in integer coordinates, but the mapping from floating point coordinates to integer
       * coordinates may depend on an application.
       * <p>
       * The primary user of Pixel is the ViewPort, which uses it to distinguish between the graphics plane, which uses
       * indigo::Point, and the view plane, which uses Pixel.
       */
      class Pixel
      {
            /** Create the pixel 0,0 */
         public:
            inline Pixel()throws()
            : _x(0),_y(0)
            {}

            /**
             * Create a new pixel.
             * @param fx x-coordinate of the pixel
             * @param fy y-coordinate of the pixel
             */
         public:
            inline Pixel(double fx, double fy)throws()
            :_x(fx),_y(fy)
            {}

            /**
             * Create a new pixel from a point.
             * @param p a point
             */
         public:
            explicit inline Pixel(const Point& p)throws()
            :_x(p.x()),_y(p.y())
            {}

            /**
             * Get the x coordinate
             * @return the x-coordinate
             */
         public:
            inline double x() const throws() {return _x;}

            /**
             * Get the y coordinate
             * @return the y-coordinate
             */
         public:
            inline double y() const throws() {return _y;}

            /**
             * Compute the square of the distance between two pixels.
             * @param p a pixel
             * @return square of the distance between the two pixels
             */
         public:
            double sqrDistance(const Pixel& p) const throws();

            /**
             * Compute the distance between two pixels.
             * @param p a pixel
             * @return square of the distance between two pixels.
             */
         public:
            inline double distanceTo(const Pixel& p) const throws()
            {  return ::std::sqrt(sqrDistance(p));}

            /**
             * Compare two pixels.
             * @param pt a pixel
             * @return true if the pixel coordinates are exactly the same
             */
         public:
            bool equals(const Pixel& pt) const throws();

            /**
             * Compare two pixels.
             * @param pt a pixel
             * @param delta the square of a distance
             * @return true if pt is within delta of this pixel
             */
         public:
            inline bool equals(const Pixel& pt, double delta) const throws()
            {  return sqrDistance(pt) <= (delta*delta);}

            /**
             * Compare two pixels.
             */
         public:
            inline bool operator==(const Pixel& pt) const throws()
            {  return equals(pt);}

            /**
             * Compare two pixels.
             */
         public:
            inline bool operator!=(const Pixel& pt) const throws()
            {  return !equals(pt);}

            /**
             * Compute a hashvalue for this pixel.
             * @return a hashvalue
             */
         public:
            inline Int hashValue() const throws()
            {  return hash(_x) ^ hash(_y);}

            /** The pixel coordinates */
         private:
            double _x, _y;
      };

   }
}
/**
 * Print a string representation of a given pixel.
 * @param out an output stream
 * @param b a pixel
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::Pixel& b);

#endif
