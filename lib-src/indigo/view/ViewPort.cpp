#include <indigo/view/ViewPort.h>

#include <cmath>

namespace indigo {
   namespace view {

      ViewPort::ViewPort()
      throws()
            : _window(1, 1), _bounds(0, 0, 1, 1)
      {
      }

      ViewPort::ViewPort(const Bounds& b, const Window& w)
      throws ( ::std::invalid_argument)
            : _window(w), _bounds(b)
      {
         if (!checkViewPort(b, w)) {
            throw new ::std::invalid_argument("Cannot construct viewport from specified bounds and window");
         }
      }

      ViewPort::~ViewPort()
      throws()
      {
      }

      ViewPort& ViewPort::operator=(const ViewPort& src)
      throws()
      {
         if (this != &src) {
            setViewPort(src._bounds, src._window);
         }
         return *this;
      }

      bool ViewPort::checkViewPort(const Bounds&, const Window& newView) const throws()
      {
         if (newView.width() == 0 || newView.height() == 0) {
            return false;
         }
         return true;
      }

      void ViewPort::setViewPort(const Bounds& newBounds, const Window& newWindow)
      throws ( ::std::invalid_argument)
      {
         if (!checkViewPort(newBounds, newWindow)) {
            throw new ::std::invalid_argument("Cannot construct viewport from specified bounds and window");
         }
         _bounds = newBounds;
         _window = newWindow;
      }

      double ViewPort::horizontalScale() const throws()
      {
         return _window.width() / _bounds.width();
      }

      double ViewPort::verticalScale() const throws()
      {
         return _window.height() / _bounds.height();
      }

      AffineTransform2D ViewPort::getModelViewTransform() const throws()
      {
         AffineTransform2D t;

         // first translate by the bottom-left point
         t.translate(-_bounds.x(), -_bounds.y());

         // rotate
         t.rotate(-_bounds.rotation());

         // scale (invert the height parameter)
         t.scale(horizontalScale(), verticalScale());

         return t;
      }

      AffineTransform2D ViewPort::getViewModelTransform() const throws()
      {
         return AffineTransform2D(_bounds.rotation(), 1.0 / horizontalScale(), 1.0 / verticalScale(), _bounds.x(),
               _bounds.y());
      }

      void ViewPort::pixel2point(double px, double py, double& x, double& y) const throws()
      {
         _bounds.fromBounds(px / horizontalScale(), py / verticalScale(), x, y);
      }

      void ViewPort::point2pixel(double x, double y, double& px, double& py) const throws()
      {
         _bounds.toBounds(x, y, px, py);
         px *= horizontalScale();
         py *= verticalScale();
      }

      ViewPort::Point ViewPort::centerPoint() const throws()
      {
         double cx, cy;
         _bounds.getCenter(cx, cy);
         return Point(cx, cy);
      }

      ViewPort::Pixel ViewPort::centerPixel() const throws()
      {
         return Pixel(_window.width() / 2, _window.height() / 2);
      }

      void ViewPort::resizeViewWindow(double left, double right, double down,
            double up) throws ( ::std::invalid_argument)
      {
         double x, y, w, h;
         double width, height;

         const double hs = horizontalScale();
         const double vs = verticalScale();
         width = _window.width() + left + right;
         height = _window.height() + up + down;
         w = width / hs;
         h = height / vs;
         Point p = getPoint(-left, -down);
         x = p.x();
         y = p.y();

         Bounds b(x, y, w, h, _bounds.rotation());
         Window win(width, height);
         setViewPort(b, win);
      }

      void ViewPort::resizeBounds(double left, double right, double down, double up)
      throws ( ::std::invalid_argument)
      {
         double x, y, w, h;
         double width, height;

         const double hs = horizontalScale();
         const double vs = verticalScale();

         w = _bounds.width() + left + right;
         h = _bounds.height() + up + down;
         // need to use fromBounds since we're doing the operation relative to the current bounds
         _bounds.fromBounds(-left, -down, x, y);
         width = hs * w;
         height = vs * h;

         Bounds b(x, y, w, h, _bounds.rotation());
         Window win(width, height);
         setViewPort(b, win);
      }

      void ViewPort::movePointToPixel(const Point& p, const Pixel& px)
      throws(::std::invalid_argument)
      {
         Pixel pix = getPixel(p);
         // we're really saying: translate pixel px such that it coincides with the point p
         translateViewWindow(pix.x() - px.x(), pix.y() - px.y());
         // we may be off by 1 pixel or so
         pix = getPixel(p);
         assert(pix.equals(px, 1.5));
      }

      void ViewPort::translateViewWindow(double dx, double dy)
      throws (::std::invalid_argument)
      {
         Point p0 = getPoint(0, 0);
         Point p = getPoint(dx, dy);
         const Bounds b(_bounds.x() + p.x() - p0.x(), _bounds.y() + p.y() - p0.y(), _bounds.width(), _bounds.height(),
               _bounds.rotation());
         setViewPort(b, _window);
      }

      void ViewPort::translateBounds(double dx, double dy)
      throws (::std::invalid_argument)
      {
         double x, y;
         _bounds.fromBounds(dx, dy, x, y);
         const Bounds b(x, y, _bounds.width(), _bounds.height(), _bounds.rotation());
         setViewPort(b, _window);
      }

      void ViewPort::rotateBounds(double phi, Point p)
      throws(::std::invalid_argument)
      {
         // first transform (x,y) to local coordinates;
         // note: the origin is at -lx,-ly from the point x,y
         double lx, ly;
         _bounds.toBounds(p.x(), p.y(), lx, ly);

         // shift the bounds over to the new center and rotate by phi
         Bounds b(p.x(), p.y(), _bounds.width(), _bounds.height(), _bounds.rotation() + phi);

         // now, take -lx, and -ly and express it in the global frame; (-lx,-ly) represent
         // the coordinates of original origin, but within the reference from of the shifted
         // and rotated bounds; therefore, we can recover the absolute coordinates of the original
         // origin
         double gx, gy;
         b.fromBounds(-lx, -ly, gx, gy);
         b.moveTo(gx, gy);

         setViewPort(b, _window);
      }

      void ViewPort::scaleBounds(double sx, double sy, Point p)
      throws (::std::invalid_argument)
      {

         // first transform (x,y) to local coordinates;
         // note: the origin is at -lx,-ly from the point x,y
         double lx, ly;
         _bounds.toBounds(p.x(), p.y(), lx, ly);

         // shift the bounds over to the new center and scale the width and height as needed
         Bounds b(p.x(), p.y(), _bounds.width() * sx, _bounds.height() * sy, _bounds.rotation());

         // now, take -lx, and -ly and express it in the global frame; (-lx,-ly) represent
         // the coordinates of original origin, but within the reference from of the shifted
         // and rotated bounds; therefore, we can recover the absolute coordinates of the original
         // origin
         double gx, gy;
         b.fromBounds(-lx * sx, -ly * sy, gx, gy);
         b.moveTo(gx, gy);

         setViewPort(b, _window);
      }

      void ViewPort::resizeBoundsToScale(double sx, double sy, Point p)
      throws(::std::invalid_argument)
      {
         if (sx <= 0 || sy <= 0) {
            throw ::std::invalid_argument("Invalid scale factor");
         }
         double nw = _window.width() / sx;
         double nh = _window.height() / sy;

         double sBoundsX = nw / _bounds.width();
         double sBoundsY = nh / _bounds.height();

         // first transform (x,y) to local coordinates;
         // note: the origin is at -lx,-ly from the point x,y
         double lx, ly;
         _bounds.toBounds(p.x(), p.y(), lx, ly);

         // shift the bounds over to the new center and scale the width and height as needed
         Bounds b(p.x(), p.y(), nw, nh, _bounds.rotation());

         // now, take -lx, and -ly and express it in the global frame; (-lx,-ly) represent
         // the coordinates of original origin, but within the reference from of the shifted
         // and rotated bounds; therefore, we can recover the absolute coordinates of the original
         // origin
         double gx, gy;
         b.fromBounds(-lx * sBoundsX, -ly * sBoundsY, gx, gy);
         b.moveTo(gx, gy);

         setViewPort(b, _window);
      }

   }

}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::ViewPort& vp)
throws()
{
   return out << "ViewPort [ window= " << vp.viewWindow() << ", bounds= " << vp.bounds() << ", horizontalScale="
         << vp.horizontalScale() << ", verticalScale=" << vp.verticalScale() << "]";
}
