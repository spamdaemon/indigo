#ifndef _INDIGO_VIEW_VIEW_H
#define _INDIGO_VIEW_VIEW_H

namespace indigo {

   /**
    * This package provides classes that are useful in dealing with
    * mapping a graphics model onto a display surface.
    * The primary class is the ViewPort, which provides a convenient way
    * to express common transformations utilized for rendering graphics.
    */
   namespace view {

   }
}
#endif
