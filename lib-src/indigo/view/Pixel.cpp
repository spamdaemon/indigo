#include <indigo/view/Pixel.h>
#include <iostream>

namespace indigo {
   namespace view {

      bool Pixel::equals(const Pixel& pt) const
      throws()
      {  return
         _x==pt._x &&
         _y==pt._y;
      }

      double Pixel::sqrDistance(const Pixel& p) const
throws   ()
   {  return sqr(_x-p._x)+
      sqr(_y-p._y);
   }

}
}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::Pixel& b)
{
   out << '(' << b.x() << ';' << b.y() << ')';
   return out;
}

