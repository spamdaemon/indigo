#include <indigo/view/IPixel.h>
#include <indigo/view/Pixel.h>
#include <iostream>
#include <cfloat>

namespace indigo {
   namespace view {

      IPixel IPixel::fromPixel(const Pixel& p)
      throws()
      {
         // change the rounding mode towards 0
         long int px = ::std::lround(p.x());
         long int py = ::std::lround(p.y());
         return IPixel(::canopy::checked_cast<int32_t>(px),::canopy::checked_cast<int32_t>(py));
      }

      Pixel IPixel::toPixel() const
      throws()
      {
         // shift the pixel by half a pixel
         return Pixel(_x,_y);
      }

      bool IPixel::equals(const IPixel& pt) const
      throws()
      {  return
         _x==pt._x &&
         _y==pt._y;
      }

      double IPixel::sqrDistance(const IPixel& p) const
throws   ()
   {  return sqr(_x-p._x)+
      sqr(_y-p._y);
   }

}
}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::IPixel& b)
throws()
{
   out << '(' << b.x() << ';' << b.y() << ')';
   return out;
}

