#include <indigo/view/Size.h>
#include <iostream>

namespace indigo {
   namespace view {

      bool Size::equals(const Size& sz) const throws()
      {
         return _width==sz._width && _height == sz._height;
      }

   }
}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::Size& b)
throws()
{
   out << '(' << b.width() << ';' << b.height() << ')';
   return out;
}

