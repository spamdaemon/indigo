#ifndef _INDIGO_VIEW_BOUNDS_H
#define _INDIGO_VIEW_BOUNDS_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <iosfwd>

namespace indigo {
   namespace view {

      /**
       * The Bounds class is used to define arbitrarily oriented rectangles within some Cartesian coordinate frame. One feature of this
       * class is that orienting or translating the bounds <em>will</em> never modify the width and height of the rectangle.
       * The rectangle is defined by three properties:
       * <ol>
       * <li>The anchor point, which is point whose coordinates are the smallest x and y when the rotation angle is 0.
       * Typically, this point would either be the lower-left point of the rectangle in a
       * standard cartesian coordinate frame, or the upper-left corner in a screen coordinates.
       * <li>The size, i.e. width and height, of the rectangle
       * <li>The orientation of rectangle in terms of a rotation around the anchor point
       * </ol>
       * <p>
       * One of the goals for this class is to achieve numerical stability. Every operation that potentially modifies the bounds is
       * checked to ensure that the resulting bounds do not have a size of 0. For example, operations whose results would yields a ratio
       * of x()/width() exceeding the precision of a normal double typically throw an exception.
       */
      class Bounds
      {
            /**
             * The default bounds are the defined by the a size of 1 and the anchor point (0,0)
             *
             */
         public:
            Bounds()throws();

            /**
             * Create a new bounds object defined by <em>a</em> corner point and width and height. This constructor
             * allows the width and/or height to be negative. The correct anchor point is computed such that w and h will
             * be positive for these bounds. The rotation is set to 0.
             * @param x x-coordinate of a corner point
             * @param y y-coordinate of a corner point
             * @param w the width as seen from this corner point
             * @param h the width as seen from this corner point
             * @throws ::std::invalid_argument if the width or height are too small
             */
         public:
            Bounds(double x, double y, double w, double h)
            throws( ::std::invalid_argument);

            /**
             * Create a new bounds object defined by <em>a</em> corner point and width and height. This constructor
             * allows the width and/or height to be negative. The correct anchor point is computed such that w and h will
             * be positive for these bounds. The rotation is set to phi.
             * @param x x-coordinate of a corner point
             * @param y y-coordinate of a corner point
             * @param w the width as seen from this corner point
             * @param h the width as seen from this corner point
             * @param phi the orientation of this bounds object (rotated around the anchor point in radians)
             * @throws ::std::invalid_argument if the width or height are too small
             */
         public:
            Bounds(double x, double y, double w, double h, double phi)
            throws( ::std::invalid_argument);

            /**
             * Directly set the bounds values. This method
             * allows the width and/or height to be negative. The correct anchor point is computed such that w and h will
             * be positive for these bounds. The rotation is set to phi.
             * @param x x-coordinate of a corner point
             * @param y y-coordinate of a corner point
             * @param w the width as seen from this corner point
             * @param h the width as seen from this corner point
             * @param phi the orientation of this bounds object (rotated around the anchor point in radians)
             * @throws ::std::invalid_argument if the width or height are too small
             */
         public:
            void setBounds(double x, double y, double w, double h, double phi)
            throws( ::std::invalid_argument);

            /**
             * @name CoordinateConversion
             * These methods enable the conversion of between points specified in the bounds coordinates and those in a global
             * or inertial coordinate frame.
             * @{
             */

            /**
             * Transform a point in the global (inertial) coordinate frame into the local coordinate frame
             * defined by this Bounds object. This example illustrates the transformation:
             * <pre>
             *   double lx,ly;
             *   b.toBounds(b.x(),b.y(),lx,ly);
             *   assert(lx==0 && ly==0);
             * </pre>
             * @param globalX the x-coordinate of the global point to be transformed
             * @param globalY the y-coordinate of the global point to be transformed
             * @param localX the x-coordinate of the same point with respect to this rectangle
             * @param localY the Y-coordinate of the same point with respect to this rectangle
             */
         public:
            void toBounds(double globalX, double globalY, double& localX, double& localY) const throws();

            /**
             * Transform a point in the coordinate frame of this Bounds object to the equivalent point in the global (inertial)
             * coordinate frame. This example illustrates the transformation:
             * <pre>
             *   double gx,gy;
             *   b.fromBounds(0,0,gx,gy);
             *   assert(b.x()==gx && b.y()==gy)
             * </pre>
             * @param localX the x-coordinate of a point with respect to this rectangle
             * @param localY the Y-coordinate of a point with respect to this rectangle
             * @param globalX the x-coordinate of the global point
             * @param globalY the y-coordinate of the global point
             */
         public:
            void fromBounds(double localX, double localY, double& globalX, double& globalY) const throws();

            /**
             * This method computes the center of the bounds in the global coordinate frame. It is
             * equivalent to <code>fromBounds(width()/2,height()/2,outX,outY)</code>.
             * @param outX the x-coordinate of the center point
             * @param outY the y-coordinate of the center point
             */
         public:
            void getCenter(double& outX, double& outY) const throws();

            /**
             * This method computes the diagonally opposite point of the origin in the global coordinate frame. It is
             * equivalent to <code>fromBounds(width,height(),outX,outY)</code>.
             * @param outX the x-coordinate of the opposing point
             * @param outY the y-coordinate of the opposing point
             */
         public:
            void getOppositePoint(double& outX, double& outY) const throws();

            /*@}*/

            /**
             * @name InvariantProperties.
             * These are properties that will never change unless explicitly modified.
             */

            /**
             * Get the current rotation of this bounds object with respect to the horizontal x-axis.
             * @return the angle (in radians) the edge makes with the horizontal as measured in graphics plane
             */
         public:
            inline double rotation() const throws()
            {
               return _phi;
            }

            /**
             * Get the width of the bounds.
             * @return the width
             */
         public:
            inline double width() const throws()
            {
               return _w;
            }

            /**
             * Get the height of the bounds.
             * @return the height
             */
         public:
            inline double height() const throws()
            {
               return _h;
            }

            /**
             * Get the x-coordinate of the anchor point.
             * @return the x-coordinate of the anchor point
             */
         public:
            inline double x() const throws()
            {
               return _x;
            }

            /**
             * Get the y-coordinate of the anchor point.
             * @return the y-coordinate of the anchor point
             */
         public:
            inline double y() const throws()
            {
               return _y;
            }

            /*@}*/

            /**
             * Check the validity of the specified bounds.
             * @param x the x-coordinate of the anchor point
             * @param y the y-coordinate of the anchor point
             * @param w the new width
             * @param h the new height
             * @param phi the new rotation angle in radians
             * @return true if the values result in a proper, non-empty bounds object
             */
         public:
            static bool checkBounds(double x, double y, double w, double h, double phi) throws();

            /**
             * @name Updates
             * These methods are used to update individual components of this bounds object. The updates
             * are within the global coordinate frame.
             * @{
             */

            /**
             * Move this bounds object's anchor point to the specified location. This method
             * does not change the rotation or the size. The new location is a point defined
             * within the global coordinate frame. Use the checkBounds() method to determine if this
             * method will throw an exception.
             * @param newX the new x-coordinate of the anchor point
             * @param newY the new y-coordinate of the anchor point
             * @throws ::std::invalid_argument if the new location that numerical would yield an empty rectangle.
             */
         public:
            void moveTo(double newX, double newY)
            throws( ::std::invalid_argument);

            /**
             * Set the rotation value to the specified value.
             * @param newRotation the new rotation angle in radians
             * @throws ::std::invalid_argument if the new rotation would yield an empty rectangle.
             */
         public:
            void setRotation(double newPhi)throws(::std::invalid_argument);

            /**
             * Set the new width and height. If the values for width or height are too small, then this method
             * throws an exception. Use the method checkBounds() to determine if the height and width are too small.
             * @param newWidth the new width
             * @param newHeight the new height
             * @throws ::std::invalid_argument if newWidth <=0 || newHeight <= 0.
             * @throws ::std::invalid_argument if newWidth and/or newHeight are too small.
             */
         public:
            void setSize(double newWidth, double newHeight)
            throws ( ::std::invalid_argument);

            /*@}*/

            /**
             * Fit the specified bouns into this bounds object. The given bounds are
             * are assumed to have the same rotation value. This method moves the given bounds
             * such that it fits entirely within the this bounds. It does this in the following
             * way:
             * <ol>
             * <li>If b is partially to the left of this, then the left edges are aligned
             * <li>If b is partially to the right of this, then the right edges are aligned
             * <li>If b is partially above this, then the the top edges are aligned
             * <li>If b is partially below this, then the bottom edges are aligned
             * <li>If b is entirely inside this, then nothing is done
             * <li>If b is wider than this, then the x-coordinates of the centers of the two bounds are aligned
             * <li>If b is higher than this, then the y-coordinates of the centers of the two bounds are aligned,
             * </ol>
             * @param b the bounds to be fitted into this
             * @return a new Bounds
             * @throws ::std::invalid_argument if this->rotation()!=b.rotation()
             * @throws ::std::invalid_argument if an invalid bounds object would result.
             */
         public:
            Bounds fitBounds(const Bounds& b) const throws( ::std::invalid_argument);

            /*@}*/
            /** The anchor point */
         private:
            double _x, _y;

            /** The current rotation */
         private:
            double _phi;

            /** The current size */
            double _w, _h;

      };

   }
}
/**
 * Emit a text representation of a bounds object.
 * @param out
 * @param b a bounds object
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::Bounds& b);

#endif
