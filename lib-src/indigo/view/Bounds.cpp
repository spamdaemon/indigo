#include <indigo/view/Bounds.h>
#include <indigo/Point.h>
#include <cmath>
#include <iostream>

namespace indigo {
   namespace view {
      Bounds::Bounds()
      throws()
            : _x(0), _y(0), _phi(0), _w(1), _h(1)
      {
      }

      Bounds::Bounds(double px, double py, double pw, double ph)
      throws (::std::invalid_argument)
      {
         setBounds(px, py, pw, ph, 0);
      }

      Bounds::Bounds(double px, double py, double pw, double ph, double phi)
      throws(::std::invalid_argument)
      {
         setBounds(px, py, pw, ph, phi);
      }

      bool Bounds::checkBounds(double ax, double ay, double bw, double bh, double phi)
      throws()
      {
         // FIXME: should we use phi or not?

         // make these volatile to prevent any sort of optimization by the compiler
         volatile double bx = ax + bw;
         volatile double by = ay + bh;

         // normally, bx and by are different from ax and ay, but if the ratio of ax to bw
         // exceeds the precision, then bx and ax (by and ay) will be the same.
         return bx != ax && by != ay;
      }

      void Bounds::setBounds(double px, double py, double pw, double ph, double phi)
      throws(::std::invalid_argument)
      {
         double nPhi = phi;
         double nx, ny, nw, nh;

         if (pw < 0) {
            nx = px + pw;
            nw = -pw;
         }
         else {
            nx = px;
            nw = pw;
         }
         if (ph < 0) {
            ny = py + ph;
            nh = -ph;
         }
         else {
            ny = py;
            nh = ph;
         }

         if (!checkBounds(nx, ny, nw, nh, nPhi)) {
            throw ::std::invalid_argument("Coordinates do not form a proper bounds rectangle");
         }
         _x = nx;
         _y = ny;
         _w = nw;
         _h = nh;
         _phi = nPhi;
      }

      void Bounds::moveTo(double newX, double newY)
      throws( ::std::invalid_argument)
      {
         if (!checkBounds(newX, newY, _w, _h, _phi)) {
            throw ::std::invalid_argument("Coordinates do not form a proper bounds rectangle");
         }
         _x = newX;
         _y = newY;
      }

      void Bounds::setRotation(double newPhi)
      throws( ::std::invalid_argument)
      {
         if (!checkBounds(_x, _y, _w, _h, newPhi)) {
            throw ::std::invalid_argument("Coordinates do not form a proper bounds rectangle");
         }
         _phi = newPhi;
      }

      void Bounds::setSize(double newWidth, double newHeight)
      throws( ::std::invalid_argument)
      {
         if (newWidth <= 0 || newHeight <= 0) {
            throw ::std::invalid_argument("Invalid width or height specified");
         }
         if (!checkBounds(_x, _y, newWidth, newHeight, _phi)) {
            throw ::std::invalid_argument("Coordinates do not form a proper bounds rectangle");
         }
         _w = newWidth;
         _h = newHeight;
      }

      void Bounds::toBounds(double globalX, double globalY, double& localX, double& localY) const throws()
      {
         double sinr = ::std::sin(-_phi);
         double cosr = ::std::cos(-_phi);

         double dx = globalX - _x;
         double dy = globalY - _y;

         localX = cosr * dx - sinr * dy;
         localY = sinr * dx + cosr * dy;
      }

      void Bounds::fromBounds(double localX, double localY, double& globalX, double& globalY) const throws()
      {
         double sinr = ::std::sin(_phi);
         double cosr = ::std::cos(_phi);

         globalX = _x + cosr * localX - sinr * localY;
         globalY = _y + sinr * localX + cosr * localY;
      }

      void Bounds::getCenter(double& outX, double& outY) const throws()
      {
         fromBounds(0.5 * _w, 0.5 * _h, outX, outY);
      }

      void Bounds::getOppositePoint(double& outX, double& outY) const throws()
      {
         fromBounds(_w, _h, outX, outY);
      }

      Bounds Bounds::fitBounds(const Bounds& b) const throws(::std::invalid_argument)
      {
         if (b._phi != _phi) {
            throw ::std::invalid_argument("fitBounds: rotation values do not match");
         }

         // compute the location of x() and y() relative to b
         double bx, by;
         toBounds(b._x, b._y, bx, by);

         // if the width of this bounds object is larger than b's width
         // the make the centesr's coincide; otherwise, shift x left or
         // right, depending on where right or left edge is
         //
         if (_w < b._w) {
            bx = 0.5 * (_w - b._w);
         }
         else if (bx < 0) {
            bx = 0;
         }
         else if ((bx + b._w) > _w) {
            bx = _w - b._w;
         }
         else {
            // leave bx as is
         }

         // do something analogous to the shift of the x-coordinates

         if (_h < b._h) {
            by = 0.5 * (_h - b._h);
         }
         else if (by < 0) {
            by = 0;
         }
         else if ((by + b._h) > _h) {
            by = _h - b._h;
         }
         else {
            // leave by as is
         }

         // transform bx,by back into the global coordinate frame and then
         // use that as the new bounds point
         fromBounds(bx, by, bx, by);
         return Bounds(bx, by, b._w, b._h, b._phi);
      }

   }
}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::view::Bounds& b)
{
   out << "Bounds [ point=(" << b.x() << ',' << b.y() << "), phi=" << b.rotation() << ",  size=" << b.width() << 'x'
         << b.height() << " ]";
   return out;
}
