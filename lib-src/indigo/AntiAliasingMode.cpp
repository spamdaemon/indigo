#include <indigo/AntiAliasingMode.h>
#include <ostream>

::std::ostream& operator<<(::std::ostream& out, const ::indigo::AntiAliasingMode& m)
{
   switch (m) {
      case ::indigo::AntiAliasingMode::BEST:
         out << "BEST";
         break;
      case ::indigo::AntiAliasingMode::FAST:
         out << "FAST";
         break;
      case ::indigo::AntiAliasingMode::NONE:
         out << "NONE";
         break;
   }
   return out;
}

