#include <indigo/Separator.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

   Separator::Separator()
   throws()
   {}

   Separator::Separator(const ::timber::Pointer< Node>& c)
   throws()
   : _child(c)
   {}

   Separator::~Separator()
   throws()
   {}

   void Separator::acceptVisitor(GfxVisitor& v) const
   throws()
   {  v.visit(*this);}

   void Separator::setNode(const ::timber::Pointer< Node>& c)
throws()
{
   if (_child!=c) {
      _child = c;
      notifyChange();
   }
}

}
