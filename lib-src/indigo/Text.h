#ifndef _INDIGO_TEXT_H
#define _INDIGO_TEXT_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_FONT_H
#include <indigo/Font.h>
#endif

#include <string>

namespace indigo {
  /**
   * This class enables display of text in a SceneGraph. Text is displayed
   * only if a font is set on the text.
   */
  class Text : public Node {
    Text&operator=(const Text&);
    
    /** Default constructor with the empty text and no font */
  public:
    inline Text () throws()
    {}

    /** 
     * Copy constructor.
     * @param t a text
     */
  public:
    Text (const Text& t) throws();
      
    /**
     * Create a Text node with the specified text, but no font. 
     * The text will not be rendered until a font is set on this node.
     * @param t a string 
     */
  public:
    Text (const ::std::string& t) throws();

    /**
     * Create a Text node with the specified text, but no font. 
     * The text will not be rendered until a font is set on this node.
     * @param t a null-terminated string
     */
  public:
    Text (const char* t) throws();

    /**
     * Create a Text node with the specified text and font.
     * @param t a string 
     * @param f a font
     */
  public:
    Text (const ::std::string& t, const ::timber::Pointer<Font>& f) throws();

    /**
     * Create a Text node with the specified text and font.
     * @param t a null-terminated string
     * @param f a font
     */
  public:
    Text (const char* t, const  ::timber::Pointer<Font>& f) throws();

    /** Destroy this text. */
  public:
    ~Text () throws();

    /**
     * Get the length of the text.
     * @return the text length
     */
  public:
    inline size_t length () const throws()
    { return _text.length(); }

    /**
     * Set the text.
     * @param t the text
     */
  public:
    void setText (const char* t) throws();

    /**
     * Set the text.
     * @param t the text
     */
  public:
    void setText (const ::std::string& t) throws();

    /**
     * Get the text.
     * @return the text
     */
  public:
    inline const ::std::string& text () const throws()
    { return _text; }

    /**
     * Set the font for this text.
     * @param f the font for this text
     */
  public:
    void setFont (const ::timber::Pointer<Font>& f) throws();
      
    /**
     * Get the current text font.
     * @return the current text font
     */
  public:
    inline ::timber::Pointer<Font> font() const throws()
    { return _font; }

    /**
     * Test if this text graphic has a font.
     * @return true if this text graphic has a font
     */
  public:
    inline bool hasFont () const throws()
    { return _font!=nullptr; }

  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
	
    /** The text */
  private:
    ::std::string _text;

    /** The text font */
  private:
    ::timber::Pointer<Font> _font;
  };
}

#endif
