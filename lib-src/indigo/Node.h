#ifndef _INDIGO_NODE_H
#define _INDIGO_NODE_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#ifndef _TIMBER_EVENT_DISPATCHER_H
#include <timber/event/Dispatcher.h>
#endif

namespace indigo {

  /** A graphic observer */
  class GfxObserver;
  
  /** A graphic observer */
  class GfxEvent;
  
  /** A visitor */
  class GfxVisitor;
  
  /** A scene graph */
  class SceneGraph;
  
  /** This class represents a graphic node. */
  class Node : public ::timber::Counted {
        CANOPY_BOILERPLATE_PREVENT_COPYING(Node);
    
    /** The scene graph will be a friend of the node */
    friend class SceneGraph;
    
    /** A dispatchable */
  private:
    struct Dispatchable {
      inline Dispatchable (GfxObserver* o, SceneGraph* g) throws()
	: _observer(o),_sceneGraph(g),_refCount(1) {}
      
      GfxObserver* _observer;
      SceneGraph* _sceneGraph;
      mutable UShort _refCount;
      bool operator==(const Dispatchable& d) const throws()
      { return _sceneGraph == d._sceneGraph; }
    };
    
    /** A dispatch function */
  private:
    struct DispatchFunction;

    /** The function that attaches a node to a scenegraph */
  private:
    struct AttachToSceneGraph;

    /** The function that detaches a node to a scenegraph */
  private:
    struct DetachFromSceneGraph;

    /** An attach function */
  private:
    struct AttachFunction;
    
    /** A detach function */
  private:
    struct DetachFunction;
    
    /** Default constructor */
  protected:
    inline Node () throws() {}
    
    /** Destroy this node */
  public:
    ~Node () throws();
    
    /**
     * Return a reference to self.
     * @return this
     */
  private:
    inline Node& self() const throws()
    { return const_cast<Node&>(*this); }
    
    /**
     * Get the number of observers
     * @return the number of observers.
     */
  protected:
    inline Int observerCount() const throws()
    { return _dispatcher.size(); }
    
    /**
     * Notify the observers of the specified event.
     * @param e an event
     */
  protected:
    void fireEvent (const GfxEvent& e) throws();
    
    /**
     * Send a gfx event to all registered listeners.
     * This just call <code>fireEvent(GfxEvent(this))</code>
     */
  protected:
    void notifyChange () throws();
    
    /**
     * Accept the specified visitor
     * @param v a visitor
     */
  public:
    inline void accept (GfxVisitor& v) const throws()
    { acceptVisitor(v); }
    
    /**
     * Accept the specified visitor. By default this method does nothing.
     * @param v a visitor
     */
  protected:
    virtual void acceptVisitor (GfxVisitor& v) const throws();
    
    /**
     * Attach this node to the specified scene graph.
     * This method does nothing by default.
     * @param sg a scene graph
     */
  protected:
    virtual void attachToSceneGraph (SceneGraph& sg) throws();
      
    /**
     * Detach this node from the specified scenegraph. It is
     * an error if sg was not attached. This method does nothing
     * by default.
     * @param sg a scene graph. 
     */
  protected:
    virtual void detachFromSceneGraph (SceneGraph& sg) throws();
      
    /**
     * Detach this node from the specified scenegraph. It is
     * an error if sg was not attached. 
     * @param node a node
     * @param sg a scene graph. 
     */
  protected:
    static void detachNode (const ::timber::Reference<Node>& node, SceneGraph& sg) throws();
      
    /**
     * Attach the given node to the specified scene graph. If
     * this node has already been attached, then the reference
     * count for this scene graph is simply bumped up by 1.
     * @param node a node
     * @param sg a scene graph
     */
  protected:
    static void attachNode (const ::timber::Reference<Node>& node, SceneGraph& sg) throws();
    
    /**
     * A node requires detaching; subclasses must call this if
     * a node is being removed while it it attached.
     * @param node a node or 0
     */
  protected:
    void detachNode (const ::timber::Reference<Node>& node) throws();

    /**
     * Attach the specified node. Subclasses must call this if
     * a new node is being added while it is attached.
     * @param node a node or 0
     */
  protected:
    void attachNode (const ::timber::Reference<Node>& node) throws();


    /**
     * Create an observer for the specified graphic. 
     * @param sg a scene graph
     * @param node a node
     * @return an observer for the graphic
     */
  private:
    static GfxObserver* createObserver (SceneGraph& sg, Node& node) throws();

    /**
     * Destroy the specified observer of a scene graph 
     */
  private:
    static void destroyObserver (SceneGraph& sg, GfxObserver* obs) throws();
    
    /** The dispatcher */
  private:
    mutable ::timber::event::Dispatcher<Dispatchable> _dispatcher;
  };
}
#endif
