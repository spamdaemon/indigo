#include <indigo/Polygons.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

  Polygons::Polygons (const Point& a, const Point& b, const Point& c) throws()
  : Shapes<Polygon>(Polygon(a,b,c)) {}
  
  Polygons::Polygons (const Point& pt, double w, double h) throws()
  : Shapes<Polygon>(Polygon::createRectangle(pt,w,h)) {}
  
  Polygons::Polygons (const Rectangle& rect) throws()
  : Shapes<Polygon>(Polygon::createRectangle(Point(rect.x(),rect.y()),rect.width(),rect.height()))
  {}
  
  Polygons::Polygons (const Polygon& p) throws()
  : Shapes<Polygon>(p) {}
  
  Polygons::Polygons (const Polygons& g) throws()
  : Shapes<Polygon>(g) {}
  
  Polygons::~Polygons () throws()
  {}
  
  void Polygons::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
