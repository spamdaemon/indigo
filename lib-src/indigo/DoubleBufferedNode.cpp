#include <indigo/DoubleBufferedNode.h>
#include <indigo/SceneGraph.h>
#include <indigo/GfxObserver.h>
#include <canopy/mt/Mutex.h>
#include <canopy/mt/MutexGuard.h>

namespace indigo {

   DoubleBufferedNode::DoubleBufferedNode()
   throws()
   {}

   DoubleBufferedNode::~DoubleBufferedNode()
   throws()
   {}

   ::timber::Reference< Node> DoubleBufferedNode::create()
throws()
{
   struct Impl : public DoubleBufferedNode {

      struct SceneGraphImpl : public SceneGraph, public GfxObserver
      {
            SceneGraphImpl(Impl* impl)throws() : _impl(impl) {}
            ~SceneGraphImpl()throws() {}

            GfxObserver* createObserver(Node&)throws() {return this;}

            void gfxChanged(const GfxEvent& e)throws()
            {
               if (_impl) {
                  _impl->fireEvent(e);
               }
            }

            Impl* _impl;
      };

      Impl() throws()
      : _front(new SceneGraphImpl(this)),_back(new SceneGraphImpl(0))
      { }
      ~Impl() throws() {
         _front->setRootNode(::timber::Pointer<Node>());
         _back->setRootNode(::timber::Pointer<Node>());
      }

      void acceptVisitor(GfxVisitor& v) const
      throws()
      {
         ::timber::Pointer< Node> c = frontBufferNode();
         if (c) {
            c->accept(v);
         }
      }

      void swapBuffers()throws()
      {
         bool changed=false;
         {
            ::canopy::mt::MutexGuard< ::canopy::mt::Mutex> G(_mutex);
            changed = _front->rootNode() != _back->rootNode();
            ::std::unique_ptr<SceneGraphImpl> tmp = move(_front);
            _front = move(_back);
            _back = move(tmp);
            _back->_impl = 0;
            _front->_impl = this;
         }
         if (changed) {
            notifyChange();
         }
      }

      void setBackBufferNode(const ::timber::Pointer< Node>& node)throws()
      {
         ::canopy::mt::MutexGuard< ::canopy::mt::Mutex> G(_mutex);
         _back->setRootNode(node);
      }

      ::timber::Pointer< Node> backBufferNode() const throws()
      {
         ::canopy::mt::MutexGuard< ::canopy::mt::Mutex> G(_mutex);
         return _back->rootNode();
      }

      ::timber::Pointer< Node> frontBufferNode() const throws() {
         ::canopy::mt::MutexGuard< ::canopy::mt::Mutex> G(_mutex);
         return _front->rootNode();
      }

      private:
      ::std::unique_ptr<SceneGraphImpl> _front,_back;

      /** The mutex */
      private:
      ::canopy::mt::Mutex _mutex;
   };
   Impl* res = new Impl();
   return res;
}
}

