#ifndef _INDIGO_FILLNODE_H
#define _INDIGO_FILLNODE_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

namespace indigo {

  /**
   * The fill node is used to render the interior of a shape. The fill node does
   * not  apply to paths or polylines. Filling may be disabled by setting the opacity
   * of the color to 0.
   * @todo How deal with a transparent fill and stroke; will the opacity values add up?
   */
  class FillNode : public StateNode {
    /** No copying */
    FillNode&operator=(const FillNode&);
    FillNode(const FillNode&);

    /**
     * Create default fillNode.
     */
  protected:
    FillNode () throws();

    /** Destructor */
  public:
    ~FillNode() throws();
  };
}

#endif
