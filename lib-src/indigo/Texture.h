#ifndef _INDIGO_TEXTURE_H
#define _INDIGO_TEXTURE_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

namespace indigo {
  /**
   * A Texture allows images to be rotated, stretched, and translated
   * in various ways. Using texture requires hardware support in
   * general. If no image transformations are needed, then an Icon
   * may be a better choice.
   */
  class Texture : public Node {
    Texture&operator=(const Texture&);
    Texture(const Texture&);

    /** 
     * Create an empty texture.
     */
  public:
    Texture () throws();

    /**  Destroy this graphic */
  public:
    ~Texture () throws();
    
    /**
     * Accept the specified visitor. Calls
     * the Node::acceptVisitor() method, followed
     * by dispatchVisitor();
     * @param v a visitor.
     */
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
