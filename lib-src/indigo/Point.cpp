#include <indigo/Point.h>
#include <iostream>

namespace indigo {

   bool Point::equals(const Point& pt) const
   throws()
   {  return
      _coords[0]==pt._coords[0] &&
      _coords[1]==pt._coords[1] &&
      _coords[2]==pt._coords[2];
   }

   double Point::sqrDistance(const Point& p) const
throws()
{  return sqr(_coords[0]-p._coords[0])+
   sqr(_coords[1]-p._coords[1])+
   sqr(_coords[2]-p._coords[2]);
}

}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::Point& b)
{
   out << '(' << b.x() << ';' << b.y() << ';' << b.z() << ')';
   return out;
}

