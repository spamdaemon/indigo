#ifndef _INDIGO_FONTDESCRIPTION_H
#define _INDIGO_FONTDESCRIPTION_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#include <string>
#include <memory>

namespace indigo {
   /**
    * This class is used to describe a particular font.
    * The description of the fields is largely taken from
    * <pre>
    *  X Logical Font Description Conventions
    *              Version 1.5
    *         X Consortium Standard
    *        X Version 11, Release 6
    *
    *             Jim Flowers
    * </pre>
    * @date 08 Jun 2003
    */
   class FontDescription
   {
         /** The <em>slant</em> of the font */
      public:
         enum Slant
         {
            ROMAN, //< Upright design
            ITALIC, //< Italic design, slanted clockwise from vertical
            OBLIQUE, //< Oblique upright design, slanted clockwise from vertical
            REVERSE_ITALIC, //< Italic design, slanted counterclockwise from vertical
            REVERSE_OBLIQUE, //< Oblique upright design, slanted counterclockwise from vertical
         };

         /**
          * Create a default font description.
          */
      public:
         FontDescription() throws();

         /**
          * Copy constructor
          * @param f a font description
          */
      public:
         FontDescription(const FontDescription& f) throws();

         /** Destroy this font */
      public:
         virtual ~FontDescription() throws();

         /**
          * Parse a font description.
          * @param string a string
          * @return a pointer to a font-description if the description could not be parsed.
          */
      public:
         static ::std::unique_ptr< FontDescription> parse(const ::std::string& str) throws();

         /** The foundry */
      private:
         ::std::string _foundry;

         /** The family */
      private:
         ::std::string _family;

         /** The weight */
      private:
         ::timber::Int _weight;

         /** The slant */
      private:
         Slant _slant;

         /** The point size */
      private:
         float _pointSize;
   };
}
#endif
