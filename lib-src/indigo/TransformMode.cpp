#include <indigo/TransformMode.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  
  TransformMode::~TransformMode () throws()
  {}
  
  void TransformMode::setMode (Mode m) throws()
  { 
    if (_mode!=m) {
      _mode = m;
      notifyChange();
    }
  }
  
  void TransformMode::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
  
}
