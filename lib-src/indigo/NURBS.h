#ifndef _INDIGO_NURBS_H
#define _INDIGO_NURBS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif


namespace indigo {
  /**
   * This class represents an immutable NURBS. NURBSs may be efficiently
   * copied, because they can share the set of vertices. Points are specified
   * always in cartesian coordinates and not homogeneous. Homogeneous coordinates
   * may be created through the use of weights.
   */
  class NURBS {
    /** The Nurbs curve */
  public:
    class NurbsCurve {};

    /** Copy constructor */
  public:
    NURBS(const NURBS& n) throws();

    /** Copy operator */
  public:
    NURBS& operator=(const NURBS& n) throws();

    /**
     * Create a new NURBS from the specified curve.
     * @param c a nurbs curve
     */
  public:
    NURBS(const NurbsCurve& c) throws();
    
    /** 
     * Create a new NURBS with uniform knots
     * @param d the degree of the NURBS
     * @param n the number of control points
     * @param pts an array of control points
     * @pre REQUIRE_GREATER(n,0)
     */
  public:
    NURBS(size_t d, size_t n, const Point* pts) throws();
    
    
    /** 
     * Create a new NURBS with the specified knots
     * @param d the degree of the NURBS
     * @param n the number of control points
     * @param pts an array of control points
     * @param knots the knots for this curve 
     * @pre REQUIRE_GREATER(n,0)
     */
  public:
    NURBS (size_t d, size_t n, const Point* pts, const double* knots) throws();
    
    /** 
     * Create a new NURBS with the specified knots
     * @param d the degree of the NURBS
     * @param n the number of control points
     * @param pts an array of control points
     * @param weights an array of weights (or 0)
     * @param knots the knots for this curve 
     * @pre REQUIRE_GREATER(n,0)
     */
  public:
    NURBS (size_t d, size_t n, const Point* pts, const double* weights, const double* knots) throws();
    
    /**
     * Get the degree of this curve
     * @return the degree of this curve
     */
  public:
    size_t degree () const throws();
    
    /**
     * Get the lower bound for the evaluation parameter.
     * @return the smallest u which is always 0
     */
  public:
    double lowerBound () const throws();
    
    /**
     * Get the upper bound for the evaluation paramter
     * @return the largest u which is always 1
     */
  public:
    double upperBound () const throws();
    
    /**
     * Return the number of control points.
     * @return the number of control points
     */
  public:
    size_t controlPointCount () const throws();
	
    /**
     * Get the specified control point.
     * @param i the index of a control vector
     * @pre REQUIRE_RANGE(i,0,controlPointCount()-1)
     * @return the i^th control point in homogeneous coordinates
     */
  public:
    Point controlPoint (Index i) const throws();
    
    /**
     * Evaluate the curve at the specified parameter.
     * @param u a value
     * @pre assertRange(u,getLowerBound(),getUpperBound());
     * @return the vector corresponding to u
     */
  public:
    Point pointAt (double u) const throws();

    /** The nurbs curve */
  private:
    NurbsCurve _curve;
  };
}
#endif
