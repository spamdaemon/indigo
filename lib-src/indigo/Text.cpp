#include <indigo/Text.h>
#include <indigo/GfxVisitor.h>
#include <indigo/Font.h>

namespace indigo {

  Text::Text (const Text& g) throws()
  : Node(),_text(g._text),_font(g._font)
  {}

  Text::Text (const ::std::string& t) throws()
  : _text(t) {}
    
  Text::Text (const char* t) throws()
  : _text(t) {}
    
  Text::Text (const ::std::string& t, const ::timber::Pointer<Font>& f) throws()
  : _text(t),_font(f) {}
    
  Text::Text (const char* t, const  ::timber::Pointer<Font>& f) throws()
  : _text(t),_font(f) {}
    
  Text::~Text () throws()
  {}
    
  void Text::setText (const char* t) throws()
  {
    if (t!=0) {
      _text = t;
    }
    else {
      _text.clear();
    }
    notifyChange();
  }

  void Text::setText (const ::std::string& t) throws()
  {
    _text = t;
    notifyChange();
  }
   
  void Text::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }

  void Text::setFont (const ::timber::Pointer<Font>& f) throws()
  {
    if (f!=_font) {
      _font = f;
      notifyChange();
    }
  }

}
