#ifndef _INDIGO_POINT_H
#define _INDIGO_POINT_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#include <cmath>
#include <iosfwd>

namespace indigo {
  
  /**
   * This class represents a 3-d or 2-d point. Points
   * are represented using the double type.
   */
  class Point {
    /** Create a default point */
  public:
    constexpr inline Point () throws()
     : _coords{0,0,0}
    {}
    
    /** 
     * Create a new 2d point 
     * @param fx x-coordinate of the point
     * @param fy y-coordinate of the point
     */
  public:
    constexpr inline Point (double fx, double fy) throws()
    : _coords{fx,fy,0}
    {}
    
    /** 
     * Create a new 3d point 
     * @param fx x-coordinate of the point
     * @param fy y-coordinate of the point
     * @param fz z-coordinate of the point
     */
  public:
    constexpr inline Point (double fx, double fy, double fz) throws()
     : _coords{fx,fy,fz}
    {}
    
    /**
     * Get the x coordinate
     * @return the x-coordinate
     */
  public:
    inline double x() const throws() { return _coords[0]; }
    
    /**
     * Get the y coordinate
     * @return the y-coordinate
     */
  public:
    inline double y() const throws() { return _coords[1]; }
    
    /**
     * Get the z coordinate
     * @return the z-coordinate
     */
  public:
    inline double z() const throws() { return _coords[2]; }
	
    /**
     * Compute the square of the distance between two points.
     * @param p a point
     * @return square of the distance between the two points
     */
  public:
    double sqrDistance (const Point& p) const throws();

    /**
     * Compute the distance between two points.
     * @param p a point
     * @return square of the distance between the two points
     */
  public:
    inline double distanceTo (const Point& p) const throws()
    { return ::std::sqrt(sqrDistance(p)); }

    /** Compare two points. */
  public:
    bool equals (const Point& pt) const throws();

    /** 
     * Compare two points with a delta value.
     * @param pt a point
     * @param delta the a distance
     * @return true if pt is within delta of this point
     */
  public:
    inline bool equals (const Point& pt, double delta) const throws()
    { return sqrDistance(pt) <= delta*delta; }
      
    /**
     * Compare two points.
     */
  public:
    inline bool operator== (const Point& pt) const throws()
    { return equals(pt); }

    /**
     * Compare two points.
     */
  public:
    inline bool operator!= (const Point& pt) const throws()
    { return !equals(pt); }

    /**
     * Compute a hashvalue for this point.
     * @return a hashvalue
     */
  public:
    inline Int hashValue () const throws()
    { return hash(_coords[0]) ^ hash(_coords[1]) ^ hash(_coords[2]); }
      
    /** The point coordinates */
  private:
    double _coords[3];
  };
      
}


/**
 * Print a string representation of a given point.
 * @param out an output stream
 * @param b a point
 * @return out
 */
::std::ostream& operator<< (::std::ostream& out, const ::indigo::Point& b);

#endif
