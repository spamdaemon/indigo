#ifndef _INDIGO_SHAPESEVENT_H
#define _INDIGO_SHAPESEVENT_H

#ifndef _INDIGO_GFXEVENT_H
#include <indigo/GfxEvent.h>
#endif

namespace indigo {
    
  /**
   * This class is created by Scene graph objects.
   * and is passed to GfxObserver.
   */
  class ShapesEvent : public GfxEvent {
    /** The event type */
  public:
    enum Type { ADDED, MODIFIED, REMOVED };
      
    /** 
     * Create a new ShapesEvent 
     * @param g the source graphic object
     * @param t the type of shape event
     * @param i the index of a shape involved
     */
  public:
    ShapesEvent (const ::timber::Pointer<Node>& g,Type t, size_t i) throws();
      
    /** Destroy this event */
  public:
    ~ShapesEvent () throws();
      
    /** 
     * Get the type of shape event.
     * @return the type of change that occurred to the shape node
     */
  public:
    inline Type type() const throws() { return _type; }
      
    /**
     * Get the index of the shape that was added, modified, or removed. If
     * the index is an invalid index, then the change involved all children.
     * @return the index of the involved shape
     * @note the index must be checked against an invalidIndex()
     */
  public:
    inline size_t index() const throws() { return _index; }
      
    /** The event type */
  private:
    Type _type;
      
    /** The index of the involved item */
  private:
    size_t _index;
  };
}
#endif
