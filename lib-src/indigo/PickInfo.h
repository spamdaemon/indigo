#ifndef _INDIGO_PICKINFO_H
#define _INDIGO_PICKINFO_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

namespace indigo {

  /**
   * This stateful node marks any nodes following
   * this node as pickable. Information maybe attached to this 
   * node by subclassing it.
   * @todo Should this have an on/off switch?
   */
  class PickInfo : public StateNode {
    PickInfo&operator=(const PickInfo&);
    PickInfo(const PickInfo&);

    /**
     * Default constructor. Creates a pick info with FILL.
     */
  public:
    inline PickInfo () throws()
    {}
	
    /**  Destroy this graphic handle */
  public:
    ~PickInfo () throws();

  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
