#include <indigo/PolyLine.h>

namespace indigo {

  PolyLine::PolyLine () throws()
  : _closed(false)
  {
  }
  
  PolyLine::PolyLine (size_t n, const Point* pts) throws()
  : _closed(false)
  {
    _vertices.reserve(n);
    _vertices.insert(_vertices.begin(),pts,pts+n);
  }
  
  PolyLine::PolyLine (size_t n, const Point* pts, bool close) throws()
  : _closed(close)
  {
    _vertices.reserve(n);
    _vertices.insert(_vertices.begin(),pts,pts+n);
  }
  

  PolyLine::PolyLine (const Point& p1, const Point& p2) throws()
  : _closed(false)
  {
    _vertices.reserve(2);
    _vertices.push_back(p1);
    _vertices.push_back(p2);
  }
      
  PolyLine::PolyLine (const Point& p1, const Point& p2, const Point& p3) throws()
  : _closed(false)
  {
    _vertices.reserve(3);
    _vertices.push_back(p1);
    _vertices.push_back(p2);
    _vertices.push_back(p3);
  }
      
  PolyLine::PolyLine (const Point& p1, const Point& p2, const Point& p3, const Point& p4) throws()
  : _closed(false)
  {
    _vertices.reserve(4);
    _vertices.push_back(p1);
    _vertices.push_back(p2);
    _vertices.push_back(p3);
    _vertices.push_back(p4);
  }
      
  PolyLine::PolyLine (const Point& p1, const Point& p2, const Point& p3, const Point& p4, const Point& p5) throws()
  : _closed(false)
  {
    _vertices.reserve(5);
    _vertices.push_back(p1);
    _vertices.push_back(p2);
    _vertices.push_back(p3);
    _vertices.push_back(p4);
    _vertices.push_back(p5);
  }

  PolyLine::PolyLine (const ::std::vector<Point>& pts, bool close) throws()
  : _vertices(pts),_closed(close)
  {}
}
