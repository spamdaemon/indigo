#ifndef _INDIGO_AntiAliasingModeNode_H
#define _INDIGO_AntiAliasingModeNode_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

#ifndef _INDIGO_ANTIALIASINGMODE_H
#include <indigo/AntiAliasingMode.h>
#endif


namespace indigo {

   /**
    * The fill node is used to render the interior of a shape. The fill node does
    * not  apply to paths or polylines. Filling may be disabled by setting the opacity
    * of the color to 0.
    * @todo How deal with a transparent fill and stroke; will the opacity values add up?
    */
   class AntiAliasingModeNode : public StateNode
   {
         /**
          * Create default anti-aliasing mode node. The default mode is AntiAliasingMode::FAST.
          */
      public:
         AntiAliasingModeNode()throws();

         /**
          * Create a new Anti-aliasing mode node.
          * @param mode the antialiasing mode
          */
      public:
         AntiAliasingModeNode(AntiAliasingMode mode)throws();

         /**
          * Copy operator
          * @param n a node
          */
      public:
         AntiAliasingModeNode& operator=(const AntiAliasingModeNode& n)throws();

         /**
          * Get the anti-aliasing mode.
          * @return the current anti-aliasing mode
          */
      public:
         inline AntiAliasingMode antiAliasingMode() const throws()
         {  return _aaMode;}

         /**
          * Set the new anti-aliasing mode.
          * param newMode the new anti-aliasing mode
          */
      public:
         void setAntiAliasingMode(AntiAliasingMode newMode) throws();

         /**
          * Compare two fillNodes. It's probably somewhat unlikely that
          * two colors are the same considering that they are represented
          * as floating point values.
          * @param m a fillNode
          * @return true if this fillNode is the same as m
          */
      public:
         bool equals(const AntiAliasingModeNode& m) const throws();

         /**
          * Compute a hashvalue for this fillNode.
          * @return a hashvalue
          */
      public:
         Int hashValue() const throws();

         /**
          * Check the two fillNodes are the same.
          * @param f a fillNodes
          * @return true if this and f are the same fillNode
          */
      public:
         inline bool operator==(const AntiAliasingModeNode& f) const throws()
         {  return equals(f);}

         /**
          * Check the two fillNodes are different.
          * @param f a fillNodes
          * @return true if this and f are not the same fillNode
          */
      public:
         inline bool operator!=(const AntiAliasingModeNode& f) const throws()
         {  return !equals(f);}

      protected:
         void acceptVisitor(GfxVisitor& v) const throws();

         /** The anti-aliasing mode */
      private:
         AntiAliasingMode _aaMode;
   };
}

#endif
