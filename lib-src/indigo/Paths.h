#ifndef _INDIGO_PATHS_H
#define _INDIGO_PATHS_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _TIKAL_PATH2D_H
#include <tikal/Path2D.h>
#endif

#include <vector>

namespace tikal {
  class Path2D;
}
namespace indigo {
  
  /**
   * This graphic is a path container. All paths
   * in this container a rendered with the same attributes.
   */
  class Paths : public Shapes< ::timber::Pointer< ::tikal::Path2D> >  {
    Paths&operator=(const Paths&);

    /** Default constructor */
  public:
    inline Paths () throws()
    {}
    
    /**
     * Create a paths object with a single path.
     * @param r a path or null
     */
  public:
    Paths (const ::timber::Reference< ::tikal::Path2D>& r) throws();
    
    /**
     * Create Paths with may paths.
     * @param regs a vector paths
     */
  public:
    Paths (const ::std::vector< ::timber::Pointer< ::tikal::Path2D> >& regs) throws();
    
    /**
     * Create Paths with may paths.
     * @param regs a vector paths
     */
  public:
    Paths (const ::std::vector< ::timber::Reference< ::tikal::Path2D> >& regs) throws();
    
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  private:
    Paths (const Paths& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~Paths () throws();
      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
