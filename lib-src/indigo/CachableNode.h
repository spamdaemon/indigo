#ifndef _INDIGO_CACHABLENODE_H
#define _INDIGO_CACHABLENODE_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

namespace indigo {
  class SceneGraph;

  /**
   * This node indicates that the subtree rooted at this node may benefit from caching the rendering instructions.
   * The exact caching mechanism is not defined, but typical mechanism are a bitmap, or an OpenGL display list.
   * The number of cachable nodes may be restricted by the renderer and to ensure that complex graphics can still
   * be cached, users of this class can specify a priority value indicating the relative importance of caching this node.
   * This node implicitly acts as a separator.
   */
  class CachableNode : public Node {
    CachableNode(const CachableNode&);
    CachableNode&operator=(const CachableNode&);

    /** The local scenegraph */
  private:
    class NodeGraph;

    /** The cache state */
  public:
    typedef ::timber::ULong CacheState;
    
    /**
     * Create a cachable node that optionally acts as a separator.
     * @param separator true if the node should act as a separator
     */
  public:
    CachableNode(bool separator=true) throws();
    
    /**
     * Create a node that can cache the specified node.
     * @param n a node
     * @param separator true if the node should act as a separator
     */
  public:
    CachableNode(const ::timber::Pointer<Node>& n, bool separator=true) throws();
    
    /** Destructor */
  public:
    ~CachableNode() throws();
   
    /**
     * Determines if this node acts as a separator.
     * @return true if this node acts as a separator
     */
  public:
    inline bool isSeparator() const throws() { return _separator; }
    
    /**
     * Get the current cache state. The only operation supported by cache state is a 
     * a comparison operator.
     * @note this number is always incremented; a 64-bit number <em>should</em> ensure that 
     * modification counts are essentially infinite
     * @return the current cache state
     */
  public:
    CacheState state() const throws();
    
    /**
     * Set the priority for this node. The higher the priority, the more likely
     * it is that the node is cached. The default is 0. 
     * @note Setting the priority does <em>not</em> result in a notification of any observers.
     * @param p a priority value
     */
  public:
    void setCachePriority (double p) throws();

    /**
     * Get the priority associated with this node.
     * @return the priority value
     */
  public:
    inline double cachePriority() const throws() { return _priority; }

    /**
     * Set the graphic that is to be cached.
     * @param node the node to be cached or null to clear the current node
     */
  public:
    void setNode (const ::timber::Pointer<Node> node) throws();

    /**
     * Return the cached node.
     * @return the cached node
     */
  public:
    ::timber::Pointer<Node> node () const throws();
    
    /** Marks the cache as dirty */
  protected:
    void attachToSceneGraph (SceneGraph& sg) throws();
    void detachFromSceneGraph (SceneGraph& sg) throws();

    /** Notify the baseclass */
  private:
    void notifyCacheDirty() throws();

    /** The priority */
  private:
    double _priority;

    /** The node (this will be the same as that of the root node) */
  private:
    ::timber::Pointer<Node> _node;

    /** The scene graph */
  private:
    ::std::unique_ptr<SceneGraph> _root;   

    /** The cache state */
  private:
    ::timber::ULong _state;

    /** A flag */
  private:
    bool _separator;

    /** The attach count */
  private:
    size_t _attachCount;
  };
}
#endif
