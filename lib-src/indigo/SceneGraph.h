#ifndef _INDIGO_SCENEGRAPH_H
#define _INDIGO_SCENEGRAPH_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

namespace indigo {

  /**
   * This class represents a scene graph.
   */
  class SceneGraph {
    SceneGraph(const SceneGraph&);
    SceneGraph&operator=(const SceneGraph&);

    /** The node is a friend of this class */
    friend class Node;

    /** Default constructor */
  public:
    SceneGraph () throws();

    /** Destroy this root */
  public:
    virtual ~SceneGraph() throws();

    /**
     * Get the currently set root node
     * @return the root node
     */
  public:
    inline ::timber::Pointer<Node> rootNode () const throws()
    { return _root; }

    /**
     * Set the root node of the scene graph.
     * @param h a handle to a new root or 0 to remove the current root.
     */
  public:
    virtual void setRootNode (::timber::Pointer<Node> h) throws();

    /**
     * Create an observer for the specified graphic. Returns
     * 0 by default.
     * @param node a node
     * @return an observer for the graphic
     */
  protected:
    virtual GfxObserver* createObserver (Node& node) throws();

    /**
     * Destroy the specified observer. This method does nothing at all.
     * @param obj a node observer
     */
  protected:
    virtual void destroyObserver (GfxObserver* obj) throws();
	
    /** The root node */
  private:
    ::timber::Pointer<Node> _root;
  };
}
#endif
