#include <indigo/SolidFillNode.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

  SolidFillNode::SolidFillNode () throws()
  : _color(Color::TRANSPARENT_BLACK)
  {}
  
  SolidFillNode::SolidFillNode (const Color& c) throws()
  : _color(c) 
  {}
  
  
  void SolidFillNode::setColor (const Color& c) throws()
  {
    if (_color!=c) {
      _color = c;
      notifyChange();
    }
  }

  bool SolidFillNode::equals (const SolidFillNode& m) const throws()
  {
    return _color == m._color;
  }

  Int SolidFillNode::hashValue () const throws()
  { return _color.hashValue(); }
  
  SolidFillNode& SolidFillNode::operator=(const SolidFillNode& n) throws()
  {
    if (&n != this) {
      bool changed=false;
      if (_color!=n._color) {
	_color = n._color;
	changed=true;
      }
      
      if (changed) {
	notifyChange();
      }
    }
    return *this;
  }


  void SolidFillNode::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }

}
