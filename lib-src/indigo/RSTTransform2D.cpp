#include <indigo/RSTTransform2D.h>
#include <iostream>

namespace indigo {
  
  bool RSTTransform2D::equals (const RSTTransform2D& t) const throws()
  { return _phi==t._phi && _scale==t._scale && _tx==t._tx && _ty==t._ty; }
  
  Int RSTTransform2D::hashValue () const throws()
  { return hash(_phi) ^ hash(_scale) ^ hash(_tx) ^ hash(_ty); }
  
  RSTTransform2D& RSTTransform2D::setTransform (double phi, double s, double tx, double ty) throws()
  {
    assert(s!=0.0);
    _phi = phi;
    _scale  = s;
    _tx  = tx;
    _ty  = ty;
    _transform.setInverseTransform(-_phi,1/_scale,-_tx,-_ty);
    return *this;
  }
  
  RSTTransform2D& RSTTransform2D::setInverseTransform (double phi, double s, double tx, double ty) throws()
  {
    assert(s!=0.0);
    
    _phi = -phi;
    _scale  = 1/s;
    _tx  = -tx;
    _ty  = -ty;
    _transform.setTransform(_phi,_scale,_tx,_ty);
    return *this;
  }
}

::std::ostream& operator<< (::std::ostream& out, const ::indigo::RSTTransform2D& t)
{ 
  out << "phi: " << t.rotation() 
      << ", scale:"<<t.scale()
      << ", translation: " << t.xTranslation()<<","<<t.yTranslation();
  return out;
}
