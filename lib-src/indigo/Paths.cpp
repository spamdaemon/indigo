#include <indigo/Paths.h>
#include <indigo/GfxVisitor.h>

using namespace ::std;
using namespace ::timber;
using namespace ::tikal;

namespace indigo {

  Paths::Paths (const Reference<Path2D>& p) throws()
  { add(p); }
  
  Paths::Paths (const vector<Reference<Path2D> >& paths) throws()
  {
    for (size_t i=0;i<paths.size();++i) {
      add(paths[i]);
    }
  }
  
  Paths::Paths (const vector<Pointer<Path2D> >& paths) throws()
  {
    for (size_t i=0;i<paths.size();++i) {
      if (paths[i]) {
	add(paths[i]);
      }
    }
  }
  
  Paths::Paths (const Paths& g) throws()
  : Shapes<Pointer<Path2D> >(g) {}
  
  Paths::~Paths () throws()
  {}
  
  void Paths::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
