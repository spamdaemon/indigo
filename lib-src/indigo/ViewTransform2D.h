#ifndef _INDIGO_VIEWTRANSFORM2D_H
#define _INDIGO_VIEWTRANSFORM2D_H

#ifndef _INDIGO_TRANSFORM2D_H
#include <indigo/Transform2D.h>
#endif

#ifndef _INDIGO_AFFINETRANSFORM2D_H
#include <indigo/AffineTransform2D.h>
#endif

#include <stdexcept>

namespace indigo {

  /**
   * This node modifies the current view transform for all nodes following.
   * Normally, this node should only be at the top of the scene graph.
   * The view transform is always guaranteed to be invertible!
   */
  class ViewTransform2D : public Transform {
    ViewTransform2D& operator=(const ViewTransform2D&);
    ViewTransform2D(const ViewTransform2D&);

    /** Default constructor */
  public:
    inline ViewTransform2D () throws()
    {}
	
    /** 
     * Create an initialized transform.
     * @param t a transform
     */
  public:
    inline ViewTransform2D (const AffineTransform2D& t) throws()
      : _transform(t) {}
      
    /**  Destroy this graphic handle */
  public:
    ~ViewTransform2D () throws();

    /**
     * Transform the specified point.
     * @param pt a point
     * @return the transformed point
     */
  public:
    Point transformPoint (const Point& pt) const throws();

    /**
     * Set the affine transform. 
     * @param t an affine transform
     * @throws std::exception if the transform is not invertible.
     */
  public:
    void setTransform (const AffineTransform2D& t) throws (::std::exception);
      
    /**
     * Get the affine transform.
     * @return the affine transform
     */
  public:
    inline const AffineTransform2D& transform () const throws()
    { return _transform; }

  protected:
    void acceptVisitor (GfxVisitor& v) const throws();

    /** The affine transform */
  private:
    AffineTransform2D _transform;
  };
}
#endif
