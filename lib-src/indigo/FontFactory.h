#ifndef _INDIGO_FONTFACTORY_H
#define _INDIGO_FONTFACTORY_H

#ifndef _INDIGO_FONT_H
#include <indigo/Font.h>
#endif

namespace indigo {

  /**
   * This class produces fonts based on a font description.
   */
  class FontFactory : public ::timber::Counted {
    
    /** The default constructor */
  protected:
    inline FontFactory() throws() {}
    
    /** Destructor */
  public:
    ~FontFactory() throws();
    
    /**
     * Get the default font for this factory.
     * @return the default font for this factory or 0 if there is no such font
     */
  public:
    virtual ::timber::Pointer<Font> queryDefaultFont () const throws() = 0;
    
    /**
     * Find a font matching the specified description.
     * @param fd a font description
     * @return the default font for this factory or 0 if there is no such font
     */
  public:
    virtual ::timber::Pointer<Font> queryFont (const FontDescription& fd) const throws() = 0;
  };
}

#endif
