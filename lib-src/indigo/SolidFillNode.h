#ifndef _INDIGO_SOLIDFILLNODE_H
#define _INDIGO_SOLIDFILLNODE_H

#ifndef _INDIGO_FILLNODE_H
#include <indigo/FillNode.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

namespace indigo {

  /**
   * The fill node is used to render the interior of a shape. The fill node does
   * not  apply to paths or polylines. Filling may be disabled by setting the opacity
   * of the color to 0.
   * @todo How deal with a transparent fill and stroke; will the opacity values add up?
   */
  class SolidFillNode : public FillNode {

    /**
     * Create default fillNode using Color::TRANSPARENT_BLACK. This default constructor
     * prevents filling of region-like objects such as polygons.
     */
  public:
    SolidFillNode () throws();

    /**
     * Create a fillNode with the specified color and width. A width of 0
     * specifies that the thinnest possible line should be drawn.
     * @param c the solid fillNode color
     */
  public:
    SolidFillNode (const Color& c) throws();

    /**
     * Copy operator
     * @param n a node
     */
  public:
    SolidFillNode& operator=(const SolidFillNode& n) throws();
    
    /**
     * Get the fillNode color.
     * @return the fillNode color
     */
  public:
    inline const Color& color () const throws()
    { return _color; }

    /**
     * Set the fillNode color. Setting a color with an opacity of 0
     * disables this fillNode.
     * @param c the new color.
     */
  public:
    void setColor (const Color& c) throws();

    /**
     * Compare two fillNodes. It's probably somewhat unlikely that 
     * two colors are the same considering that they are represented
     * as floating point values.
     * @param m a fillNode
     * @return true if this fillNode is the same as m 
     */
  public:
    bool equals (const SolidFillNode& m) const throws();

    /**
     * Compute a hashvalue for this fillNode. 
     * @return a hashvalue
     */
  public:
    Int hashValue () const throws();

    /**
     * Check the two fillNodes are the same.
     * @param f a fillNodes 
     * @return true if this and f are the same fillNode
     */
  public:
    inline bool operator==(const SolidFillNode& f) const throws()
    { return equals(f); }
      
    /**
     * Check the two fillNodes are different.
     * @param f a fillNodes 
     * @return true if this and f are not the same fillNode
     */
  public:
    inline bool operator!=(const SolidFillNode& f) const throws()
    { return !equals(f); }
	
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();

    /** The emissive color */
  private:
    Color _color;
  };
}

#endif
