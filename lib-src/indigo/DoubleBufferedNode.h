#ifndef _INDIGO_DOUBLEBUFFEREDNODE_H
#define _INDIGO_DOUBLEBUFFEREDNODE_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

namespace indigo {

   /**
    * This node can be used to implement a double-buffer scheme. The node in the backbuffer
    * can be updated independently, even in a different thread, and then be drawn after
    * swapping the buffers.
    * This node is threadsafe.
    */
   class DoubleBufferedNode : public Node
   {
         DoubleBufferedNode(const DoubleBufferedNode&);
         DoubleBufferedNode&operator=(const DoubleBufferedNode&);

         /**
          * Default constructor.
          */
      private:
         DoubleBufferedNode()throws();

         /**  Destroy this graphic */
      public:
         ~DoubleBufferedNode()throws();

         /**
          * Create a new DoubleBufferedNode. In order to set the frontBuffer of a newly created
          * node, a sequence of
          * <pre>
          * setBackBufferNode();
          * swapBuffers();
          * </pre>
          * must be used.
          * @return a double buffered node
          */
      public:
         static ::timber::Reference< Node> create()throws();

         /** Swap the front and back buffers */
      public:
         virtual void swapBuffers()throws() = 0;

         /**
          * Set the backbuffer node
          * @param node the back buffer node
          */
      public:
         virtual void setBackBufferNode(const ::timber::Pointer< Node>& node)throws() = 0;

         /**
          * Get the backbuffer node
          * @return the current backbuffer node
          */
      public:
         virtual ::timber::Pointer< Node> backBufferNode() const throws() = 0;

         /**
          * Get the frontbuffer node
          * @return the current frontbuffer node
          */
      public:
         virtual ::timber::Pointer< Node> frontBufferNode() const throws()= 0;
   };
}
#endif
