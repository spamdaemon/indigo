#ifndef _INDIGO_RSTTRANSFORM2D_H
#define _INDIGO_RSTTRANSFORM2D_H

#ifndef _INDIGO_AFFINETRANSFORM2D_H
#include <indigo/AffineTransform2D.h>
#endif

#include <cmath>
#include <iosfwd>

namespace indigo {

  /**
   * This class is used to represent transformations
   * that are represented as a sequence of translations,
   * rotations, and scaling. The RST in RSTTransform2D thus
   * represents the Rotation, Scale, and Translation components.
   * 
   * 
   * @date 26 Jul 2003
   */
  class RSTTransform2D {
    /**
     * Default constructor. 
     */
  public:
    inline RSTTransform2D () throws()
      : _phi(0),_scale(1),_tx(0),_ty(0)
      { }
      
    /**
     * Create an RST transform to rotate, scale, and translate. This
     * transform is equivalent to this sequence of transforms applied
     * to a point pt:
     * <pre>
     *  translate(tx,ty)*scale(s)*rotate(phi)*pt
     * </pre>
     * @param phi the angle of rotation in radians
     * @param s a uniform scale factor
     * @param tx a translation in x
     * @param ty a translation in y
     */
  public:
    inline RSTTransform2D (double phi, double s, double tx, double ty) throws()
    { setTransform(phi,s,tx,ty); }

   
    /**
     * Get the inverse of this transform.
     * @return an RST transform that is the inverse of this.
     */
  public:
    inline RSTTransform2D inverse () const throws()
    { RSTTransform2D t; t.setInverseTransform(_phi,_scale,_tx,_ty); return t; }


    /**
     * Get the affine transform.
     * @return the affine transform
     */
  public:
    inline const AffineTransform2D& transform() const throws()
    { return _transform; }

    /**
     * @name Manipulating the transform matrix.
     * @{
     */
      
    /**
     * Set the rotation, scale, and offsets. This method generates a er2DEvent.
     * The  transform is applied like this to a point pt:
     * <pre>
     *  scale(s)*rotate(phi)*translate(tx,ty)*pt
     * </pre>
     * @param phi a rotation angle
     * @param s the scale factor
     * @param tx the x-coordiante of the origin on the screen
     * @param ty the y-coordiante of the origin on the screen
     */
  public:
    RSTTransform2D& setTransform (double phi, double s, double tx, double ty) throws();
      
    /**
     * Set the transform to the inverse of the result of setTransform. This
     * transform is equivalent to this sequence of transforms applied
     * to a point pt:
     * <pre>
     *  rotate(-phi)*scale(1/s)*translate(-tx,-ty)*pt
     * </pre>
     * @param phi the angle of rotation in radians
     * @param s a uniform scale factor
     * @param tx a translation in x
     * @param ty a translation in y
     * @return *this
     */
  public:
    RSTTransform2D& setInverseTransform (double phi, double s, double tx, double ty) throws();
      
    /**
     * Get the values of the  transform.
     * @param phi a rotation angle
     * @param s the scale factor
     * @param tx the x-coordiante of the origin on the screen
     * @param ty the y-coordiante of the origin on the screen
     * @see setTransform
     */
  public:
    inline void getTransform (double& phi, double& s, double& tx, double& ty) throws()
    {
      phi = _phi; s  = _scale;
      tx  = _tx; ty  = _ty;
    }
      
    /**
     * Get the rotation component.
     * @return the angle by which the  is rotated.
     */
  public:
    inline double rotation () const throws()
    { return _phi; }
      
    /**
     * Set the rotation value.
     * @param phi the new rotation value
     * @see setTransform
     */
  public:
    inline void setRotation (double phi) throws()
    { setTransform(phi,_scale,_tx,_ty); }
      
    /**
     * Get the scale factor along the
     * @return the scale factor 
     */
  public:
    inline double scale() const throws()
    { return _scale; }
      
    /**
     * Set the scale factors.
     * @param s the new scale factor 
     * @see setTransform
     */
  public:
    inline void setScale (double s) throws()
    { setTransform(_phi,s,_tx,_ty); }
      
    /**
     * Scale this .
     * @param s a value by which the current scale is multiplied
     * @see setTransform
     */
  public:
    inline void scale (double s) throws()
    { 
      assert (s!=0.0);
      
      if (s!=1) {
	setTransform(_phi,_scale*s,_tx,_ty);
      }
    }
      
    /**
     * Rotate this.
     * @param phi the angle by which to rotate this 
     * @see setTransform
     */
  public:
    inline void rotate (double phi) throws()
    {
      if (phi!=0) {
	setTransform(_phi+phi,_scale,_tx,_ty);
      }
    }

    /**
     * Translate this  by a vector measured in world coordinates.
     * @param tx the x translation value
     * @param ty the y translation value
     * @see setTransform
     */
  public:
    void translate (double tx, double ty) throws()
    {
      if (tx!=0 || ty!=0) {
	setTransform(_phi,_scale,tx+_tx,ty+_ty);
      }
    }
      
    /**
     * Get the translation along the x-axis.
     * @return the translation along the x-axis
     */
  public:
    inline double xTranslation() const throws()
    { return _tx; }
	
    /**
     * Get the translation along the y-axis.
     * @return the translation along the y-axis
     */
  public:
    inline double yTranslation() const throws()
    { return _ty; }
	
    /**
     * Set the translation.
     * @param tx the translation along the x-axis
     * @param ty the translation along the y-axis
     * @see setTransform
     */
  public:
    inline void setTranslation(double tx, double ty) throws()
    { setTransform(_phi,_scale,tx,ty); }

    /*@}*/

    /**
     * Compare two transforms. It is very unlikley that
     * two transforms are the same, because of numerical error.
     * @param t a transform
     * @return true if this transform is the same as t 
     */
  public:
    bool equals (const RSTTransform2D& t) const throws();
      
    /**
     * Compute a hashvalue for this matrix 
     * @return a hashvalue
     */
  public:
    Int hashValue () const throws();
      
    /**
     * @name The transform paramters 
     * @{
     */
  private:
    double _phi;  //< The rotation angle
    double _scale;   //< The scale factor along the x-axis
    double _tx;   //< The translation along the x-axis
    double _ty;   //< The translation along the y-axis
    /*@}*/

    /** The transform's matrix */
  private:
    AffineTransform2D _transform;
  };
}

::std::ostream& operator<< (::std::ostream& out, const ::indigo::RSTTransform2D& t);

#endif
