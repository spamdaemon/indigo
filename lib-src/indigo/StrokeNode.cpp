#include <indigo/StrokeNode.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

  StrokeNode::StrokeNode () throws()
  : _color(0.0,0,0,0),_width(1)
  {}
  
  StrokeNode::StrokeNode (const Color& c, double w) throws()
  : _color(c),_width(w) 
  {}
  
  
  void StrokeNode::setColor (const Color& c) throws()
  {
    if (_color!=c) {
      _color = c;
      notifyChange();
    }
  }

  void StrokeNode::setWidth (double w) throws()
  {
    if (_width!=w) {
      _width = w;
      notifyChange();
    }
  }

  bool StrokeNode::equals (const StrokeNode& m) const throws()
  {
    return _width == m._width && _color == m._color;
  }

  Int StrokeNode::hashValue () const throws()
  { return _color.hashValue(); }

  StrokeNode& StrokeNode::operator=(const StrokeNode& n) throws()
  {
    if (&n != this) {
      bool changed=false;
      if (_color!=n._color) {
	_color = n._color;
	changed=true;
      }
      if (_width!=n._width) {
	_width = n._width;
	changed = true;
      }
      
      if (changed) {
	notifyChange();
      }
    }
    return *this;
  }

  void StrokeNode::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }

  
}
