#include <indigo/NurbsCurves.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  NurbsCurves::NurbsCurves (const NURBS& p) throws()
  : Shapes<NURBS>(p) {}
  
  NurbsCurves::NurbsCurves (const NurbsCurves& g) throws()
  : Shapes<NURBS>(g) {}
  
  NurbsCurves::~NurbsCurves () throws()
  {}
  
  
  void NurbsCurves::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
