#include <indigo/Node.h>
#include <indigo/SceneGraph.h>
#include <indigo/GfxObserver.h>
#include <indigo/GfxEvent.h>

using namespace ::timber;

namespace indigo {

   struct Node::DispatchFunction
   {
         inline DispatchFunction(const GfxEvent& ev)
               : _event(ev)
         {
         }
         inline void operator()(const Dispatchable& d) const throws()
         {
            if (d._observer) {
               d._observer->gfxChanged(_event);
            }
         }
         const GfxEvent& _event;
   };

   struct Node::AttachToSceneGraph
   {
         inline AttachToSceneGraph(const Reference< Node>& n, SceneGraph& sg)
               : _node(n), _sg(sg), _attached(false)
         {
         }
         inline void operator()(const Dispatchable& d) throws()
         {
            if (d._sceneGraph == &_sg) {
               // already attached
               ++d._refCount;
               _attached = true;
            }
         }

         void attach()
         {
            if (!_attached) {

               GfxObserver* obs = Node::createObserver(_sg, *_node);
               try {
                  Dispatchable d(obs, &_sg);
                  _node->_dispatcher.add(d);
                  _node->attachToSceneGraph(_sg);
                  assert(_node->_dispatcher.contains(d));
                  _attached = true; // just in case
               }
               catch (...) {
                  // just in case, even through we're not throwing here
                  Node::destroyObserver(_sg, obs);
                  throw;
               }
            }
         }

         Reference< Node> _node;
         SceneGraph& _sg;
         bool _attached;
   };

   struct Node::DetachFromSceneGraph
   {
         inline DetachFromSceneGraph(const Reference< Node>& n, SceneGraph& sg)
               : _node(n), _sg(sg)
         {
         }
         inline void operator()(Dispatchable& d) const throws()
         {
            if (d._sceneGraph == &_sg) {
               if (--d._refCount == 0) {
                  _node->detachFromSceneGraph(_sg);
                  GfxObserver* obs = d._observer;
                  d._sceneGraph = nullptr;
                  _node->_dispatcher.remove(d);
                  if (obs) {
                     Node::destroyObserver(_sg, obs);
                  }
               }
            }
         }
         Reference< Node> _node;
         SceneGraph& _sg;
   };

   struct Node::AttachFunction
   {
         inline AttachFunction(const Reference< Node>& n)
               : _node(n)
         {
         }
         inline void operator()(const Dispatchable& d) const throws()
         {
            attachNode(_node, *d._sceneGraph);
         }
         Reference< Node> _node;
   };

   struct Node::DetachFunction
   {
         inline DetachFunction(const Reference< Node>& n)
               : _node(n)
         {
         }
         inline void operator()(const Dispatchable& d) const throws()
         {
            detachNode(_node, *d._sceneGraph);
         }
         Reference< Node> _node;
   };

   Node::~Node() throws()
   {
   }

   void Node::attachToSceneGraph(SceneGraph&) throws()
   {
   }

   void Node::detachFromSceneGraph(SceneGraph&) throws()
   {
   }

   void Node::attachNode(const Reference< Node>& node, SceneGraph& g) throws()
   {
      AttachToSceneGraph xattach(node, g);
      node->_dispatcher.dispatch(xattach);
      // need to the actual attaching if the node was not yet registered
      xattach.attach();
   }

   void Node::detachNode(const Reference< Node>& node, SceneGraph& g) throws()
   {
      DetachFromSceneGraph xdetach(node, g);
      node->_dispatcher.dispatch(xdetach);
   }

   void Node::fireEvent(const GfxEvent& e) throws()
   {
      DispatchFunction t(e);
      _dispatcher.dispatch(t);
   }

   void Node::detachNode(const Reference< Node>& node) throws()
   {
      assert(&(*node) != this);
      DetachFunction t(node);
      _dispatcher.dispatch(t);
   }

   void Node::attachNode(const Reference< Node>& node) throws()
   {
      assert(&(*node) != this);
      AttachFunction t(node);
      _dispatcher.dispatch(t);
   }

   void Node::acceptVisitor(GfxVisitor&) const throws()
   {
   }

   void Node::notifyChange() throws()
   {
      if (observerCount() > 0) {
         fireEvent(GfxEvent(this));
      }
   }

   GfxObserver* Node::createObserver(SceneGraph& sg, Node& node) throws()
   {
      return sg.createObserver(node);
   }

   void Node::destroyObserver(SceneGraph& sg, GfxObserver* obs) throws()
   {
      sg.destroyObserver(obs);
   }

}
