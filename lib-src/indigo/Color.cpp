#include <indigo/Color.h>
#include <ostream>

namespace indigo {

   const Color Color::RED(1., 0., 0.);
   const Color Color::GREEN(0., 1., 0.);
   const Color Color::BLUE(0., 0., 1.);
   const Color Color::BLACK(0., 0., 0.);
   const Color Color::WHITE(1., 1., 1.);
   const Color Color::YELLOW(1., 1., 0.);
   const Color Color::MAGENTA(1., 0., 1.);
   const Color Color::CYAN(0., 1., 1.);
   const Color Color::LIGHT_GREY(.75, .75, .75);
   const Color Color::GREY(.5, .5, .5);
   const Color Color::DARK_GREY(.25, .25, .25);
   const Color Color::TRANSPARENT_BLACK(0.0, 0.0, 0.0, 0.0);

   Color& Color::normalize() throws()
   {
      _r = clamp(_r * _opacity, 0.0, 1.0);
      _g = clamp(_g * _opacity, 0.0, 1.0);
      _b = clamp(_b * _opacity, 0.0, 1.0);
      _opacity = 1;
      return *this;
   }

   Color Color::getPremultipliedColor(double alpha) const throws()
   {
      double s = clamp(_opacity * alpha, 0.0, 1.0);
      return Color(_r * s, _g * s, _b * s, s);
   }

   Color Color::getScaledColor(double rs, double gs, double bs, double as) const throws()
   {
      return Color(clamp(rs * _r, 0.0, 1.0), clamp(_g * gs, 0.0, 1.0), clamp(_b * bs, 0.0, 1.0),
            clamp(_opacity * as, 0.0, 1.0));
   }

   Int Color::hashValue() const throws()
   {
      return hash(_r) ^ hash(_g) ^ hash(_b) ^ hash(_opacity);
   }

   bool Color::equals(const Color& c) const throws()
   {
      return _r == c._r && _g == c._g && _b == c._b && _opacity == c._opacity;
   }

}
::std::ostream& operator<<(::std::ostream& out, const ::indigo::Color& c)
{
   out << "[ red=" << c.red() << ", green=" << c.green() << ", blue=" << c.blue() << ", opacity=" << c.opacity()
         << " ]";
   return out;
}

