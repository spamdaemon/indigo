#include <indigo/NURBS.h>
#include <cassert>

using namespace ::timber;

namespace indigo {
  namespace {
#if 0
    static ::gps::math::curve::NurbsCurve::ControlPoints makeControlPoints (size_t n, const Point* pts, const double* weights) throws()
    {
      ::gps::math::curve::NurbsCurve::ControlPoints points(n);
      for (size_t i=0;i<n;++i) {
	points.setValueAt(i,::gps::math::curve::NurbsCurve::BPoint(pts[i].x(),pts[i].y(),(weights==0 ? 1.0 : weights[i])));
      }
      return points;
    }
    
    static ::gps::math::curve::NurbsCurve::KnotVector makeKnots (size_t d, size_t n, const double* k) throws()
    {
      assert(n>=2*(d+1));
      return ::gps::math::curve::NurbsCurve::KnotVector(1+d+n,k);
    }
#endif
  }


  NURBS::NURBS(const NURBS& n) throws()
  : _curve(n._curve)
  {}

  NURBS& NURBS::operator=(const NURBS& n) throws()
  {
    _curve = n._curve;
    return *this;
  }

  NURBS::NURBS (const NurbsCurve& c) throws()
  : _curve(c) 
  {}
  
  NURBS::NURBS (size_t d, size_t n, const Point* pts) throws()
#if 0
  : _curve(d, makeControlPoints(n,pts,weights))
#endif
  {}
  
  NURBS::NURBS (size_t d, size_t n, const Point* pts, const double* knots) throws()
#if 0
  : _curve(makeKnots(d,n,knots),makeControlPoints(n,pts,0))
#endif
  {}

  NURBS::NURBS (size_t d, size_t n, const Point* pts, const double* weights, const double* knots) throws()
#if 0
  : _curve(makeKnots(d,n,knots),makeControlPoints(n,pts,weights))
#endif
  {}

  double NURBS::lowerBound () const throws()
  {
#if 0
    return _curve.lowerBound(); 
#else
    return 1;
#endif
  }

  double NURBS::upperBound () const throws()
  {
#if 0
    return _curve.upperBound(); 
#else
    return 1;
#endif
  }

    size_t NURBS::controlPointCount () const throws()
    {
#if 0
      return _curve.controlPointCount(); 
#else
    return 1;
#endif
    }

  Point NURBS::controlPoint (size_t i) const throws()
  {
#if 0
    ::gps::math::curve::NurbsCurve::BPoint p=_curve.controlPoint(i);
    return Point(p(0),p(1));
#else
    return Point(0,0);
#endif

  }
  
  Point NURBS::pointAt (double u) const throws()
  {
#if 0
    ::gps::math::curve::NurbsCurve::BPoint p=_curve.pointAt(u);
    return Point(p(0),p(1));
#else
    return Point(0,0);
#endif
  }
}
