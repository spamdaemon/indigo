#include <indigo/FontDescription.h>

namespace indigo {

   FontDescription::FontDescription() throws()
         : _weight(1),_slant(ROMAN),_pointSize(10)
   {
   }

   FontDescription::FontDescription(const FontDescription& f) throws()
         : _foundry(f._foundry), _family(f._family), _weight(f._weight), _slant(f._slant), _pointSize(f._pointSize)
   {
   }

   FontDescription::~FontDescription() throws()
   {
   }

   ::std::unique_ptr< FontDescription> FontDescription::parse(const ::std::string& str) throws()
   {
      return nullptr;
   }

}

