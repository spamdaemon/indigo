#ifndef _INDIGO_GFXEVENT_H
#define _INDIGO_GFXEVENT_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace indigo {
  /**
   * This class is created by Scene graph objects.
   * and is passed to GfxObserver.
   */
  class GfxEvent {
    /** Destroy this event */
  public:
    virtual ~GfxEvent () throws();

    /** 
     * Create a new GfxEvent 
     * @param g the source graphic object
     */
  public:
    inline GfxEvent (const ::timber::Pointer<Node>& g) throws()
      : _gfx(g) {}
      
    /** 
     * Get the source object.
     * @return the source object
     */
  public:
    inline ::timber::Pointer<Node> source() const throws()
    { return _gfx; }

    /** The source object */
  private:
    ::timber::Pointer<Node> _gfx;
  };
}
#endif
