#include <indigo/AlphaNode.h>
#include <indigo/GfxVisitor.h>
#include <cmath>

namespace indigo {

  AlphaNode::AlphaNode() throws()
  : _alpha(1) {}

  AlphaNode::AlphaNode(double a) throws()
  : _alpha(::std::min(1.0,::std::max(0.0,a)))
  {}

  AlphaNode::AlphaNode (const AlphaNode& n) throws()
      : _alpha(n._alpha) {}

  AlphaNode::~AlphaNode () throws()
  {}


  void AlphaNode::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }

  void AlphaNode::setAlpha (double a) throws()
  {
    a = ::std::min(1.0,::std::max(0.0,a));
    if (a!=_alpha) {
      _alpha = ::std::min(1.,::std::max(0.,a));
      notifyChange();
    }
  }

  AlphaNode& AlphaNode::operator= (const AlphaNode& n) throws()
  {
    if (_alpha!=n._alpha) {
      _alpha = n._alpha;
      notifyChange();
    }
    return *this;
  }
}
