#include <indigo/PolyLines.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  PolyLines::PolyLines (const Point& a, const Point& b) throws()
  : Shapes<PolyLine>(PolyLine(a,b)) {}
  
  PolyLines::PolyLines (const PolyLine& p) throws()
  : Shapes<PolyLine>(p) {}
  
  PolyLines::PolyLines (const PolyLines& g) throws()
  : Shapes<PolyLine>(g) {}
  
  PolyLines::~PolyLines () throws()
  {}
  
  void PolyLines::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
