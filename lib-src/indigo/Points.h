#ifndef _INDIGO_POINTS_H
#define _INDIGO_POINTS_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <vector>

namespace indigo {

  /**
   * This graphic is a point container. All points
   * in this container a rendered with the same attributes.
   */
  class Points : public Shapes<Point> {
    Points&operator=(const Point&);

    /** Default constructor */
  public:
    Points () throws();
    
    /**
     * Create a points with a single point. 
     */
  public:
    Points (const Point& p) throws();
    
    /**
     * Create a set of points.
     * @param n the number of points
     * @param pts a set of points
     */
  public:
    Points (Int n, const Point* pts) throws();
    
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  private:
    Points (const Points& g) throws();
  
    /**
     * Set the width in pixels with which the points should be rendered. If
     * the specified size is set to a value less than 1, then the point size
     * will be set to 0.
     * @param sz the size of a point in pixels
     */
  public:
    void setPointSize (Int sz) throws();

    /**
     * Get the point size. 
     * @return the point size in pixels.
     */
  public:
    inline Int pointSize () const throws()
    { return _pointSize; }

    /**  Destroy this graphic handle */
  public:
    ~Points () throws();
      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
      
    /** The point size */
  private:
    Int _pointSize;
  };
}
#endif
