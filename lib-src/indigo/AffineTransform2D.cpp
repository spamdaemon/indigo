#include <indigo/AffineTransform2D.h>
#include <iostream>
#include <cmath>

using namespace ::std;

namespace indigo {

   bool AffineTransform2D::isIdentityTransform() const
   throws()
   {
      return _matrix[0]==1 && _matrix[1]==0 && _matrix[2]==0 && _matrix[3]==0 && _matrix[4]==1 && _matrix[5]==0;
   }

   Point AffineTransform2D::transformVector(double vx, double vy) const
   throws()
   {
      double x = vx*value(0,0)+vy*value(0,1);
      double y = vx*value(1,0)+vy*value(1,1);
      return Point(x,y);
   }

   Point AffineTransform2D::transformPoint(const double px, double py) const
   throws()
   {
      double x = px*value(0,0)+py*value(0,1)+value(0,2);
      double y = px*value(1,0)+py*value(1,1)+value(1,2);
      return Point(x,y);
   }

   AffineTransform2D& AffineTransform2D::setRawTransform(double m00, double m01, double m02, double m10, double m11,
         double m12)
   throws()
   {
      value(0,0) = m00;value(0,1) = m01;value(0,2) = m02;
      value(1,0) = m10;value(1,1) = m11;value(1,2) = m12;
      return *this;
   }

   AffineTransform2D& AffineTransform2D::setTransform(double phi, double sx, double sy, double tx, double ty)
   throws()
   {
      const double SIN(::std::sin(phi));
      const double COS(::std::cos(phi));
      return setRawTransform(COS*sx,-(SIN*sx),tx,SIN*sy,COS*sy,ty);
   }

   AffineTransform2D::AffineTransform2D(const AffineTransform2D& l, const AffineTransform2D& r)
   throws()
   {
      value(0,0) = l(0,0)*r(0,0) + l(0,1)*r(1,0);
      value(1,0) = l(1,0)*r(0,0) + l(1,1)*r(1,0);
      value(0,1) = l(0,0)*r(0,1) + l(0,1)*r(1,1);
      value(1,1) = l(1,0)*r(0,1) + l(1,1)*r(1,1);
      value(0,2) = l(0,0)*r(0,2) + l(0,1)*r(1,2) + l(0,2);
      value(1,2) = l(1,0)*r(0,2) + l(1,1)*r(1,2) + l(1,2);
   }

   AffineTransform2D& AffineTransform2D::setInverseTransform(double phi, double sx, double sy, double tx, double ty)
   throws()
   {
      const double SIN(::std::sin(-phi));
      const double COS(::std::cos(-phi));

      const double m00 = COS/sx;
      const double m01 = -(SIN/sy);
      const double m10 = SIN/sx;
      const double m11 = COS/sy;

      return setRawTransform(m00,m01,-m00*tx-m01*ty,m10,m11,-m10*tx-m11*ty);
   }

   AffineTransform2D& AffineTransform2D::setToScale(double sx, double sy, double x, double y)
   throws()
   {
#if 1
      value(0,0) = sx;
      value(1,0) = value(0,1) = 0;
      value(1,1) = sy;
      value(0,2) = x *(1-sx);
      value(1,2) = y *(1-sy);
#else
      setToTranslate(-x,-y);
      scale(sx,sy);
      translate(x,y);
#endif
      return *this;
   }

   AffineTransform2D& AffineTransform2D::setToRotateScale(double phi, double sx, double sy, double x, double y)
   throws()
   {
#if 1
      setTransform(phi,sx,sy,x,y);
      value(0,2) -= x*value(0,0) + y*value(0,1);
      value(1,2) -= x*value(1,0) + y*value(1,1);
#else
      setToTranslate(-x,-y);
      rotate(phi);
      scale(sx,sy);
      translate(x,y);
#endif
      return *this;
   }

   AffineTransform2D& AffineTransform2D::rotateScale(double phi, double s)
   throws()
   {
      const double SIN(s* ::std::sin(phi));
      const double COS(s* ::std::cos(phi));

      for (Int i=0;i<3;++i) {
         double t0 = value(0,i);
         double t1 = value(1,i);
         value(0,i) = COS*t0 - SIN*t1;
         value(1,i) = SIN*t0 + COS*t1;
      }
      return *this;
   }

   AffineTransform2D& AffineTransform2D::scale(double sx, double sy)
   throws()
   {
      for (Int i=0;i<3;++i) {
         value(0,i) *= sx;
         value(1,i) *= sy;
      }
      return *this;
   }

   bool AffineTransform2D::equals(const AffineTransform2D& t) const
   throws()
   {
      for (size_t i=0;i<6;++i) {
         if (_matrix[i]!=t._matrix[i]) {
            return false;
         }
      }
      return true;
   }

   Int AffineTransform2D::hashValue() const
   throws()
   {
      Int h = 0;
      for (size_t i=0;i<6;++i) {
         h = h ^ hash(_matrix[i]);
      }
      return h;
   }

   AffineTransform2D AffineTransform2D::inverse() const throws(::std::runtime_error)
   {
      const double err = 0.0; //1.0e-10

      size_t i, j;
      if (::std::abs(value(0, 0)) < ::std::abs(value(1, 0))) {
         // a row swap
         i = 1;
         j = 0;
      }
      else {
         i = 0;
         j = 1;
      }

      double a00 = value(i, 0);

      if (::std::abs(a00) <= err) {
         throw ::runtime_error("Not invertible");
      }

      double a01 = value(i, 1);
      double a02 = value(i, 2);
      double a10 = value(j, 0);
      double a11 = value(j, 1);
      double a12 = value(j, 2);

      double m00 = 1;
      double m11 = 1;
      double m01 = 0;
      double m10 = 0;

      // first stage of elimination: third column
      double m02 = -a02;
      double m12 = -a12;

      // second stage of elimination: first column
      a11 = (a00 * a11 - a01 * a10);
      if (::std::abs(a11) <= err) {
         throw ::std::runtime_error("Not invertible");
      }

      m10 = (a00 * m10 - m00 * a10) / a11;
      m11 = (a00 * m11 - m01 * a10) / a11;
      m12 = (a00 * m12 - m02 * a10) / a11;

      // last stage: subtract second line from first line
      m00 = (m00 - a01 * m10) / a00;
      m01 = (m01 - a01 * m11) / a00;
      m02 = (m02 - a01 * m12) / a00;

      // done
      if (i == 0) {
         return AffineTransform2D(m00, m01, m02, m10, m11, m12);
      }
      else {
         return AffineTransform2D(m01, m00, m02, m11, m10, m12);
      }
   }

}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::AffineTransform2D& t)
{
   out << "[ [ " << t(0, 0) << ", " << t(0, 1) << "," << t(0, 2) << "], [ ";
   out << t(1, 0) << ", " << t(1, 1) << "," << t(1, 2) << "] ]";
   return out;
}
