#ifndef _INDIGO_POLYLINES_H
#define _INDIGO_POLYLINES_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _INDIGO_POLYLINE_H
#include <indigo/PolyLine.h>
#endif

#include <vector>

namespace indigo {

  /**
   * This graphic is a polyline container. All polylines
   * in this container a rendered with the same attributes.
   */
  class PolyLines : public Shapes<PolyLine> {
    PolyLines&operator=(const PolyLines&);
    
    /** Default constructor */
  public:
    inline PolyLines () throws()
    {}
	
    /**
     * Create a polylines consisting of a single line between two points.
     * @param a the start point
     * @param b the end point
     */
  public:
    PolyLines (const Point& a, const Point& b) throws();
      
    /**
     * Create a polylines with a single polyline. 
     * @param p a polyline
     */
  public:
    PolyLines (const PolyLine& p) throws();
      
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     * @param g polylines
     */
  private:
    PolyLines (const PolyLines& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~PolyLines () throws();

      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
