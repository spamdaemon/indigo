#ifndef _INDIGO_CIRCLE_H
#define _INDIGO_CIRCLE_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif


namespace indigo {

  /**
   * This class represents an immutable circle. Circles may be efficiently
   * copied, because they can share the set of vertices.
   */
  class Circle {
      
    /**
     * Create the unit circle centered at (0,0) and radius 1.
     */
  public:
    inline Circle () throws()
      : _center(0,0),_radius(1)
      {}

    
    /**
     * Create a circle with a given radius. The radius
     * may 0, in which case the circle should be rendered
     * as a point.
     * @param c the circle center
     * @param r the radius of the circle
     * @pre REQUIRE_GREATER_OR_EQUAL(r,0)
     */
  public:
    inline Circle (const Point& c, double r) throws()
      : _center(c),_radius(r)
      {
	assert(r>=0.0);
      }
      
    /**
     * Get the length of the major axis.
     * @return the length of the major axis
     */
  public:
    inline double radius () const throws()
    { return _radius; }

    /**
     * Get the circle center.
     * @return a reference to the center point
     */
  public:
    inline const Point& center () const throws()
    { return _center; }

    /** The circle center */
  private:
    Point _center;

    /** The major axis length */
  private:
    double _radius;
  };
}
#endif
