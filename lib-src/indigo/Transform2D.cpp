#include <indigo/Transform2D.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

   Transform2D::Transform2D(const AffineTransform2D& t)
   throws()
   : _transform(t) {}

   Transform2D::~Transform2D()
   throws()
   {}

   ::timber::Reference< Transform2D> Transform2D::createRotation(double phi)
   throws()
   {  return new Transform2D(AffineTransform2D::createRotation(phi));}

   ::timber::Reference< Transform2D> Transform2D::createScale(double s)
   throws()
   {  return new Transform2D(AffineTransform2D::createScale(s));}

   ::timber::Reference< Transform2D> Transform2D::createScale(double sx, double sy)
   throws()
   {  return new Transform2D(AffineTransform2D::createScale(sx,sy));}

   ::timber::Reference< Transform2D> Transform2D::createTranslation(double tx, double ty)
   throws()
   {  return new Transform2D(AffineTransform2D::createTranslation(tx,ty));}

   Point Transform2D::transformPoint(const Point& pt) const
   throws()
   {  return _transform.transformPoint(pt);}

   void Transform2D::acceptVisitor(GfxVisitor& v) const
throws()
{  v.visit(*this);}
}
