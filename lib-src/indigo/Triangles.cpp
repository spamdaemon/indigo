#include <indigo/Triangles.h>

namespace indigo {
  
  Triangles::Triangles (Int n, Point* pts, Type t) throws()
  : _type(t)
  {
    assert(n>2);
    _vertices.reserve(n);
    _vertices.insert(_vertices.begin(),pts,pts+n);
  }
  
  Triangles::Triangles (const Point& a, const Point& b, const Point& c) throws()
  : _type(TRIANGLES)
  {
    _vertices.reserve(3);
    _vertices.push_back(a);
    _vertices.push_back(b);
    _vertices.push_back(c);
  }
  
  
  void Triangles::vertices (size_t t, size_t& v0, size_t& v1, size_t& v2) const throws()
  {
    assert(t<count());
    triangleVertices(t,_type,v0,v1,v2);
  }

  void Triangles::triangleVertices (size_t t, Type tp, size_t& v0, size_t& v1, size_t& v2) throws()
  {
    switch (tp) {
    case TRIANGLES:
      v0 = 3*t; v1=v0+1;v2=v1+1;
      break;
    case STRIP:
      if ((t % 2) == 0) {
	v0 = t; v1=v0+1; v2=v1+1;
      }
      else {
	v2 = t; v1=v2+1; v0=v1+1;
      }
      break;
    case FAN:
      v0 = 0; v1 = t+1; v2 = v1+1;
      break;
    }
  }

}
