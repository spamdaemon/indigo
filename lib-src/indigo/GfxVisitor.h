#ifndef _INDIGO_GFXVISITOR_H
#define _INDIGO_GFXVISITOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace indigo {

   /**
    * @name Forward declarations of core gfx classes.
    * @{
    */
   class AlphaNode;
   class AntiAliasingModeNode;
   class CachableNode;
   class Circles;
   class CompositeNode;
   class Coordinates2D;
   class Coordinates3D;
   class Ellipses;
   class Icon;
   class NurbsCurves;
   class Paths;
   class PickInfo;
   class Points;
   class Polygons;
   class PolyLines;
   class Raster;
   class Regions;
   class SolidFillNode;
   class Separator;
   class StrokeNode;
   class Switch;
   class Text;
   class Texture;
   class Transform2D;
   class TransformMode;
   class TriangleSet;
   class ViewTransform2D;
   /*@}*/

   /**
    * The abstract visitor class.
    */
   class GfxVisitor
   {
         /** Destroy this visitor */
      public:
         virtual ~GfxVisitor()throws() = 0;

         /**
          * @name Visitation functions for all core gfx classes.
          * @{
          */

         /**
          * Visit an alpha value node
          * @param g an alpha value node
          */
      public:
         virtual void visit(const AlphaNode& g)throws() = 0;

         /**
          * Visit an anti-aliasing mode node
          * @param g an anti-aliasing node
          */
      public:
         virtual void visit(const AntiAliasingModeNode& g)throws() = 0;

         /**
          * Visit an cachable node
          * @param g an CachableNode
          */
      public:
         virtual void visit(const CachableNode& g)throws() = 0;

         /**
          * Visit an Circles shape
          * @param g an Circles shape
          */
      public:
         virtual void visit(const Circles& g)throws() = 0;

         /**
          * Visit an Ellipses shape
          * @param g an Ellipses shape
          */
      public:
         virtual void visit(const Ellipses& g)throws() = 0;

         /**
          * Visit a Points shape
          * @param g a Points shape
          */
      public:
         virtual void visit(const Points& g)throws() = 0;

         /**
          * Visit a Polygons shape
          * @param g a Polygons shape
          */
      public:
         virtual void visit(const Polygons& g)throws() = 0;

         /**
          * Visit a TriangleSet shape
          * @param g a TriangleSet shape
          */
      public:
         virtual void visit(const TriangleSet& g)throws() = 0;

         /**
          * Visit a PolyLines shape
          * @param g a PolyLines shape
          */
      public:
         virtual void visit(const PolyLines& g)throws() = 0;

         /**
          * Visit a NurbsCurves shape
          * @param g a NurbsCurves shape
          */
      public:
         virtual void visit(const NurbsCurves& g)throws() = 0;

         /**
          * Visit a CompositeNode
          * @param g a CompositeNode
          */
      public:
         virtual void visit(const CompositeNode& g)throws() = 0;

         /**
          * Visit a Switch node
          * @param g a Switch
          */
      public:
         virtual void visit(const Switch& g)throws() = 0;

         /**
          * Visit a Transform2D node.
          * @param g a Transform2D node
          */
      public:
         virtual void visit(const Transform2D& g)throws() = 0;

         /**
          * Visit a ViewTransform2D node.
          * @param g a ViewTransform2D node
          */
      public:
         virtual void visit(const ViewTransform2D& g)throws() = 0;

         /**
          * Visit a TransformMode node
          * @param g a TransformMode node
          */
      public:
         virtual void visit(const TransformMode& g)throws() = 0;

         /**
          * Visit a fill node.
          * @param f a fill
          */
      public:
         virtual void visit(const SolidFillNode& f)throws() = 0;

         /**
          * Visit a stroke node.
          * @param s a stroke
          */
      public:
         virtual void visit(const StrokeNode& s)throws() = 0;

         /**
          * Visit a Text node.
          * @param g a Text node
          */
      public:
         virtual void visit(const Text& g)throws() = 0;

         /**
          * Visit a coordinates nodes.
          * @param g a points graphic
          */
      public:
         virtual void visit(const Coordinates2D& g)throws() = 0;

         /**
          * Visit a coordinates nodes.
          * @param g a points graphic
          */
      public:
         virtual void visit(const Coordinates3D& g)throws() = 0;

         /**
          * Visit an icon.
          */
      public:
         virtual void visit(const Icon& g)throws() = 0;

         /**
          * Visit a raster.
          */
      public:
         virtual void visit(const Raster& g)throws() = 0;
         /*@}*/

         /**
          * Visit an texture.
          */
      public:
         virtual void visit(const Texture& g)throws() = 0;

         /**
          * Visit regions
          */
      public:
         virtual void visit(const Regions& g)throws() = 0;

         /**
          * Visit paths.
          */
      public:
         virtual void visit(const Paths& g)throws() = 0;

         /**
          * Visit a pick info node
          */
         virtual void visit(const PickInfo& g)throws() = 0;

         /**
          * Visit a separator node
          * @param g a separator node
          */
      public:
         virtual void visit(const Separator& g)throws() = 0;

         /*@}*/

   };
}
#endif
