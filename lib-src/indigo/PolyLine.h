#ifndef _INDIGO_POLYLINE_H
#define _INDIGO_POLYLINE_H

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <vector>

namespace indigo {

  /**
   * This class represents an immutable polyline. PolyLines may be efficiently
   * copied, because they can share the set of vertices.
   * @note PolyLines may have 0 vertices to indicate that there is no line
   */
  class PolyLine {
    /** 
     * Create an empty polyline.
     */
  public:
    PolyLine () throws();
      
    /** 
     * Create a new line between two points.
     * @param p1 the start point
     * @param p2 the end point
     */
  public:
    PolyLine (const Point& p1, const Point& p2) throws();
      
    /** 
     * Create a new line consisting of two segments.
     * @param p1 the start point
     * @param p2 the middle point
     * @param p3 the end point
     */
  public:
    PolyLine (const Point& p1, const Point& p2, const Point& p3) throws();
      
    /** 
     * Create a new line consisting of three segments.
     * @param p1 the start point
     * @param p2 the second point
     * @param p3 the third point
     * @param p4 the end point
     */
  public:
    PolyLine (const Point& p1, const Point& p2, const Point& p3, const Point& p4) throws();
      
    /** 
     * Create a new line consisting of four segments.
     * @param p1 the start point
     * @param p2 the second point
     * @param p3 the third point
     * @param p4 the fourth point
     * @param p5 the end point
     */
  public:
    PolyLine (const Point& p1, const Point& p2, const Point& p3, const Point& p4, const Point& p5) throws();
      
    /** 
     * Create a new polyline.
     * @param n the number of points
     * @param pts an array of points
     * @pre REQUIRE_GREATER(n,0)
     */
  public:
    PolyLine (size_t n, const Point* pts) throws();
      
    /** 
     * Create a new polyline.
     * @param n the number of points
     * @param pts an array of points
     * @param close true if the last point should join with the first
     * @pre REQUIRE_GREATER(n,0)
     */
  public:
    PolyLine (size_t n, const Point* pts, bool close) throws();
      
    /** 
     * Create a new polyline.
     * @param pts an array of points
     * @param close true if the last point should join with the first
     * @pre REQUIRE_GREATER(pts.size(),0)
     */
  public:
    PolyLine (const ::std::vector<Point>& pts, bool close) throws();
      
    /**
     * Test if this polyline is closed.
     * @return true if the last point should join with the first
     */
  public:
    inline bool isClosed () const throws()
    { return _closed; }

    /**
     * Get the number of points or vertices.
     * @return the number of vertices in the polyline
     */
  public:
    inline size_t vertexCount () const throws()
    { return _vertices.size(); }

    /**
     * Get the start vertex.
     * @return the first vertex
     */
  public:
    inline const Point& startVertex () const throws()
    { return _vertices[0]; }

    /**
     * Get the last vertex.
     * @return the last vertex
     */
  public:
    inline const Point& endVertex () const throws()
    { return _vertices[vertexCount()-1]; }

    /**
     * Get the specified vertex.
     * @param pos a vertex index
     * @return a reference to the specified vertex
     */
  public:
    inline const Point& vertex (size_t pos) const throws()
    { return _vertices[pos]; }
      
    /**
     * Get  the vertices.
     * @return a vector of points
     */
  public:
    inline const ::std::vector<Point>& vertices() const throws() { return _vertices; }

    /** The point coordinates */
  private:
    ::std::vector<Point> _vertices;

    /** True if this line is closed */
  private:
    bool _closed;
  };
}
#endif
