#include <indigo/Polygon.h>

namespace indigo {

  Polygon::Polygon() throws()
  : _shapeType(UNKNOWN_SHAPE),
    _orientation(UNKNOWN_ORIENTATION)
  {}
  
  Polygon::Polygon (size_t n, const Point* pts, ShapeType st) throws()
  : _shapeType(n==3 ? CONVEX_SHAPE : st),
    _orientation(UNKNOWN_ORIENTATION)
  {
    _vertices.reserve(n);
    _vertices.insert(_vertices.begin(),pts,pts+n);
  }
  
  Polygon::Polygon (size_t n, const Point* pts) throws()
  : _shapeType(n==3 ? CONVEX_SHAPE : UNKNOWN_SHAPE),
    _orientation(UNKNOWN_ORIENTATION)
  {
    _vertices.reserve(n);
    _vertices.insert(_vertices.begin(),pts,pts+n);
  }

  Polygon::Polygon (const Point& a, const Point& b, const Point& c) throws()
  :  _shapeType(CONVEX_SHAPE),
    _orientation(UNKNOWN_ORIENTATION)
  {
    _vertices.reserve(3);
    _vertices.push_back(a);
    _vertices.push_back(b);
    _vertices.push_back(c);
  }
    
  Polygon Polygon::createRectangle (const Point& pt, double w, double h) throws()
  { return createRectangle(pt,Point(pt.x()+w,pt.y()+h)); }

  Polygon Polygon::createRectangle (const Point& p1, const Point& p2) throws()
  {
    Point pts[4];
    pts[0] = p1;
    pts[1] = Point(p1.x(),p2.y());
    pts[2] = p2;
    pts[3] = Point(p2.x(),p1.y());

    Polygon p = Polygon(4,pts,CONVEX_SHAPE);
    p._orientation = CCW_ORIENTATION;
    return p;
  }
}
