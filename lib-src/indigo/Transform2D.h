#ifndef _INDIGO_TRANSFORM2D_H
#define _INDIGO_TRANSFORM2D_H

#ifndef _INDIGO_TRANSFORM_H
#include <indigo/Transform.h>
#endif

#ifndef _INDIGO_AFFINETRANSFORM2D_H
#include <indigo/AffineTransform2D.h>
#endif

namespace indigo {

   /**
    * This node transforms objects using an affine transform in 2D space.
    */
   class Transform2D : public Transform
   {
         Transform2D& operator=(const Transform2D&);
         Transform2D(const Transform2D&);

         /** Default constructor */
      public:
         inline Transform2D()throws()
         {}

         /**
          * Create an initialized transform.
          * @param t a transform
          */
      public:
         Transform2D(const AffineTransform2D& t)throws();

         /**  Destroy this graphic handle */
      public:
         ~Transform2D()throws();

         /**
          * Create a rotation transform.
          * @param phi the rotation angle
          * @return <tt>AffineTransform2D(phi,1,0,0)</tt>
          */
      public:
         static ::timber::Reference< Transform2D> createRotation(double phi)throws();

         /**
          * Create a  uniform scale transform.
          * @param s the scale factor
          * @return <tt>AffineTransform2D(0,s,0,0)</tt>
          */
      public:
         static ::timber::Reference< Transform2D> createScale(double s)throws();

         /**
          * Create a  non-uniform scale transform.
          * @param sx the scale factor
          * @param sy the scale factor
          * @return <tt>AffineTransform2D(0,sx,sy,0,0)</tt>
          */
      public:
         static ::timber::Reference< Transform2D> createScale(double sx, double sy)throws();

         /**
          * Create a translation transform.
          * @param tx the translation along the x-axis
          * @param ty the translation along the y-axis
          * @return <tt>AffineTransform2D(0,1,tx,ty)</tt>
          */
      public:
         static ::timber::Reference< Transform2D> createTranslation(double tx, double ty)throws();

         /**
          * Transform the specified point.
          * @param pt a point
          * @return the transformed point
          */
      public:
         Point transformPoint(const Point& pt) const throws();

         /**
          * Set the affine transform.
          * @param t an affine transform
          */
      public:
         inline void setTransform(const AffineTransform2D& t)throws()
         {
            _transform = t;
            notifyChange();
         }

         /**
          * Get the affine transform.
          * @return the affine transform
          */
      public:
         inline const AffineTransform2D& transform() const throws()
         {  return _transform;}

      protected:
         void acceptVisitor(GfxVisitor& v) const throws();

         /** The affine transform */
      private:
         AffineTransform2D _transform;
   };
}
#endif
