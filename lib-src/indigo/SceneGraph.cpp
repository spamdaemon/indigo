#include <indigo/SceneGraph.h>

namespace indigo {

  SceneGraph::SceneGraph () throws()
  {}
  
  SceneGraph::~SceneGraph() throws()
  {
    assert(!_root && "SceneGraph still has a handle to the root");
  }
  
  void SceneGraph::setRootNode (::timber::Pointer<Node> h) throws()
  {
    if (_root!=h) {
      if (_root) {
	Node::detachNode(_root,*this);
      }
      _root = h; 
      if (_root) {
	Node::attachNode(_root,*this);
      }
    }
  }
  
  GfxObserver* SceneGraph::createObserver (Node&) throws()
  { return 0; }
  
  void SceneGraph::destroyObserver (GfxObserver*) throws()
  {  }
  
}
