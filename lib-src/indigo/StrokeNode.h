#ifndef _INDIGO_STROKENODE_H
#define _INDIGO_STROKENODE_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

namespace indigo {

  /**
   * A stroke is used to render a one-dimensional object such as a path or the 
   * outline of some 2D region. The simplest strokeNodes are those defining a simple 
   * color, but complex strokeNode patterns can be specified. To disable the current
   * strokeNode, for example, to avoid rendering the outline of a polygon, it's 
   * opacity must be set to 0.<br>
   * @todo Add line cap for the beginning and end points of lines or polylines
   * @todo Add joints for internal vertices of polylines or region outlines
   */
  class StrokeNode : public StateNode {

    /**
     * Creates a transparent stroke, which prevents lines from being rendered.
     */
  public:
    StrokeNode () throws();

    /**
     * Create a stroke with the specified color and width. A width of 0
     * specifies that the thinnest possible line should be drawn.
     * @param c the solid stroke color
     * @param w the stroke width in pixels
     */
  public:
    StrokeNode (const Color& c, double w=1) throws();
    
    /**
     * Assign operator.
     * @param n a node
     * @return this node
     */
  public:
    StrokeNode& operator=(const StrokeNode& n) throws();

    /**
     * Get the stroke color.
     * @return the stroke color
     */
  public:
    inline const Color& color () const throws()
    { return _color; }

    /**
     * Set the stroke color. Setting a color with an opacity of 0
     * disables this stroke.
     * @param c the new color.
     */
  public:
    void setColor (const Color& c) throws();

    /**
     * Get the current stroke width.
     * @return the width of this stroke
     */
  public:
    inline double width() const  throws() { return _width; }

    /**
     * Set the stroke width.
     * @param w the new stroke width
     */
  public:
    void setWidth (double w) throws();

    /**
     * Compare two strokes. It is very unlikley that
     * two transforms are the same, because of numerical error.
     * @param m a stroke
     * @return true if this stroke is the same as m 
     */
  public:
    bool equals (const StrokeNode& m) const throws();

    /**
     * Compute a hashvalue for this stroke. 
     * @return a hashvalue
     */
  public:
    Int hashValue () const throws();

    /**
     * Check the two strokes are the same.
     * @param f a strokes 
     * @return true if this and f are the same stroke
     */
  public:
    inline bool operator==(const StrokeNode& f) const throws()
    { return equals(f); }
      
    /**
     * Check the two strokes are different.
     * @param f a strokes 
     * @return true if this and f are not the same stroke
     */
  public:
    inline bool operator!=(const StrokeNode& f) const throws()
    { return !equals(f); }

	
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();

    /** The emissive color */
  private:
    Color _color;

    /** The stroke width */
  private:
    double _width;
  };
}

#endif
