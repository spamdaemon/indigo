#ifndef _INDIGO_TRIANGLESET_H
#define _INDIGO_TRIANGLESET_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _INDIGO_TRIANGLES_H
#include <indigo/Triangles.h>
#endif

#include <vector>

namespace indigo {

  /**
   * This graphic is a triangles container. All triangleset
   * in this container a rendered with the same attributes.
   */
  class TriangleSet : public Shapes<Triangles> {
    TriangleSet& operator=(const TriangleSet&);

    /** Default constructor */
  public:
    inline TriangleSet () throws()
    {}
	
    /**
     * Create a triangleset consisting of a single triangle.
     * @param a a triangle vertex
     * @param b a triangle vertex
     * @param c a triangle vertex
     */
  public:
    TriangleSet (const Point& a, const Point& b, const Point& c) throws();

    /**
     * Create a triangleset with a single triangles. 
     * @param p a single triangles objects
     */
  public:
    TriangleSet (const Triangles& p) throws();
      
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  private:
    TriangleSet (const TriangleSet& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~TriangleSet () throws();
   
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
	
  };
}
#endif
