#ifndef _INDIGO_SEPARATOR_H
#define _INDIGO_SEPARATOR_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

namespace indigo {
   /**
    * This node can be used to backup the current rendering state before rendering the child. Once
    * the child node has been rendered, the previous state is returned.
    */
   class Separator : public StateNode
   {

         Separator(const Separator&);
         Separator& operator=(const Separator&);

         /** Create a new separator node. */
      public:
         Separator()throws();

         /**
          * Create a new separator node with an optional child.
          * @param c a child node
          */
      public:
         Separator(const ::timber::Pointer< Node>& c)throws();

         /** Destructor */
      public:
         ~Separator()throws();

         /**
          * Set the new child node.
          * @param c a child node
          */
      public:
         void setNode(const ::timber::Pointer< Node>& c)throws();

         /**
          * Get the separator value.
          * @return a value between 0 (transparent) and 1 (opaque)
          */
      public:
         inline ::timber::Pointer< Node> node() const throws() {return _child;}

         /**
          * Accept the specified visitor
          * @param v a visitor
          */
      public:
         void acceptVisitor(GfxVisitor& v) const throws();

      private:
         ::timber::Pointer< Node> _child;
   };
}
#endif
