#include <indigo/Circles.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  Circles::Circles (const Point& a, double r) throws()
  : Shapes<Circle>(Circle(a,r)) {}
      
  Circles::Circles (const Circle& p) throws()
  : Shapes<Circle>(p) {}
      
  Circles::Circles (const Circles& g) throws()
  : Shapes<Circle>(g) {}
  
  Circles::~Circles () throws()
  {}
  
  void Circles::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
