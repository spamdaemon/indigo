#include <indigo/PickInfo.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  
  
  PickInfo::~PickInfo () throws()
  {}

  
  void PickInfo::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
  
}
