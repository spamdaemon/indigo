#ifndef _INDIGO_POLYGONS_H
#define _INDIGO_POLYGONS_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _INDIGO_POLYGON_H
#include <indigo/Polygon.h>
#endif

#ifndef _INDIGO_RECTANGLE_H
#include <indigo/Rectangle.h>
#endif

#include <vector>

namespace indigo {
  
  /**
   * This graphic is a polygon container. All polygons
   * in this container a rendered with the same attributes.
   */
  class Polygons : public Shapes<Polygon> {
    Polygons&operator=(const Polygons&);

    /** Default constructor */
  public:
    inline Polygons () throws()
    {}
	
    /**
     * Create a polygons consisting of a single triangle.
     * @param a a triangle vertex
     * @param b a triangle vertex
     * @param c a triangle vertex
     */
  public:
    Polygons (const Point& a, const Point& b, const Point& c) throws();
      
    /**
     * Create a polygons consisting of a single rectangle.
     * @param pt the top left corner of the rectangle
     * @param w the width
     * @param h the height
     */
  public:
    Polygons (const Point& pt, double w, double h) throws();

    /**
     * Create a polygons consisting of a single rectangle.
     * @param rect a rectangle
     */
  public:
    Polygons (const Rectangle& rect) throws();

    /**
     * Create a polygons with a single polygon. 
     */
  public:
    Polygons (const Polygon& p) throws();
      
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  private:
    Polygons (const Polygons& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~Polygons () throws();

      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
	
  };
}
#endif
