#ifndef _INDIGO_NODES_H
#define _INDIGO_NODES_H

#include <indigo/AlphaNode.h>
#include <indigo/AntiAliasingModeNode.h>
#include <indigo/CachableNode.h>
#include <indigo/Circles.h>
#include <indigo/CompositeNode.h>
#include <indigo/Coordinates2D.h>
#include <indigo/Coordinates3D.h>
#include <indigo/Ellipses.h>
#include <indigo/Group.h>
#include <indigo/Icon.h>
#include <indigo/NurbsCurves.h>
#include <indigo/Paths.h>
#include <indigo/PickInfo.h>
#include <indigo/Points.h>
#include <indigo/Polygons.h>
#include <indigo/PolyLines.h>
#include <indigo/Raster.h>
#include <indigo/Regions.h>
#include <indigo/SolidFillNode.h>
#include <indigo/Separator.h>
#include <indigo/StrokeNode.h>
#include <indigo/Switch.h>
#include <indigo/Text.h>
#include <indigo/ThreadSafeNode.h>
#include <indigo/Transform2D.h>
#include <indigo/TransformMode.h>
#include <indigo/TriangleSet.h>
#include <indigo/ViewTransform2D.h>

#endif
