#ifndef _INDIGO_ANTIALIASINGMODE_H
#define _INDIGO_ANTIALIASINGMODE_H

#include <iosfwd>

namespace indigo {

   /**
    * This class is used to define the various levels of anti-aliasing supported. Note that most likely
    * BEST and FAST anti-aliasing are going to be the same.
    * @note these modes are somewhat subject to change and may be extended in the future.
    */
   enum class AntiAliasingMode
   {
      /** A fast anti-aliasing mode is requested. */
      FAST,

      /** The best anti-aliasing mode is requested */
      BEST,

      /** No anti-aliasing is wanted */
      NONE
   };
}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::AntiAliasingMode& c);
#endif
