#ifndef _INDIGO_TRANSFORM_H
#define _INDIGO_TRANSFORM_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

namespace indigo {

  /**
   * This node type represents an transform that acts on points
   * in a linear or possibly non-linear way. Generally, transforms
   * take into account prior transforms.
   */
  class Transform : public StateNode {
    Transform(const Transform&);
    Transform&operator=(const Transform&);

    /** Default constructor */
  protected:
    inline Transform () throws()
    {}
	
    /**  Destroy this graphic handle */
  public:
    ~Transform () throws();

    /**
     * Transform the specified point.
     * @param pt a point
     * @return the transformed point
     */
  public:
    virtual Point transformPoint (const Point& pt) const throws() = 0;
  };
}
 
#endif
