#ifndef _INDIGO_ELLIPSES_H
#define _INDIGO_ELLIPSES_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _INDIGO_ELLIPSE_H
#include <indigo/Ellipse.h>
#endif

#include <vector>

namespace indigo {
  /**
   * This graphic is a ellipse container. All ellipses
   * in this container a rendered with the same attributes.
   */
  class Ellipses : public Shapes<Ellipse> {
    Ellipses&operator=(const Ellipses&);

    /** Default constructor */
  public:
    inline Ellipses () throws()
    {}
	
    /**
     * Create a ellipses consisting of a single ellipse.
     * @param c the ellipse center
     * @param r1 the length of one axis
     * @param r2 the length of the second axis
     * @param inclination the angle between r1 and the positive x-axis
     * @pre REQUIRE_GREATER_OR_EQUAL(r1,0)
     * @pre REQUIRE_GREATER_OR_EQUAL(r2,0)
     */
  public:
    Ellipses (const Point& c, double r1, double r2, double inclination=0.0) throws();
      
    /**
     * Create a ellipses with a single ellipse. 
     */
  public:
    Ellipses (const Ellipse& p) throws();
      
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  private:
    Ellipses (const Ellipses& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~Ellipses () throws();

      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
