#ifndef _INDIGO_COORDINATES3D_H
#define _INDIGO_COORDINATES3D_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_GFXEVENT_H
#include <indigo/GfxEvent.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <vector>

namespace indigo {

  /**
   * This graphic is a point container. All points2d
   * in this container a rendered with the same attributes.
   */
  class Coordinates3D : public Node {

    /** Default constructor */
  public:
    inline Coordinates3D () throws()
    {}
	
    /**
     * Create a points2d consisting of a single point.
     * @param x the x coordinate ofa point
     * @param y the y coordinate of a point
     */
  public:
    Coordinates3D (double x, double y) throws();
	
    /**
     * Create a points2d consisting of a single point.
     * @param sh a point
     */
  public:
    Coordinates3D (const Point& sh) throws();
      
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  public:
    Coordinates3D (const Coordinates3D& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~Coordinates3D () throws();

    /**
     * Get the number of children.
     * @return the number of children
     */
  public:
    inline size_t count () const throws()
    { return _points.size(); }
	
    /**
     * Get the point at the specified index.
     * @param pos an index
     * @pre REQUIRE_RANGE(pos,0,count()-1)
     * @return the point at the specified index
     */
  public:
    inline Point point (size_t pos) const throws()
    { return _points[pos]; }
      
    /**
     * Get the point at the specified index.
     * @param pos a point index
     * @return a point
     */
  public:
    inline Point operator[] (size_t pos) const throws()
    { return point(pos); }

    /**
     * Set the point at the specified index.
     * @param p a point
     * @param pos the new point's position
     */
  public:
    void set (const Point& p, size_t pos) throws();

    /**
     * Insert a point at the specified position. Calls fireEvent()
     * if a change was made.
     * @param p the new point 
     * @param pos a position
     * @pre REQUIRE_RANGE(pos,0,count())
     */
  public:
    void insert (const Point& p, size_t pos) throws();

    /**
     * Add a child. Calls fireEvent().
     * @param p the new point 
     */
  public:
    inline void add (const Point& p) throws()
    { set(p,count()); }

    /**
     * Remove the child at the specified position. Calls fireEvent().
     * @param pos a position
     * @pre REQUIRE_RANGE(pos,0,count()-1)
     * @return the point previously at the position
     */
  public:
    Point remove (size_t pos) throws();

    /** Remove all points2d. */
  public:
    void clear () throws();
      
    /**
     * Notify the observer.
     * @param pos the child position that has changed
     */
  private:
    inline void notifyChanged (size_t pos) throws()
    { notifyChange(); }
      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();

    /** The children */
  private:
    ::std::vector<Point> _points;
  };
}
#endif
 
