#include <indigo/Switch.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

   Switch::Switch()
   throws()
   : _visibleChild(invalidIndex())
   {}

   Switch::Switch(size_t n, bool separator)
   throws()
   : Group(n,separator),_visibleChild(invalidIndex()) {}

   Switch::~Switch()
   throws()
   {}

   void Switch::acceptVisitor(GfxVisitor& v) const
   throws()
   {  v.visit(*this);}

   ::timber::Pointer< Node> Switch::visibleNode() const
   throws()
   {
      if (invalidIndex() != _visibleChild && _visibleChild < nodeCount()) {
         return node(_visibleChild);
      }
      else {
         return ::timber::Pointer< Node>();
      }
   }

   void Switch::setVisibleChild(size_t child)
throws()
{
   if (child!=_visibleChild) {
      _visibleChild = child;
      notifyChange();
   }
}
}
