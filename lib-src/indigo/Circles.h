#ifndef _INDIGO_CIRCLES_H
#define _INDIGO_CIRCLES_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _INDIGO_CIRCLE_H
#include <indigo/Circle.h>
#endif

#include <vector>

namespace indigo {
  /**
   * This graphic is a circle container. All circles
   * in this container a rendered with the same attributes.
   */
  class Circles : public Shapes<Circle> {
    Circles&operator=(const Circles&);

    /** Default constructor */
  public:
    inline Circles () throws()
    {}
	
    /**
     * Create a circles consisting of a single triangle.
     * @param a the circle center
     * @param r the radius
     */
  public:
    Circles (const Point& a, double r) throws();
      
    /**
     * Create a circles with a single circle. 
     */
  public:
    Circles (const Circle& p) throws();
      
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  private:
    Circles (const Circles& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~Circles () throws();

      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
