#include <indigo/Points.h>
#include <indigo/GfxVisitor.h>

namespace indigo {

  Points::Points () throws()
  : _pointSize(0) {}
  
  Points::Points (const Point& p) throws()
  : Shapes<Point>(p),_pointSize(0) {}
  
  Points::Points (Int n, const Point* pts) throws()
  : Shapes<Point>(n,pts),_pointSize(0) {}
  
  Points::Points (const Points& g) throws()
  : Shapes<Point>(g),_pointSize(g._pointSize) {}
  
  Points::~Points () throws()
  {}
  
  void Points::setPointSize (Int sz) throws()
  {
    sz = ::std::max(sz,0); 
    if (sz!=_pointSize) {
      _pointSize = sz;
      notifyChange();
    }
  }
  
  void Points::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
