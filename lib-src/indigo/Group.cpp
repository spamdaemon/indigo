#include <indigo/Group.h>

namespace indigo {

   Group::Group()
   throws()
   : CompositeNode(true)
   {}

   Group::Group(bool separator)
   throws()
   : CompositeNode(separator) {}

   Group::Group(size_t n, bool separator)
   throws()
   : CompositeNode(n,separator) {}

   Group::Group(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& r)
   throws()
   : CompositeNode(l,r,false)
   {}

   Group::Group(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& r, bool sep)
   throws()
   : CompositeNode(l,r,sep)
   {
   }

         Group::Group(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& m,
               const ::timber::Pointer< Node>& r)
   throws()
   : CompositeNode(l,m,r,false) {}

   Group::Group(const ::timber::Pointer< Node>& l, const ::timber::Pointer< Node>& m,
         const ::timber::Pointer< Node>& r, bool sep)
   throws()
   : CompositeNode(l,m,r,sep) {}

   Group::Group(const Group& g)
   throws()
   : CompositeNode(g) {}

   Group::~Group()
   throws()
   {}
}
