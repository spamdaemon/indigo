#ifndef _INDIGO_COORDINATES2D_H
#define _INDIGO_COORDINATES2D_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif

#ifndef _INDIGO_GFXEVENT_H
#include <indigo/GfxEvent.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <vector>

namespace indigo {

  /**
   * This graphic is a point container. All coordinates2d
   * in this container a rendered with the same attributes.
   */
  class Coordinates2D : public StateNode {
    /** The internally used point */
  private:
    struct Pt { 
      inline Pt (double px, double py) throws() : _x(px),_y(py) {}
      double _x, _y; 
    };

    /** Default constructor */
  public:
    inline Coordinates2D () throws()
    {}
	
    /**
     * Create a coordinates2d consisting of a single point.
     * @param x the x coordinate ofa point
     * @param y the y coordinate of a point
     */
  public:
    Coordinates2D (double x, double y) throws();
	
    /**
     * Create a coordinates2d consisting of a single point.
     * @param sh a point
     */
  public:
    Coordinates2D (const Point& sh) throws();
      
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     */
  public:
    Coordinates2D (const Coordinates2D& g) throws();
      
    /**  Destroy this graphic handle */
  public:
    ~Coordinates2D () throws();

    /**
     * Get the number of children.
     * @return the number of children
     */
  public:
    inline size_t count () const throws()
    { return _points.size(); }
	
    /**
     * Get the point at the specified index.
     * @param pos an index
     * @pre REQUIRE_RANGE(pos,0,count()-1)
     * @return the point at the specified index
     */
  public:
    inline Point point (size_t pos) const throws()
    { 
      const Pt p(_points[pos]);
      return Point(p._x,p._y);
    }
      
    /**
     * Get the point at the specified index.
     * @param pos a point index
     * @return a point
     */
  public:
    inline Point operator[] (size_t pos) const throws()
    { return point(pos); }

    /**
     * Set the point at the specified index.
     * @param p a point
     * @param pos the new point's position
     */
  public:
    void set (const Point& p, size_t pos) throws();

    /**
     * Insert a point at the specified position. Calls fireEvent()
     * if a change was made.
     * @param p the new point 
     * @param pos a position
     * @pre REQUIRE_RANGE(pos,0,count())
     */
  public:
    void insert (const Point& p, size_t pos) throws();

    /**
     * Add a child. Calls fireEvent().
     * @param p the new point 
     */
  public:
    inline void add (const Point& p) throws()
    { set(p,count()); }

    /**
     * Remove the child at the specified position. Calls fireEvent().
     * @param pos a position
     * @pre REQUIRE_RANGE(pos,0,count()-1)
     * @return the point previously at the position
     */
  public:
    Point remove (size_t pos) throws();

    /** Remove all coordinates2d. */
  public:
    void clear () throws();

    /**
     * Notify the observer.
     * @param pos the child position that has changed
     */
  private:
    inline void notifyChanged (size_t pos) throws()
    { notifyChange(); }
      
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();

    /** The children */
  private:
    ::std::vector<Pt> _points;
  };
}
#endif
