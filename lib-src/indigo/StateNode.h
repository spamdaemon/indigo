#ifndef _INDIGO_STATENODE_H
#define _INDIGO_STATENODE_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif


namespace indigo {

  /**
   * This graphic is a material container. All materials
   * in this container a rendered with the same attributes.
   */
  class StateNode : public Node {
        CANOPY_BOILERPLATE_PREVENT_COPYING(StateNode);

    /** Default constructor */
  protected:
    inline StateNode () throws()
    {}
    
    /**  Destroy this graphic handle */
  public:
    ~StateNode () throws();
  };
}

#endif
