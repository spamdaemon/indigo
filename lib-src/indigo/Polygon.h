#ifndef _INDIGO_POLYGON_H
#define _INDIGO_POLYGON_H

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <vector>

namespace indigo {

   /**
    * This class represents an immutable polygon. Polygons may be efficiently
    * copied, because they can share the set of vertices.
    * @note Polygons may have 0 vertices to indicate that the polygon is empty.
    */
   class Polygon
   {
         /** The polygon type */
      public:
         enum ShapeType
         {
            CONVEX_SHAPE, //< a convex polygon
            CONCAVE_SHAPE, //< a concave polygon
            UNKNOWN_SHAPE
         //< a polygon of unknown shape
         };

         /** The orientation */
      public:
         enum Orientation
         {
            CW_ORIENTATION, //< a clockwise oriented polygon
            CCW_ORIENTATION,//< a counter clockwise oriented polygon
            UNKNOWN_ORIENTATION,
         //< the orientation is unknown
         };

         /**
          * Default constructor. Creates an empty polygon without vertices.
          */
      public:
         Polygon()throws();

         /**
          * Create a new polygon. A polygon with
          * 3 vertices will be a CONVEX polygon, regardless
          * of the specified shape type.
          * @param n the number of points
          * @param pts an array of points
          * @param st a shape type
          * @pre REQUIRE_GREATER(n,0)
          */
      public:
         Polygon(size_t n, const Point* pts, ShapeType st)throws();

         /**
          * Create a new polygon.
          * @param n the number of points
          * @param pts an array of points
          * @pre REQUIRE_GREATER(n,0)
          */
      public:
         Polygon(size_t n, const Point* pts)throws();

         /**
          * Create a triangle.
          * @param a the first vertex
          * @param b the second vertex
          * @param c the third vertex
          */
      public:
         Polygon(const Point& a, const Point& b, const Point& c)throws();

         /**
          * Create an axis aligned rectangle.
          * @param p1 the top-left rectangle corner
          * @param p2 the bottom-right rectangle corner
          * @return a rectangle polygon
          */
      public:
         static Polygon createRectangle(const Point& p1, const Point& p2)throws();

         /**
          * Create an axis-aligned rectangle.
          * @param pt top-left corner
          * @param w the width
          * @param h the height
          * @return a rectangle polygon
          */
      public:
         static Polygon createRectangle(const Point& pt, double w, double h)throws();

         /**
          * Get the shape type
          * @return the shape type
          */
      public:
         inline ShapeType shapeType() const throws()
         {  return _shapeType;}

         /**
          * Get the orientation.
          * @return the polygon orientation
          */
      public:
         inline Orientation orientation() const throws()
         {  return _orientation;}

         /**
          * Get the number of points. The result should be tested againsts 0.
          * @return the number of points in the polygon
          */
      public:
         inline size_t vertexCount() const throws()
         {  return _vertices.size();}

         /**
          * Get the specified vertex.
          * @param pos a vertex index
          * @return a reference to the specified vertex
          */
      public:
         inline const Point& vertex(size_t pos) const throws()
         {  return _vertices[pos];}

         /**
          * Get  the vertices.
          * @return a vector of points
          */
      public:
         inline const ::std::vector< Point>& vertices() const throws() {return _vertices;}

         /** The point coordinates */
      private:
         ::std::vector< Point> _vertices;

         /** The shape type */
      private:
         ShapeType _shapeType;

         /** The orientation */
      private:
         Orientation _orientation;
   };

}
#endif
