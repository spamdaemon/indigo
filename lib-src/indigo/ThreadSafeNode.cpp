#include <indigo/ThreadSafeNode.h>
#include <indigo/SceneGraph.h>
#include <indigo/GfxObserver.h>
#include <canopy/mt/Mutex.h>
#include <canopy/mt/MutexGuard.h>

namespace indigo {

   ThreadSafeNode::ThreadSafeNode()
   throws()
   {}

   ThreadSafeNode::~ThreadSafeNode()
   throws()
   {}
   ::timber::Reference< Node> ThreadSafeNode::create()
   throws() {return create( ::timber::Pointer< Node>());}

   ::timber::Reference< Node> ThreadSafeNode::create(const ::timber::Pointer< Node>& child)
throws()
{
   struct Impl : public ThreadSafeNode, private SceneGraph, private GfxObserver {
      Impl(const ::timber::Pointer< Node>& c) throws()
      {
         SceneGraph::setRootNode(c);
      }

      ~Impl() throws() { SceneGraph::setRootNode(::timber::Pointer<Node>()); }

      void acceptVisitor(GfxVisitor& v) const
      throws()
      {
         ::timber::Pointer< Node> c = child();
         if (c) {
            c->accept(v);
         }
      }

      void setChild(const ::timber::Pointer< Node>& c)throws()
      {
         bool changed=false;
         {
            ::canopy::mt::MutexGuard< ::canopy::mt::Mutex> G(_mutex);
            ::timber::Pointer<Node> n = SceneGraph::rootNode();
            if (c!=n) {
               SceneGraph::setRootNode(c);
               changed = true;
            }
         }
         if (changed) {
            notifyChange();
         }
      }

      ::timber::Pointer< Node> child() const throws()
      {
         ::timber::Pointer< Node> res;
         {
            ::canopy::mt::MutexGuard< ::canopy::mt::Mutex> G(_mutex);
            res = SceneGraph::rootNode();
         }
         return res;
      }

      GfxObserver* createObserver (Node&) throws() {return this;}

      void gfxChanged (const GfxEvent& e) throws()
      {
         Node::fireEvent(e);
      }

      /** The mutex */
      private:
      ::canopy::mt::Mutex _mutex;
   };
   Impl* res = new Impl(child);
   return res;
}
}

