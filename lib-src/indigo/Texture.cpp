#include <indigo/Texture.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
  Texture::Texture () throws()
  {}
 
  Texture::~Texture () throws()
  {
  }
  
  void Texture::acceptVisitor (GfxVisitor& v) const throws()
  {
    v.visit(*this);
  }
}
