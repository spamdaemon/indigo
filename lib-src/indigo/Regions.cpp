#include <indigo/Regions.h>
#include <indigo/GfxVisitor.h>

using namespace ::std;
using namespace ::timber;
using namespace ::tikal;

namespace indigo {

  Regions::Regions (const Reference<Region2D>& r) throws()
  { add(r); }
  
  Regions::Regions (const vector<Reference<Region2D> >& regs) throws()
  {
    for (size_t i=0;i<regs.size();++i) {
      add(regs[i]);
    }
  }
  
  Regions::Regions (const vector<Pointer<Region2D> >& regs) throws()
  {
    for (size_t i=0;i<regs.size();++i) {
      if (regs[i]) {
	add(regs[i]);
      }
    }
  }
  
  Regions::Regions (const Regions& g) throws()
  : Shapes<Pointer<Region2D> >(g) {}
  
  Regions::~Regions () throws()
  {}
  
  void Regions::acceptVisitor (GfxVisitor& v) const throws()
  { v.visit(*this); }
}
