#include <indigo/render/NodeRenderer.h>
#include <indigo/render/Context.h>
#include <indigo/render/PaintNodeVisitor.h>

using namespace ::timber;

namespace indigo {
   namespace render {
      namespace {

         struct NodeRendererImpl : public NodeRenderer {
            ~NodeRendererImpl()throws() {}

            void render(const ::timber::Reference< Node>& node, Context& ctx)throws()
            {
               PaintNodeVisitor v(ctx);
               node->accept(v);
            }
         };
      }

      NodeRenderer::NodeRenderer()
      throws()
      {}

      NodeRenderer::~NodeRenderer()
      throws()
      {}

      ::std::unique_ptr< NodeRenderer> NodeRenderer::create()
      {
         return ::std::unique_ptr< NodeRenderer>(new NodeRendererImpl());
      }

   }
}
