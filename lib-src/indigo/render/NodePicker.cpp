#include <indigo/render/NodePicker.h>
#include <indigo/render/PaintNodeVisitor.h>
#include <timber/logging.h>
#include <vector>

using namespace ::timber;
using namespace ::timber::logging;

namespace indigo {
   namespace render {
      namespace {

         static Log logger()
         {
            return "indigo.render.NodePicker";
         }

         struct Visitor : public PaintNodeVisitor
         {
               ~Visitor()throws()
               {
               }

               Visitor(PickContext& ctx) :
                  PaintNodeVisitor(ctx.renderContext()), _context(ctx),_currentNode(0),_currentIndex(0), _nPickInfos(0)
               {
               }

               template <class T>
               void visitBase(const T& g)throws()
               {
                  PaintNodeVisitor::visit(g);
                  // did current node affect the pick
                  if (_context.testAndReset()) {
                     _currentPath = _pickedPath;
                     _currentNode = &g;
                     _currentIndex =0;
                  }
               }

               template <class T>
               void paintBase(const T& g, size_t index, Context::PaintMode mode)throws()
               {
                  PaintNodeVisitor::paintShape(g,index,mode);
                  // did current node affect the pick
                  if (_context.testAndReset()) {
                     _currentPath = _pickedPath;
                     _currentNode = &g;
                     _currentIndex = index;
                  }
               }

               void visit(const PickInfo& p)throws()
               {
                  // no need to call base class, but we do it anyways
                  PaintNodeVisitor::visit(p);

                  ++_nPickInfos;
                  _pickedPath.push_back(&p);
               }

               void visit(const Icon& g)throws()
               {
                  visitBase(g);
               }

               void visit(const Raster& g)throws()
               {
                  visitBase(g);
               }

               void visit(const Text& g)throws()
               {
                  visitBase(g);
               }

               void paintShape(const Circles& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const Ellipses& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const PolyLines& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const Polygons& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const Points& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const NurbsCurves& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const TriangleSet& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const Regions& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }
               void paintShape(const Paths& shapes, size_t index, Context::PaintMode mode)throws()
               {
                  paintBase(shapes,index,mode);
               }

               void saveState()throws()
               {
                  // push a NULL onto the stack; this will serve as our marker
                  _pickedPath.push_back(0);
                  PaintNodeVisitor::saveState();
               }

               void restoreState()throws()
               {
                  PaintNodeVisitor::restoreState();

                  // pop until the last marker is found, i.e. NULL is found
                  while(_pickedPath.back()!=0) {
                     _pickedPath.pop_back();
                     --_nPickInfos;
                  }
                  // this pop's the null marker, which means the state is
                  // restored to before the last saveState call
                  _pickedPath.pop_back();
               }

               /**
                * Get the result of the pick
                * @param pickinfo a pickinfo output
                * @param node a picked shape
                * @param index the shape index
                */
            public:
               NodePicker::Pick getPickResult () const throws()
               {
                  NodePicker::Pick res;
                  res.shape = _currentIndex;
                  res.node = _currentNode;

                  for(auto i=_currentPath.rbegin();i!=_currentPath.rend();++i) {
                     if (*i != 0) {
                        res.info = *i;
                        break;
                     }
                  }

                  return res;
               }

               /** The pick context */
            private:
               PickContext& _context;

               /** The set of picked nodes currently active */
            private:
               ::std::vector< const PickInfo*> _pickedPath;

               /** The most recent picknodes that lead to a picked location */
            private:
               ::std::vector< const PickInfo*> _currentPath;

               /** A node */
            private:
               const Node* _currentNode;

               /** The current index (0 if N/A) */
            private:
               size_t _currentIndex;

               /** The number of non-marker pick infos */
            private:
               size_t _nPickInfos;
         };

      }

      NodePicker::NodePicker()
      throws()
      {}

      NodePicker::~NodePicker()
      throws()
      {}

      ::std::unique_ptr< NodePicker> NodePicker::create()
      {
         struct _ : public NodePicker
         {
               _()throws() {}
               ~_()throws() {}

               Pick pick(const ::timber::Reference< Node>& node, PickContext& ctx)throws()
               {
                  Visitor v(ctx);
                  node->accept(v);
                  return v.getPickResult();
               }

         };
         return ::std::unique_ptr< NodePicker>(new _());
      }

   }
}
