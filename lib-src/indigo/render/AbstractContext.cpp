#include <indigo/render/AbstractContext.h>
#include <indigo/NURBS.h>
#include <tikal/TriangleStrip2D.h>
#include <tikal/Region2D.h>
#include <tikal/Path2D.h>
#include <tikal/Contour2D.h>
#include <tikal/Area2D.h>
#include <tikal/BoundingBox2D.h>
#include <timber/media/ImageBuffer.h>
#include <cmath>

using namespace ::tikal;
using namespace ::timber;
using namespace ::timber::logging;

#define ABSTRACT_CONTEXT_ENABLE_ALL_LOGGING 0
namespace indigo {
   namespace render {

      AbstractContext::AbstractContext(double x, double y, double w, double h, const Bounds& xdeviceBounds)
      throws()
            : _deviceBounds(xdeviceBounds), _alpha(0), _logger("indigo.render.AbstractContext")
      {
         _minx = ::std::min(x, x + w);
         _miny = ::std::min(y, y + h);
         _maxx = ::std::max(x, x + w);
         _maxy = ::std::max(y, y + h);
      }

      AbstractContext::AbstractContext(const AbstractContext& other)
      throws()
            : _deviceBounds(other._deviceBounds), _alpha(other._alpha), _transform(other._transform), _logger(
                  "indigo.render.AbstractContext"), _minx(other._minx), _miny(other._miny), _maxx(other._maxx), _maxy(
                  other._maxy)
      {
         // we're not copying over the points array; no point in doing that
      }

      AbstractContext::~AbstractContext()
      throws()
      {
      }
      void AbstractContext::ensurePointSize(size_t n)
      throws()
      {
         if (_points.size() < n) {
            _points.resize(2 * n);
         }
      }
      void AbstractContext::setAlpha(double a)
      throws()
      {
         _alpha = a;
      }

      void AbstractContext::updateAlpha(double a)
      throws()
      {
         setAlpha(_alpha * a);
      }

      void AbstractContext::setTransform(const AffineTransform2D& t)
      throws()
      {
         _transform = t;
         _clip = nullptr;
      }

      void AbstractContext::updateTransform(const AffineTransform2D& local)
      throws()
      {
         _transform = AffineTransform2D(_transform, local);
         _clip = nullptr;
      }

      const Point* AbstractContext::transformPoints(const Point* pts, size_t n)
      throws ()
      {
         if (_transform.isIdentityTransform()) {
            return pts;
         }
         if (_points.size() < n) {
            _points.resize(2 * n);
         }
         const Point* p = pts;
         const Point* end = p + n;
         auto i = _points.begin();
         while (p != end) {
            *i = _transform.transformPoint(*p);
            ++p;
            ++i;
         }
         return &_points[0];
      }

      const Point* AbstractContext::transformPoints(const Point2D* pts, size_t n)
      throws ()
      {
         ensurePointSize(n);
         const Point2D* p = pts;
         const Point2D* end = p + n;
         auto i = _points.begin();
         if (_transform.isIdentityTransform()) {
            *i = Point(p->x(), p->y());
            ++p;
            ++i;
         }
         else {
            while (p != end) {
               *i = _transform.transformPoint(p->x(), p->y());
               ++p;
               ++i;
            }
         }
         return &_points[0];
      }

      const Point* AbstractContext::generateEllipsePoints(double x, double y, double M, double m, double inclination,
            size_t& n)
            throws ()
      {
         // for now, we'll just generate 36 points
         size_t nPoints = 36;
         ensurePointSize(nPoints);

         assert((nPoints & 1) == 0);

         const double PI = 3.14159265358979323846;
         const double di = PI / (nPoints / 2);

         AffineTransform2D t;
         t.rotate(inclination);
         t.translate(x, y);
         t = AffineTransform2D(_transform, t);
         auto j = _points.begin();

         for (double i = 0; i < 2 * PI; i += di) {
            *j = t.transformPoint(::std::cos(i) * M, ::std::sin(i) * m);
            ++j;
         }
         // just in case, recompute the size
         n = (j - _points.begin());
         return &_points[0];
      }

      void AbstractContext::paintRectangle(PaintMode mode, double x, double y, double w, double h)
      throws()
      {
         Point pts[4];

         double minx, miny, maxx, maxy;
         if (w < 0) {
            minx = x + w;
            maxx = x;
         }
         else {
            minx = x;
            maxx = x + w;
         }
         if (h < 0) {
            miny = y + w;
            maxy = y;
         }
         else {
            miny = y;
            maxy = y + w;
         }

         pts[0] = transformPoint(minx, miny);
         pts[1] = transformPoint(minx, maxy);
         pts[2] = transformPoint(maxx, maxy);
         pts[3] = transformPoint(maxx, miny);

         paintTransformedPolygon(mode, 4, pts, true);
      }
      void AbstractContext::paintPoints(PaintMode mode, size_t n, const Point* pts, double pointSize)
      throws()
      {
         pts = transformPoints(pts, n);
         paintTransformedPoints(mode, n, pts, pointSize);
      }

      void AbstractContext::paintPolygon(PaintMode mode, size_t n, const Point* pts, bool convex)
      throws ()
      {
         pts = transformPoints(pts, n);
         paintTransformedPolygon(mode, n, pts, convex);
      }

      void AbstractContext::paintPolyline(PaintMode mode, size_t n, const Point* pts, bool close)
      throws ()
      {
         pts = transformPoints(pts, n);
         paintTransformedPolyline(mode, n, pts, close);
      }
      void AbstractContext::paintLine(PaintMode mode, double x0, double y0, double x1, double y1)
      throws()
      {
         Point pts[2];
         pts[0] = transformPoint(x0, y0);
         pts[1] = transformPoint(x1, y1);
         paintTransformedPolyline(mode, 2, pts, false);
      }

      void AbstractContext::paintNURBS(PaintMode mode, const NURBS& c)
      throws()
      {
         size_t nPoints = 1 + c.controlPointCount() * 4;
         ensurePointSize(nPoints);
         double u = c.lowerBound();
         double v = c.upperBound();
         double du = (v - u) / (nPoints - 1);
         auto j = _points.begin();
         for (size_t i = 0; i < nPoints; ++i) {
            *j = transformPoint(c.pointAt(u));
            ++j;
            u += du;
            if (u > v) {
               u = v;
            }
         }

         paintTransformedPolyline(mode, j - _points.begin(), &_points[0], false);
      }

      void AbstractContext::paintEllipse(PaintMode mode, double x, double y, double semiMajor, double semiMinor,
            double inclination)
            throws ()
      {
         size_t n = 0;
         const Point* pts = generateEllipsePoints(x, y, semiMajor, semiMinor, inclination, n);
         paintTransformedPolygon(mode, n, pts, true);
      }

      void AbstractContext::paintTriangles(PaintMode mode, size_t n, const Point* pts)
      throws()
      {
         pts = transformPoints(pts, n);
         paintTransformedTriangles(mode, n, pts);
      }

      void AbstractContext::paintTriStrip(PaintMode mode, size_t n, const Point* pts)
      throws()
      {
         pts = transformPoints(pts, n);
         paintTransformedTriStrip(mode, n, pts);
      }

      void AbstractContext::paintTransformedTriangles(PaintMode mode, size_t n, const Point* pts)
      throws()
      {

         auto end = pts + n - 2;
         for (auto i = pts; i < end; i += 3) {
            paintTransformedPolygon(mode, 3, pts, true);
         }
      }

      void AbstractContext::paintTransformedTriStrip(PaintMode mode, size_t n, const Point* pts)
      throws()
      {
         Point vtx[3];

         for (size_t i = 0; i < n - 2; ++i) {
            if ((i % 2) == 0) {
               vtx[0] = pts[i];
               vtx[1] = pts[i + 1];
            }
            else {
               vtx[0] = pts[i + 1];
               vtx[1] = pts[i];
            }
            vtx[2] = pts[i + 2];
            paintTransformedTriangles(mode, 3, vtx);
         }
      }

      void AbstractContext::paintTriFan(PaintMode mode, size_t n, const Point* pts)
      throws ()
      {
         pts = transformPoints(pts, n);
         Point vtx[3];
         size_t i = 0;
         vtx[0] = pts[i];
         while (i < n - 2) {
            vtx[1] = pts[i++];
            vtx[2] = pts[i++];
            paintTransformedTriangles(mode, 3, vtx);
         }
      }

      void AbstractContext::renderTriangleStrips(PaintMode mode, Region2D& strips)
      throws()
      {
         for (size_t i = 0, sz = strips.areaCount(); i < sz; ++i) {
            Pointer< TriangleStrip2D> r = strips.area(i).tryDynamicCast< TriangleStrip2D>();
            if (r) {
               const ::std::vector< Point2D>& vtx = r->stripVertices();
               const Point* pts = transformPoints(&vtx[0], vtx.size());
               paintTransformedTriStrip(mode, vtx.size(), pts);
            }
         }
      }

      /**
       * Render a contour consisting of possibly many.
       * @param r a region
       * @param doFill if true, the fill the region
       * @param doDraw if true, then draw the region
       */
      bool AbstractContext::renderPolygon(PaintMode mode, Polygon2D& r)
      throws()
      {
         // create a tri-strip
         if (_alpha != 1 && (mode & (FILL))) {
            Pointer< Region2D> strips = r.toTriangleStrips();
            if (strips) {
               renderTriangleStrips(mode & FILL, *strips);
               if ((mode & DRAW) == 0) {
                  return true;
               }
               mode = mode & ~FILL;
            }
         }

         ensurePointSize(r.vertexCount());
         auto j = _points.begin();

         for (size_t i = 0, sz = r.vertexCount(); i != sz; ++i, ++j) {
            *j = transformPoint(r.vertex(i));
         }
         paintTransformedPolygon(mode, j - _points.begin(), &_points[0], false);
         return true;
      }

      /**
       * Render a contour consisting of possibly many.
       * @param r a region
       * @param doFill if true, the fill the region
       * @param doDraw if true, then draw the region
       */
      bool AbstractContext::renderContour(PaintMode mode, Contour2D& r)
      throws()
      {
         Polygon2D* polygon = dynamic_cast< Polygon2D*>(&r);
         if (polygon) {
            return renderPolygon(mode, *polygon);
         }

         if (r.maxDegree() > 1) {
            // ignore contours of degree > 1
            return false;
         }

         ensurePointSize(r.segmentCount() + 2);

         auto j = _points.begin();
         Point2D pts[2];
         for (size_t i = 0; i < r.segmentCount(); ++i, ++j) {
            r.segment(i, pts);
            *j = transformPoint(pts[0]);
         }
         paintTransformedPolygon(mode, j - _points.begin(), &_points[0], false);
         return true;
      }

      void AbstractContext::renderArea(PaintMode mode, Area2D& r)
      throws()
      {
         if (!renderContour(mode, *r.outsideContour())) {
            return;
         }
         if ((mode & DRAW)) {
            // internal contours are only rendered when drawing, not when filling
            for (size_t i = 0, n = r.contourCount(); i < n; ++i) {
               renderContour(DRAW, *r.insideContour(i));
            }
         }
      }

      Reference< Region2D> AbstractContext::createClipRegion()
      throws()
      {
         const AffineTransform2D inv = _transform.inverse();
         Point pt;
         // make the bounds just 1 pixel wider to eliminate clipping issues
         // at the boundaries
         pt = inv.transformPoint(_minx - 1, _miny - 1);
         BoundingBox2D bbox(pt.x(), pt.y());
         pt = inv.transformPoint(_maxx + 1, _miny);
         bbox.insert(pt.x(), pt.y());
         pt = inv.transformPoint(_maxx + 1, _maxy + 1);
         bbox.insert(pt.x(), pt.y());
         pt = inv.transformPoint(_minx - 1, _maxy + 1);
         bbox.insert(pt.x(), pt.y());
         return Polygon2D::create(bbox);
      }

      bool AbstractContext::isVisible(const BoundingBox2D& bbox, const Pointer< Region2D>& curClip,
            Pointer< Region2D>& clip)
            throws()
      {
         const Point tl = transformPoint(bbox.minx(), bbox.maxy());
         const Point tr = transformPoint(bbox.maxx(), bbox.maxy());
         const Point bl = transformPoint(bbox.minx(), bbox.miny());
         const Point br = transformPoint(bbox.maxx(), bbox.miny());

         // here we're in the screens coordinate frame!!!
         double minX = ::std::min(tl.x(), ::std::min(tr.x(), ::std::min(bl.x(), br.x())));
         double minY = ::std::min(tl.y(), ::std::min(tr.y(), ::std::min(bl.y(), br.y())));
         double maxX = ::std::max(tl.x(), ::std::max(tr.x(), ::std::max(bl.x(), br.x())));
         double maxY = ::std::max(tl.y(), ::std::max(tr.y(), ::std::max(bl.y(), br.y())));

         // this the clip against the visible region
         if (maxX < _minx || minX > _maxx || maxY < _miny || minY > _maxy) {
#if 0
            LogEntry(_logger).debugging() << "Region not visible " << bbox << ::std::endl
            << minX << ", " << minY << " --> " << maxX << ", " << maxY << ::std::endl
            << _minx << ", " << _miny << " --> " << _maxx << ", " << _maxy
            << doLog;
#endif
            return false;
         }

         // here we basically ensure that we clip against the valid region for
         // X11 points, which are represented as signed shorts. if a points
         // falls outside this validity region, then it is clipped to the
         // visible region
         if (maxX > _deviceBounds.maxx || minX < _deviceBounds.minx || minY < _deviceBounds.miny
               || maxY > _deviceBounds.maxy) {
            if (curClip) {
               clip = curClip;
            }
            else {
               if (!_clip) {
                  _clip = createClipRegion();
               }
               clip = _clip;
            }
         }

#if 0
         _logger.info("Region visible");
#endif
         return true;
      }

      void AbstractContext::paintRegion(PaintMode mode, const Reference< Region2D>& region)
      throws()
      {

         Pointer< Region2D> reg(region);
         // check if the region is visible at all
         // or if it needs any clipping
         Pointer< Region2D> currentClip;
         Pointer< Region2D> clip;
         // pass in the current clip region or 0 if it
         // needs to be built
         if (!isVisible(reg->bounds(), clip, currentClip)) {
            return;
         }

         // check if a clip is necessary
         if (currentClip) {
            clip = currentClip;
            reg = clip->intersect(reg);
            if (!reg) {
               return;
            }
         }

         // when filling polygons, we need to remove holes for X11
         Pointer< Region2D> noHoles = reg;
         if ((mode & FILL)) {
            noHoles = reg->removeHoles();
            if (!noHoles) {
               _logger.warn("Could not remove holes from region; skipping region");
               return;
            }
         }

         // if noHoles is the same region, then nothing needs to be done
         // because the region has no holes
         if (noHoles == reg) {
            for (size_t j = 0, cnt = reg->areaCount(); j < cnt; ++j) {
               renderArea(mode, *reg->area(j));
            }
         }
         else {
            // render filled area first
            for (size_t j = 0, cnt = noHoles->areaCount(); j < cnt; ++j) {
               renderArea(mode & ~DRAW, *noHoles->area(j));
            }
            // render the original area in draw mode
            if ((mode & DRAW)) {
               for (size_t j = 0, cnt = reg->areaCount(); j < cnt; ++j) {
                  renderArea(mode & ~FILL, *reg->area(j));
               }
            }
         }
      }
      void AbstractContext::paintPath(PaintMode mode, const Reference< Path2D>& path)
      throws ()
      {
         if (path->maxDegree() > 1) {
            // cannot render paths of higher degrees (yet)
            return;
         }

         Pointer< Region2D> clip;
         Pointer< Path2D> p(path);
         // check if the region is visible at all
         // or if it needs any clipping
         Pointer< Region2D> currentClip;
         // pass in the current clip region or 0 if it
         // needs to be built
         if (!isVisible(p->bounds(), clip, currentClip)) {
            return;
         }
         if (currentClip) {
            clip = currentClip;
            p = clip->clipPath(p);
            if (!p) {
               return;
            }
         }

         ensurePointSize(p->segmentCount() + 1);

         Point2D pts[1]; // a maximum 2 points due to maxdegree of 1
         auto k = _points.begin();
         for (size_t j = 0, cnt = p->segmentCount(); j < cnt; ++j) {
            size_t deg = p->segment(j, pts);
            // check if a new polyline must be started
            if (deg == 0) {
               size_t n = k - _points.begin();
               if (n > 0) {
                  paintTransformedPolyline(mode, n, &_points[0], false);
                  k = _points.begin();
               }
            }
            *k = transformPoint(pts[0].x(), pts[0].y());
            ++k;
         }
         size_t n = k - _points.begin();
         if (n > 0) {
            paintTransformedPolyline(mode, n, &_points[0], false);
         }

      }

     void AbstractContext::drawRaster(const ImageRef& image,
				      const Point& xtl,
				      const Point& xtr,
#if ABSTRACT_CONTEXT_ENABLE_ALL_LOGGING
				      const Point& xbr,
#else
				      const Point& /* unused */,
#endif
				      const Point& xbl)
       throws ()
      {
         // first, we need to transform the image region
         const Point tl = _transform.transformPoint(xtl);
         const Point tr = _transform.transformPoint(xtr);
	 // we do not need to use xbr point
         const Point bl = _transform.transformPoint(xbl);

         double vec[2] { tr.x() - tl.x(), tr.y() - tl.y() };
         double phi = ::std::atan2(vec[1], vec[0]);

         // need to determine the scale factor
         double dx = ::std::sqrt(vec[0] * vec[0] + vec[1] * vec[1]);
         double sx = dx / image->width();
         double sy = bl.distanceTo(tl) / image->height();

         const AffineTransform2D imageTransform(phi, sx, sy, 0, 0);

#if ABSTRACT_CONTEXT_ENABLE_ALL_LOGGING == 1
         LogEntry(_logger).info() << "drawRaster : [" << phi << ", "<< sx << ", " << sy << " ] " << xtl << ", " << xtr << ", " << xbr << " : " << dx << ", " << bl.distanceTo(tl) << imageTransform << doLog;
#endif
         drawImage(xtl.x(), xtl.y(), image, imageTransform);
      }
   }

}
