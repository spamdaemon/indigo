#ifndef _INDIGO_RENDER_PICKCONTEXT_H
#define _INDIGO_RENDER_PICKCONTEXT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <memory>

namespace indigo {
   class Node;
   namespace render {
      class Context;

      /** A pick context */
      class PickContext
      {
            PickContext(const PickContext&);
            PickContext&operator=(const PickContext&);

            /** Default constructor */
         protected:
            PickContext()throws();

            /** Destructor */
         public:
            virtual ~PickContext()throws() = 0;

            /**
             * Get the rendering context.
             * @return a context to render
             */
         public:
            virtual Context& renderContext()throws() = 0;

            /**
             * Determine if a pick was registered and reset the state for the next pick.
             * Calling this method twice in a row without painting anything in between will
             * cause the second call to always return <tt>false</tt>.
             * @return true if a pick has been registered, false otherwise
             */
         public:
            virtual bool testAndReset()throws() = 0;
      };

   }
}
#endif
