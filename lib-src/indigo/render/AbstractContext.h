#ifndef _INDIGO_RENDER_ABSTRACTCONTEXT_H
#define _INDIGO_RENDER_ABSTRACTCONTEXT_H

#ifndef _INDIGO_RENDER_CONTEXT_H
#include <indigo/render/Context.h>
#endif

#ifndef _INDIGO_AFFINETRANSFORM2D_H
#include <indigo/AffineTransform2D.h>
#endif

#ifndef _TIKAL_POINT2D_H
#include <tikal/Point2D.h>
#endif

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

#include <vector>

namespace tikal {
   class Area2D;
   class BoundingBox2D;
   class Contour2D;
   class Polygon2D;
   class Region2D;
}
namespace indigo {
   namespace render {

      /**
       * This context implements primarily the setTransformMethod and provides a method to transform a
       * vector of points using the current transform.
       */
      class AbstractContext : public Context
      {
            /** A class represents */
         public:
            struct Bounds
            {
                  double minx, miny, maxx, maxy;
            };

            /** Default constructor */
         protected:
            AbstractContext(const AbstractContext& other)throws();

            /**
             * The size of the render area in pixel units.
             * <p>
             * FIXME: we should work this out a little bit better, what do x,y,w,h really mean in terms of device coordinates
             * @param x origin of the viewport
             * @param y origin of the viewport
             * @param w the width (>0)
             * @param h the height (>0);
             * @param xdeviceBounds the device bounds
             * */
         protected:
            AbstractContext(double x, double y, double w, double h, const Bounds& xdeviceBounds)throws();

            /** Destructor */
         public:
            ~AbstractContext()throws();

            /**
             * Set a new transform.
             * @param transform the new transform
             */
         public:
            void setTransform(const AffineTransform2D& transform)throws();
            void updateTransform(const AffineTransform2D& local)throws();

            /**
             * Set the alpha value. This method is invoked from updateAlpha
             * @param alpha
             */
            void setAlpha(double alpha)throws();
            void updateAlpha(double alpha)throws();

            /**
             * Get the current alpha value.
             * @return the current alpha value
             */
         public:
            inline double alpha() const throws()
            {
               return _alpha;
            }

         public:
            void paintPoints(PaintMode mode, size_t n, const Point* pts, double pointSize)throws();
            void paintRectangle(PaintMode mode, double x, double y, double w, double h)throws();
            void paintLine(PaintMode mode, double x0, double y0, double x1, double y1)throws();
            void paintPolygon(PaintMode mode, size_t n, const Point* ptr, bool convex)throws();
            void paintPolyline(PaintMode mode, size_t n, const Point* pts, bool close)throws();
            void paintEllipse(PaintMode mode, double x, double y, double semiMajor, double semiMinor,
                  double inclination)throws();
            void paintTriangles(PaintMode mode, size_t n, const Point* pts)throws();

            /**
             * Paint a triangle strip in terms of paintTransformedTriangles
             */
            void paintTriStrip(PaintMode mode, size_t n, const Point* ptr)throws();
            /**
             * Paint a triangle fan in terms of paintTransformedTriangles
             */
            void paintTriFan(PaintMode mode, size_t n, const Point* ptr)throws();

            /**
             * Paint a NURBS curve in terms of paintTransformedPolyLine
             * @param mode
             * @param curve
             */
            void paintNURBS(PaintMode mode, const NURBS& curve)throws();

            void paintRegion(PaintMode mode, const ::timber::Reference< ::tikal::Region2D>& region)throws();
            void paintPath(PaintMode mode, const ::timber::Reference< ::tikal::Path2D>& region)throws();
            void drawRaster(const ImageRef& image, const Point& tl, const Point& tr, const Point& br,
                  const Point& bl)throws();

            /**
             * Generate the points for an ellipse. The returned pointer is owned by this
             * context and is valid until the next call to generate. The points will
             * have been transformed according to the current transform.
             * <p>
             * Note that the first and last points may not be identical. When drawing
             * the ellipse, callers need to take care of rendering the last
             * @param x the center of the ellipse
             * @param y the center of the ellipse
             * @param M the semi-major axis length
             * @param m the semi-minor axis length
             * @param inclination the angle (in radians) between the major axis and the x-axis
             * @param n the number of points generated
             * @return a pointer to points
             */
         protected:
            const Point* generateEllipsePoints(double x, double y, double M, double m, double inclination,
                  size_t& n)throws();

            /**
             * Transform the specified point.
             * @param pt a point
             */
         protected:
            inline Point transformPoint(const Point& pt)throws()
            {
               return _transform.transformPoint(pt);
            }

            /**
             * Transform the specified point.
             * @param pt a point
             */
         protected:
            inline Point transformPoint(const ::tikal::Point2D& pt)throws()
            {
               return _transform.transformPoint(pt.x(), pt.y());
            }

            /**
             * Transform the specified point.
             * @param x x-coordinate
             * @param y y-coordinate
             */
         protected:
            inline Point transformPoint(double x, double y)throws()
            {
               return _transform.transformPoint(x, y);
            }

            /**
             * Transform the specified points. The returned pointer is owned by
             * this class and is only valid until the next transform operation.
             * @param pts the points to be set and transformed
             * @param n the number of points
             * @return a pointer to n points that have been transformed.
             */
         protected:
            const Point* transformPoints(const Point* pts, size_t n)throws();

            /**
             * Transform the specified points. The returned pointer is owned by
             * this class and is only valid until the next transform operation.
             * @param pts the points to be set and transformed
             * @param n the number of points
             * @return a pointer to n points that have been transformed.
             */
         protected:
            const Point* transformPoints(const ::tikal::Point2D* pts, size_t n)throws();

            /**
             * Paint individual transformed points
             * @param mode
             * @param n
             * @param pts
             */
         protected:
            virtual void paintTransformedPoints(PaintMode mode, size_t n, const Point* pts, double ptSize)throws() = 0;

            /**
             * Paint a polygon that has already been transformed.
             * @param mode
             * @param n
             * @param pts
             */
         protected:
            virtual void paintTransformedPolygon(PaintMode mode, size_t n, const Point* pts, bool convex)throws() = 0;

            /**
             * Paint a polyline that has already been transformed.
             * @param mode
             * @param n
             * @param pts
             */
         protected:
            virtual void paintTransformedPolyline(PaintMode mode, size_t n, const Point* pts, bool close)throws() = 0;

            /**
             * Paint transformed triangles.
             * @param mode
             * @param n
             * @param pts
             */
         protected:
            virtual void paintTransformedTriangles(PaintMode mode, size_t n, const Point* pts)throws();

            /**
             * Paint a transform tristrip. This method is implemented in terms of paintTransformedTriangles.
             * @param mode
             * @param n
             * @param pts
             */
         protected:
            void paintTransformedTriStrip(PaintMode mode, size_t n, const Point* pts)throws();

            /**
             * Get the device bounds that were passed to the constructor.
             * @return the device bounds
             */
         protected:
            const Bounds& deviceBounds() const throws()
            {
               return _deviceBounds;
            }

            /**
             * Convert a region to a triangle strip and paint it with the specified paint mode.
             * @param mode the paint mode
             * @param strips a region consisting of triangle strips.
             */
         private:
            void renderTriangleStrips(PaintMode mode, ::tikal::Region2D& strips)throws();

            /**
             * Render a contour consisting of a single polygon.
             * @param r a region
             */
         private:
            bool renderPolygon(PaintMode mode, ::tikal::Polygon2D& r)throws();

            /**
             * Render a contour.
             * @param mode
             * @param r
             * @return
             */
         private:
            bool renderContour(PaintMode mode, ::tikal::Contour2D& r)throws();

            /**
             * Render ab area consisting of possibly many contours or area, but
             * not containing hold if doFill == true
             * @param mode
             * @param r a region
             */
         private:
            void renderArea(PaintMode mode, ::tikal::Area2D& r)throws();

            /**
             * Determine if the specified bounding box is visible in the current clip region under
             * the current transform.
             * @param bbox
             * @param curClip
             * @param clip
             * @return
             */
         private:
            bool isVisible(const ::tikal::BoundingBox2D& bbox, const ::timber::Pointer< ::tikal::Region2D>& curClip,
                  ::timber::Pointer< ::tikal::Region2D>& clip)throws();

            /**
             * Create a clip region for the current bounds of the context in the coordinate
             * system define by the current transform.
             * The clip region will be based on bounding box that is 1 pixel
             * larger.
             * @return a clip region
             */
         private:
            ::timber::Reference< ::tikal::Region2D> createClipRegion()throws();

            /**
             * Ensure that the internal size of points is at least n
             * @param n the minimum size required
             */
         private:
            void ensurePointSize(size_t n)throws();

            /** The min/max values for the transformed points. Since, eventually, floating
             * point number have to be converted to integers there is an implied bounding
             * box and we may not produce points outside that box.
             */
         private:
            Bounds _deviceBounds;

            /** The default alpha value */
         private:
            double _alpha;

            /**
             * The current transform.
             */
         private:
            AffineTransform2D _transform;

            /** The points transformed by the last operation. */
         private:
            ::std::vector< Point> _points;

            /** Temporary points */
         private:
            ::std::vector< Point> _tmpPoints;

            /** A logger */
         private:
            ::timber::logging::Log _logger;

            /** The output region */
         private:
            double _minx, _miny, _maxx, _maxy;

            /** The current clip */
         private:
            ::timber::Pointer< ::tikal::Region2D> _clip;
      };

   }

}

#endif
