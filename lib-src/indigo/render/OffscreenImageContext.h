#ifndef _INDIGO_RENDER_PICKCONTEXT_H
#define _INDIGO_RENDER_PICKCONTEXT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGE_H
#include <timber/media/Image.h>
#endif

#include <memory>

namespace indigo {
   namespace render {
      class Context;

      /**
       * This class is used to render offscreen images.
       */
      class OffscreenImageContext
      {
            OffscreenImageContext(const OffscreenImageContext&);
            OffscreenImageContext&operator=(const OffscreenImageContext&);

            /** Default constructor */
         protected:
            OffscreenImageContext()throws();

            /** Destructor */
         public:
            virtual ~OffscreenImageContext()throws() = 0;

            /**
             * Get the rendering context.
             * @return a context to render
             */
         public:
            virtual Context& renderContext()throws() = 0;

            /**
             * Get the image rendered so far.
             * @return the image rendered so far.
             */
         public:
            virtual ::timber::SharedRef< ::timber::media::Image> getImage() throws() = 0;
      };

   }
}
#endif
