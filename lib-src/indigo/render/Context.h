#ifndef _INDIGO_RENDER_CONTEXT_H
#define _INDIGO_RENDER_CONTEXT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace timber {
   namespace media {
      class ImageBuffer;
   }
}

namespace tikal {
   class Path2D;
   class Region2D;
}

namespace indigo {
   class AffineTransform2D;
   enum class AntiAliasingMode;
   class Color;
   class Font;
   class Node;
   class NURBS;
   class Point;

   namespace render {

      /**
       * The context provides is used to maintain rendering state and to draw shapes
       * into an unspecified canvas. Currently, the context supports only drawing lines
       * and filling shapes.<p>
       * TODO: in the future we also will add paint methods for each of the fill and draw
       * method to support a richer means for rendering.
       * <p>
       * One of the features of the context is its ability to draw a scenegraph.
       */
      class Context
      {
            /** An image buffer */
         public:
            typedef ::timber::SharedRef< ::timber::media::ImageBuffer> ImageRef;

            /** The render modes; rendering shapes requires or'ing these flags */
         public:
            static constexpr int FILL = 0x00ff; ///< Captures all FILL operations
            static constexpr int DRAW = 0xff00; ///< Captures all DRAW operations

            /** The paint mode is a combination of FILL, DRAW, and future other modes */
         public:
            typedef int PaintMode;

            /** Default constructor */
         protected:
            Context()throws();

            /** Destructor */
         public:
            virtual ~Context()throws() = 0;

            /**
             * @name StateModification
             * Methods modifying the current rendering state.
             * @{
             */

            /**
             * Update the current transform. This method post-multiplies the given
             * transform with the current transform, such that the overall transform
             * is <tt>GLOBAL = GLOBAL * local</tt>.
             * @param t the local transform
             */
         public:
            virtual void updateTransform (const AffineTransform2D& local) throws() =0;

            /**
             * Set a new transform.
             * @param transform the new transform
             */
         public:
            virtual void setTransform(const AffineTransform2D& transform)throws() = 0;

            /**
             * Set the alpha or opacity value for subsequent rendering operations. An alpha
             * value of 0 indicates complete translucency and 1 complete opacity.
             *
             * @param alpha the new alpha value
             */
         public:
            virtual void setAlpha(double alpha)throws() = 0;

            /**
             * Update the current alpha value by multiplying it by the specified value.
             * This changes the global alpha value such that <tt>GLOBAL = GLOBAL * local</tt>.
             * @param local a multiplier between 0 and 1
             */
         public:
            virtual void updateAlpha (double alpha) throws() = 0;

            /**
             * Set the anti-aliasing mode.
             * @param mode the new anti-aliasing mode to use
             */
         public:
            virtual void setAntiAliasingMode(AntiAliasingMode mode)throws() = 0;

            /**
             * Set the text font.
             * @param font the text font
             */
         public:
            virtual void setFont(const ::timber::Reference< Font>& font)throws() = 0;

            /**
             * Set the solid fill color which will be used to draw polygons.
             * @param color the color to use for polygons
             */
         public:
            virtual void setSolidFillColor(const Color& fillColor)throws() = 0;

            /**
             * Set the solid stroke color.
             * @param color the color to use for drawing lines, text, and outlines
             */
         public:
            virtual void setStrokeColor(const Color& strokeColor)throws() = 0;

            /**
             * Set the stroke width.
             * @param width the new stroke  width
             */
         public:
            virtual void setStrokeWidth(double width)throws() = 0;

            /*@}*/

            /**
             * @name DrawingAndFillingShapes
             * These methods are about drawing outlines and filling shapes with solids colors
             * @{
             */
            /**
             * Paint a rectangle. The rectangle will be transformed by the current matrix.
             * @param mode the paint mode
             * @param x x-coordinate of a corner
             * @param y y-coordinate of a corner
             * @param w the width (can be negative)
             * @param h the height (can be negative)
             */
         public:
            virtual void paintRectangle(PaintMode mode, double x, double y, double w, double h)throws() = 0;

            /**
             * Draw a points.
             * @param mode the paint mode
             * @param n the number of points to draw
             * @param pts the points
             * @param pointSize the size of a point
             */
         public:
            virtual void paintPoints(PaintMode mode, size_t n, const Point* pts, double pointSize)throws() = 0;

            /**
             * Draw a polyline.
             * @param mode the paint mode
             * @param n the number of points to draw
             * @param pts the points to connect
             * @param close if true, then draw a line from the first to the last point
             */
         public:
            virtual void paintPolyline(PaintMode mode, size_t n, const Point* pts, bool close)throws() = 0;

            /**
             * Paint a NURBS curve.
             * @param mode the paint mode
             * @param curve a nurbs curve
             */
         public:
            virtual void paintNURBS (PaintMode mode, const NURBS& curve) throws() = 0;

            /**
             * Paint a polygon.
             * @param mode the paint mode
             * @param n the number of points to draw
             * @param pts the points to draw
             * @param convex if true, then the polygon is convex
             */
         public:
            virtual void paintPolygon(PaintMode mode, size_t n, const Point* ptr, bool convex)throws() = 0;

            /**
             * Paint triangles.
             * @param mode the paint mode
             * @param n the number of points (there are exactly n/3 triangles)
             * @param pts the points
             */
         public:
            virtual void paintTriangles(PaintMode mode, size_t n, const Point* pts)throws() = 0;

            /**
             * Paint a triangle strip.
             * @param mode the paint mode
             * @param n the number of points
             * @param pts the points of the triangle strip
             */
         public:
            virtual void paintTriStrip(PaintMode mode, size_t n, const Point* ptr)throws() = 0;

            /**
             * Paint a triangle fan.
             * @param mode the paint mode
             * @param n the number of points
             * @param pts the points of the triangle fan
             */
         public:
            virtual void paintTriFan(PaintMode mode, size_t n, const Point* ptr)throws() = 0;

            /**
             * Draw an ellipse. The current transform affects the shape of the ellipse.
             * @param mode the paint mode
             * @param x the center of the ellipse
             * @param y the center of the ellipse
             * @param semiMajor the semiMajor axis length
             * @param semiMinor the semiMinor axis length
             * @param inclination the angle (in radians) between the x-axis and the semiMajor axis
             */
         public:
            virtual void paintEllipse(PaintMode mode, double x, double y, double semiMajor, double semiMinor, double inclination)throws() = 0;

            /**
             * Draw the outline of a region.
             * @param mode the paint mode
             * @param region a region
             */
         public:
            virtual void paintRegion(PaintMode mode, const ::timber::Reference< ::tikal::Region2D>& region)throws() = 0;

            /**
             * Draw a path.
             * @param region a region
             */
         public:
            virtual void paintPath(PaintMode mode, const ::timber::Reference< ::tikal::Path2D>& region)throws() = 0;

            /**
             * Draw a single line
             * @param mode the paint mode
             * @param x0 the start coordinate
             * @param y0 the start coordinate
             * @param x1 the end coordinate
             * @param y1 the end coordinate
             */
         public:
            virtual void paintLine(PaintMode mode, double x0, double y0, double x1, double y1)throws() = 0;

            /**
             * Draw a raster image. The image location is affected by the current transform, but the image
             * itself is not. This method is equivalent to <tt>drawRaster(x,y,image,-1,-1)</tt>.
             * @param x the location of the top-left corner of the image
             * @param y the location of the top-left corner of the image
             * @param image an image
             */
         public:
            virtual void drawImage(double x, double y, const ImageRef& image)throws();

            /**
             * Draw a raster image that is scaled to the specified width and height.
             * The image location is affected by the current transform, but the image
             * itself is not and is thus axis-aligned with the edges of the drawing surface.
             * @param x the location of the top-left corner of the image
             * @param y the location of the top-left corner of the image
             * @param image an image
             * @param w the width of the image (<0 to not scale)
             * @param h the height of the image (<0 to not scale)
             */
         public:
            virtual void drawImage(double x, double y, const ImageRef& image, ::std::int32_t w, ::std::int32_t h)throws()= 0;

            /**
             * Draw an image that is possibly transformed by an affine transform. The top-left corner of the image
             * is first moved to the specified location before applying the transform.
             * @param x the location of the top-left corner of the image
             * @param y the location of the top-left corner of the image
             * @param image an image
             * @param t the transform that is applied to the image
             */
         public:
            virtual void drawImage(double x, double y, const ImageRef& image, const AffineTransform2D& t)throws()= 0;

            /**
             * Render an image over the rectangular region specified by its four corner points.
             * @param image an image
             * @param tl the location where the top-left image pixel is to be rendered
             * @param tr the location where the top-right image pixel is to be rendered
             * @param br the location where the bottom-right image pixel is to be rendered
             * @param bl the location where the bottom-left image pixel is to be rendered
             */
         public:
            virtual void drawRaster(const ImageRef& image, const Point& tl, const Point& tr, const Point& br, const Point& bl)throws()= 0;

            /**
             * Draw text using the current font.
             * @param x the location of the baseline for the text
             * @param y the locaction of the baseline for the text
             * @param utf8 the text
             */
         public:
            virtual void drawTextUtf8(double x, double y, const ::std::string& utf8)throws() =0;

            /*@}*/
      };

   }

}

#endif
