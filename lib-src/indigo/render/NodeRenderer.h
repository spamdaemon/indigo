#ifndef _INDIGO_RENDER_NODERENDERER_H
#define _INDIGO_RENDER_NODERENDERER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <memory>

namespace indigo {
   class Node;
   namespace render {
      class Context;

      /**
       * The NodeRenderer is used to render nodes into render context. This particular type of renderer
       * does not support picking.
       */
      class NodeRenderer
      {
            /** The default constructor */
         protected:
            NodeRenderer()throws();

            /** Destructor */
         public:
            virtual ~NodeRenderer()throws();

            /**
             * Create a new renderer.
             * @param renderer
             * @return a renderer
             */
         public:
           static ::std::unique_ptr< NodeRenderer> create();

            /**
             * Render the specified node into a context.
             * @param n a node
             * @param ctx a context
             */
         public:
            virtual void render(const ::timber::Reference<Node>& node, Context& ctx)throws() = 0;

      };

   }
}
#endif
