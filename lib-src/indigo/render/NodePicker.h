#ifndef _INDIGO_RENDER_NODEPICKER_H
#define _INDIGO_RENDER_NODEPICKER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INDIGO_RENDER_PICKCONTEXT_H
#include <indigo/render/PickContext.h>
#endif

#ifndef _INDIGO_PICKINFO_H
#include <indigo/PickInfo.h>
#endif

#include <memory>

namespace indigo {
   namespace render {

      /**
       * The node picker is used to determine which node and possibly
       * which renderable objects is being drawn at given pixel.
       */
      class NodePicker
      {
            /** This structure represents the result of a single pick */
         public:
            struct Pick
            {
                  inline Pick()throws() : shape(0) {}

                  /** The node that was picked */
                  ::timber::Pointer< ::indigo::PickInfo> info;

                  /** The node that was picked (null if none was picked) */
                  ::timber::Pointer< ::indigo::Node> node;

                  /** The index of the shape that was picked (undefined if node is null) */
                  size_t shape;
            };

            /** The default constructor */
         protected:
            NodePicker()throws();

            /** Destructor */
         public:
            virtual ~NodePicker()throws();

            /**
             * Create a new picker for a specific pixel.
             * @return a picker
             */
         public:
            static ::std::unique_ptr< NodePicker> create();

            /**
             * Render the specified node into a context.
             * @param n a node
             * @param ctx a context
             * @return a pick object
             */
         public:
            virtual Pick pick(const ::timber::Reference< Node>& node, PickContext& ctx)throws() = 0;
      };

   }
}
#endif
