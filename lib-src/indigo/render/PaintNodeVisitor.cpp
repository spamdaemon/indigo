#include <indigo/render/PaintNodeVisitor.h>
#include <timber/logging.h>

using namespace ::timber;
using namespace ::timber::logging;

namespace indigo {
   namespace render {
      namespace {

         static Log logger()
         {
            return "indigo.render.PaintNodeVisitor";
         }
      }

      PaintNodeVisitor::RenderState::RenderState()
      throws()
            : _solidFillPolygon(false), _drawPolygon(false), _resetTransformMode(false), _opaque(true), _alpha(1), _aaMode(
                  ::indigo::AntiAliasingMode::FAST), _stroke(nullptr), _solidFill(nullptr)
      // concatenate transforms by default
      {
         _transform = _viewTransform;
      }

      PaintNodeVisitor::RenderState::~RenderState()
      throws()
      {
      }

      Context::PaintMode PaintNodeVisitor::RenderState::paintMode() const throws()
      {
         Context::PaintMode mode = 0;
         if (isFillMode()) {
            mode |= Context::FILL;
         }
         if (isDrawMode()) {
            mode |= Context::DRAW;
         }
         return mode;
      }

      PaintNodeVisitor::PaintNodeVisitor(Context& ctx)
      throws()
            : _context(ctx), _stateChanged(true)
      {
         _stack.reserve(100);
         _stack.push_back(RenderState());

         // immediately, set the context
         PaintNodeVisitor::updateGraphicState();
      }

      PaintNodeVisitor::~PaintNodeVisitor()
      throws()
      {
      }
      void PaintNodeVisitor::saveState()
      throws()
      {
         _stack.reserve(_stack.size() + 1);
         _stack.push_back(_stack.back());
      }

      void PaintNodeVisitor::restoreState()
      throws()
      {
         if (_stack.size() == 1) {
            logger().info("State is out-of-order; not restoring state");
            return;
         }
         _stack.pop_back();
         _stateChanged = true;
      }

      const PaintNodeVisitor::RenderState& PaintNodeVisitor::updateGraphicState()
      throws()
      {
         RenderState& state = getState();
         if (_stateChanged) {
            _stateChanged = false;
            state._solidFillPolygon = false;
            state._drawPolygon = false;

            if (state._solidFill) {
               state._solidFillPolygon = true;
               _context.setSolidFillColor(state._solidFill->color());
            }
            else {
               _context.setSolidFillColor(_defaultFill.color());
            }
            if (state._stroke) {
               state._drawPolygon = true;
               _context.setStrokeColor(state._stroke->color());
               _context.setStrokeWidth(state._stroke->width());
            }
            else {
               _context.setStrokeColor(_defaultStroke.color());
               _context.setStrokeWidth(_defaultStroke.width());
            }
            if (state._alpha == 0) {
               state._solidFillPolygon = state._drawPolygon = false;
            }

            _context.setAlpha(state._alpha);
            _context.setAntiAliasingMode(state._aaMode);
            _context.setTransform(state._transform);
         }
         return state;
      }

      void PaintNodeVisitor::setSolidFill(const SolidFillNode* fn)
      throws()
      {
         RenderState& state = getState();
         if (state._solidFill != fn) {
            state._solidFill = fn;
            state._solidFillPolygon = (fn != 0) && (state._alpha != 0);
            if (fn) {
               _context.setSolidFillColor(fn->color());
            }
         }
      }

      void PaintNodeVisitor::setStroke(const StrokeNode* fn)
      throws()
      {
         RenderState& state = getState();
         if (state._stroke != fn) {
            state._stroke = fn;
            state._drawPolygon = (fn != 0) && (state._alpha != 0);
            if (fn) {
               _context.setStrokeColor(fn->color());
               _context.setStrokeWidth(fn->width());
            }
         }
      }

      void PaintNodeVisitor::setAlpha(double alpha)
      throws()
      {
         RenderState& state = getState();
         if (state._alpha != alpha) {
            state._alpha = alpha;
            if (alpha == 0) {
               state._solidFillPolygon = state._drawPolygon = false;
            }
            _context.setAlpha(alpha);
         }
      }

      void PaintNodeVisitor::visit(const Coordinates2D&)
      throws()
      {
         // TODO: implement me
      }

      void PaintNodeVisitor::visit(const Texture& g)
      throws()
      {
         // TODO: implement me
      }

      void PaintNodeVisitor::visit(const Coordinates3D& g)
      throws()
      {
         // TODO: implement me
      }

      void PaintNodeVisitor::visit(const CachableNode& cn)
      throws()
      {
         Pointer< Node> n = cn.node();
         if (n) {
            visitChild(*n, cn.isSeparator());
         }
      }

      void PaintNodeVisitor::visit(const Icon& g)
      throws()
      {
         if (g.image()) {
            _context.drawImage(0, 0, g.image(), g.transform());
         }
      }

      void PaintNodeVisitor::visit(const Raster& g)
      throws()
      {
         if (g.image()) {
            _context.drawRaster(g.image(), g.topLeft(), g.topRight(), g.bottomRight(), g.bottomLeft());
         }
      }

      void PaintNodeVisitor::visit(const ::indigo::PickInfo& p)
      throws()
      {
      }

      void PaintNodeVisitor::visit(const AntiAliasingModeNode& g)
      throws()
      {
         RenderState& state = getState();
         state._aaMode = g.antiAliasingMode();
         _context.setAntiAliasingMode(state._aaMode);
      }

      void PaintNodeVisitor::visit(const ::indigo::AlphaNode& p)
      throws()
      {
         setAlpha(getState()._alpha * p.alpha());
      }

      void PaintNodeVisitor::visit(const TransformMode& g)
      throws()
      {
         RenderState& state = getState();
         switch (g.mode()) {
            case TransformMode::RESET:
               state._resetTransformMode = true;
               _stateChanged = true;
               break;
            case TransformMode::CONCATENATE:
            default:
               state._resetTransformMode = false;
               _stateChanged = true;
         }
      }

      void PaintNodeVisitor::visit(const Text& txt)
      throws()
      {
         if (txt.length() == 0) {
            return;
         }
         Pointer< Font> ft = txt.font();
         if (!ft) {
#if 0
            ft = _renderer.defaultFont();
#endif
            if (!ft) {
               return;
            }
         }

         updateGraphicState();
         _context.setFont(ft);
         _context.drawTextUtf8(0, 0, txt.text());
      }

      void PaintNodeVisitor::visit(const ViewTransform2D& t)
      throws()
      {
         RenderState& state = getState();
         const AffineTransform2D inv(state._viewTransform.inverse());

         // premultiply the view transform by the compound object transform, which contains a view transform
         const AffineTransform2D objTransform = AffineTransform2D(inv, state._transform);

         // the new transform
         if (state.isResetTransform()) {
            state._viewTransform = t.transform();
         }
         else {
            state._viewTransform = AffineTransform2D(state._viewTransform, t.transform());
         }

         // update the overal transform
         state._transform = AffineTransform2D(state._viewTransform, objTransform);
         _context.setTransform(state._transform);
      }

      void PaintNodeVisitor::visit(const Transform2D& t)
      throws()
      {
         RenderState& state = getState();
         if (state.isResetTransform()) {
            // reset the transform
            state._transform = AffineTransform2D(state._viewTransform, t.transform());
         }
         else {
            // concatenate the transforms
            state._transform = AffineTransform2D(state._transform, t.transform());
         }
         _context.setTransform(state._transform);
      }

      void PaintNodeVisitor::paintShape(const Circles& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const Circle &p = shapes.shape(index);
         const Point& ctr = p.center();
         if (p.radius() == 0) {
            _context.paintPoints(mode, 1, &ctr, 1);
         }
         else {
            _context.paintEllipse(mode, ctr.x(), ctr.y(), p.radius(), p.radius(), 0);
         }
      }

      void PaintNodeVisitor::paintShape(const Ellipses& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const Ellipse& p = shapes.shape(index);

         double majorAxis = p.majorAxis();
         double minorAxis = p.minorAxis();

         const Point& ctr = p.center();

         if (majorAxis == 0 && minorAxis == 0) {
            _context.paintPoints(mode, 1, &ctr, 1);
         }
         else {
            _context.paintEllipse(mode, ctr.x(), ctr.y(), p.majorAxis(), p.minorAxis(), p.theta());
         }
      }

      void PaintNodeVisitor::paintShape(const PolyLines& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const PolyLine& p = shapes.shape(index);
         if (p.vertexCount() > 0) {
            _context.paintPolyline(mode, p.vertexCount(), &p.vertices()[0], p.isClosed());
         }
      }

      void PaintNodeVisitor::paintShape(const Points& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const Point& p = shapes[index];
         _context.paintPoints(mode, 1, &p, shapes.pointSize());
      }

      void PaintNodeVisitor::paintShape(const NurbsCurves& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const NURBS& c = shapes.shape(index);
         _context.paintNURBS(mode, c);
      }

      void PaintNodeVisitor::paintShape(const TriangleSet& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const Triangles& p = shapes.shape(index);
         if (p.vertexCount() != 0) {
            const Point* pts = &p.vertices()[0];

            switch (p.type()) {
               case Triangles::TRIANGLES:
                  _context.paintTriangles(mode, p.vertexCount(), pts);
                  break;
               case Triangles::STRIP:
                  _context.paintTriStrip(mode, p.vertexCount(), pts);
                  break;
               case Triangles::FAN:
                  _context.paintTriFan(mode, p.vertexCount(), pts);
                  break;
               default:
                  assert(false);
                  break;
            }
         }
      }

      void PaintNodeVisitor::paintShape(const Polygons& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const Polygon& p = shapes.shape(index);
         if (p.vertexCount() >= 3) {
            _context.paintPolygon(mode, p.vertexCount(), &p.vertices()[0], p.shapeType() == Polygon::CONVEX_SHAPE);
         }
      }

      void PaintNodeVisitor::paintShape(const Regions& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const Pointer< ::tikal::Region2D>& reg = shapes.shape(index);
         if (reg) {
            _context.paintRegion(mode, reg);
         }
      }

      void PaintNodeVisitor::paintShape(const Paths& shapes, size_t index, Context::PaintMode mode)
      throws()
      {
         assert(index < shapes.count());
         const Pointer< ::tikal::Path2D>& p = shapes.shape(index);
         if (p) {
            _context.paintPath(mode, p);
         }
      }

      template<class T> void PaintNodeVisitor::paintShapes(const T& g)
      throws()
      {
         size_t sz = g.count();
#if 0
         LogEntry(logger()).info() << "Painting " << sz << " shapes" << doLog;
#endif
         if (sz > 0) {
            // render from the back to the front!!
            updateGraphicState();
            Context::PaintMode mode = getState().paintMode();
            if (mode == 0) {
               return;
            }
            while (sz-- > 0) {
               paintShape(g, sz, mode);
            }
         }
      }

      void PaintNodeVisitor::visit(const Circles& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const Ellipses& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const PolyLines& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const Points& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const NurbsCurves& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const TriangleSet& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const Regions& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const Paths& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const Polygons& g)
      throws()
      {
         paintShapes(g);
      }

      void PaintNodeVisitor::visit(const SolidFillNode& f)
      throws()
      {
         const SolidFillNode* fn(0);
         if (f.color().opacity() != 0) {
            fn = &f;
         }
         if (getState()._solidFill != fn) {
            // change the fill
            setSolidFill(fn);
         }
      }

      void PaintNodeVisitor::visit(const StrokeNode& f)
      throws()
      {
         const StrokeNode* fn(0);
         if (f.color().opacity() != 0) {
            fn = &f;
         }
         if (getState()._stroke != fn) {
            // change the stroke
            setStroke(fn);
         }
      }

      void PaintNodeVisitor::visitChild(const Node& node, bool bakState)
      throws()
      {
         if (bakState) {
            saveState();
            node.accept(*this);
            restoreState();
         }
         else {
            node.accept(*this);
         }
      }

      void PaintNodeVisitor::visitChildren(const CompositeNode& g)
      throws()
      {
         size_t sz = g.nodeCount();
         // check if there are any children; if not, then it's useless
         // to do other work.
         if (sz > 0) {
            if (g.isSeparator()) {

               saveState();
               // render from the back to the front!!
               for (size_t i = 0; i < sz; ++i) {
                  const Pointer< Node> h(g.getNode(i));
                  if (h) {
                     h->accept(*this);
                  }
               }
               restoreState();
            }
            else {
               for (size_t i = 0; i < sz; ++i) {
                  const Pointer< Node> h(g.getNode(i));
                  if (h) {
                     h->accept(*this);
                  }
               }
            }
         }
      }

      void PaintNodeVisitor::visit(const CompositeNode& g)
      throws ()
      {
         visitChildren(g);
      }

      void PaintNodeVisitor::visit(const Switch& s)
      throws()
      {
         const Pointer< Node> h = s.visibleNode();
         if (h) {
            visitChild(*h, s.isSeparator());
         }
      }

      void PaintNodeVisitor::visit(const Separator& s)
      throws ()
      {
         const Pointer< Node> h = s.node();
         if (h) {
            visitChild(*h, true);
         }
      }
   }
}
