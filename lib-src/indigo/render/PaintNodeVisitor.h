#ifndef _INDIGO_RENDER_PAINTNODEVISITOR_H
#define _INDIGO_RENDER_PAINTNODEVISITOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INDIGO_RENDER_CONTEXT_H
#include <indigo/render/Context.h>
#endif

#ifndef _INDIGO_GFXVISITOR_H
#include <indigo/GfxVisitor.h>
#endif

#ifndef _INDIGO_NODES_H
#include <indigo/nodes.h>
#endif

#include <memory>
#include <vector>

namespace indigo {
   class Node;
   namespace render {

      /**
       * This visitor provides a convenient baseclass for painting nodes into
       * a provided context. A node visitor is expected to be instantiated on
       * the stack.
       */
      class PaintNodeVisitor : public GfxVisitor
      {
            PaintNodeVisitor&operator=(const PaintNodeVisitor&);
            PaintNodeVisitor(const PaintNodeVisitor&);

            /**
             * A structure representing the current rendering state.
             * <p>
             * Access to this structure is restricted and subject to change.
             */
            struct RenderState
            {
                  RenderState()throws();
                  ~RenderState()throws();

                  inline bool isFillMode() const throws()
                  {
                     return _solidFillPolygon;
                  }
                  inline bool isDrawMode() const throws()
                  {
                     return _drawPolygon;
                  }
                  inline bool isResetTransform() const throws()
                  {
                     return _resetTransformMode;
                  }

                  Context::PaintMode paintMode() const throws();

                  AffineTransform2D _viewTransform;
                  AffineTransform2D _transform;
                  bool _solidFillPolygon;
                  bool _drawPolygon;
                  bool _resetTransformMode;
                  bool _opaque;
                  double _alpha;
                  ::indigo::AntiAliasingMode _aaMode;
                  const StrokeNode* _stroke;
                  const SolidFillNode* _solidFill;
            };

            /**
             * Create a new visitor that paint to the specified context.
             * @param ctx a context
             */
         public:
            PaintNodeVisitor(Context& ctx)throws();

            /** Destructor */
         public:
            ~PaintNodeVisitor()throws();

            /** The various visitation functions. */
         public:
            void visit(const Coordinates2D&)throws();
            void visit(const Texture& g)throws();
            void visit(const Coordinates3D& g)throws();
            void visit(const CachableNode& cn)throws();
            void visit(const Icon& g)throws();
            void visit(const Raster& g)throws();
            void visit(const PickInfo& p)throws();
            void visit(const AntiAliasingModeNode& g)throws();
            void visit(const AlphaNode& p)throws();
            void visit(const TransformMode& g)throws();
            void visit(const Text& txt)throws();
            void visit(const ViewTransform2D& t)throws();
            void visit(const Transform2D& t)throws();
            void visit(const Circles& g)throws();
            void visit(const Ellipses& g)throws();
            void visit(const PolyLines& g)throws();
            void visit(const Points& g)throws();
            void visit(const NurbsCurves& g)throws();
            void visit(const TriangleSet& g)throws();
            void visit(const Regions& g)throws();
            void visit(const Paths& g)throws();
            void visit(const Polygons& g)throws();
            void visit(const SolidFillNode& f)throws();
            void visit(const StrokeNode& f)throws();
            void visit(const CompositeNode& g)throws();
            void visit(const Switch& s)throws();
            void visit(const Separator& s)throws();

         private:
            template <class T>
            void paintShapes(const T& shapes)throws();

         protected:
            virtual void paintShape(const Circles& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const Ellipses& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const PolyLines& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const Polygons& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const Points& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const NurbsCurves& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const TriangleSet& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const Regions& shapes, size_t index, Context::PaintMode mode)throws();
            virtual void paintShape(const Paths& shapes, size_t index, Context::PaintMode mode)throws();

            /**
             *  Visit the children of the specified node. This method
             *  must consider the value CompositeNode::isSeparator().
             *  @param g a composite node
             */
         private:
            void visitChildren(const CompositeNode& g)throws();

            /**
             * Visit the specified node, optionally backing up the current rendering state. This method
             * is invoked when ever a single child node is traversed.
             * @param node the node to visit
             * @param bakState true to backup the state, false otherwise
             */
         private:
            void visitChild(const Node& node, bool bakState)throws();

            /**
             * Save the current state.
             */
         protected:
            virtual void saveState()throws();

            /**
             * Restore the most recently saved state.
             */
         protected:
            virtual void restoreState()throws();

            /**
             * Get the context.
             * @return the context
             */
         protected:
            inline Context& context()throws() {return _context;}

            /**
             * Get the current render-state
             * @return the current render-state
             */
         private:
            RenderState& getState()throws() {return _stack.back();}

            /**
             * Update the renderer's graphic state.
             * @return the current rendering state
             */
         protected:
            const RenderState& updateGraphicState()throws();

            /**
             * Set the solid fill color
             * @param fn new fill
             */
         private:
            void setSolidFill(const SolidFillNode* fn)throws();

            /**
             * Set the stroke.
             * @param fn new stroke
             */
         private:
            void setStroke(const StrokeNode* fn)throws();

            /** Set a new alpha value
             * @param alpha a new alpha
             */
         private:
            void setAlpha(double alpha)throws();

            /** The context into which to render */
         private:
            Context& _context;

            /** True if the render state has changed and needs to be refreshed */
         private:
            bool _stateChanged;

            /** A state stack */
         private:
            ::std::vector< RenderState> _stack;

            /** The default fill color */
         private:
            SolidFillNode _defaultFill;

            /** The default stroke color */
         private:
            StrokeNode _defaultStroke;
      };

   }
}
#endif
