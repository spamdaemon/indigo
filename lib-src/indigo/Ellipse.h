#ifndef _INDIGO_ELLIPSE_H
#define _INDIGO_ELLIPSE_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif


namespace indigo {

  /**
   * This class represents an immutable ellipse. Ellipses may be efficiently
   * copied, because they can share the set of vertices.
   */
  class Ellipse {
      
    /**
     * Create the unit circle centered at (0,0) and radius 1.
     */
  public:
    Ellipse () throws();
    
    /**
     * Create a ellipse with a given radius. The radius
     * may 0, in which case the ellipse should be rendered
     * as a point.
     * @param c the ellipse center
     * @param r1 the length of one axis
     * @param r2 the length of the second axis
     * @param inclination the angle between r1 and the positive x-axis
     * @pre REQUIRE_GREATER_OR_EQUAL(r1,0)
     * @pre REQUIRE_GREATER_OR_EQUAL(r2,0)
     */
  public:
    Ellipse (const Point& c, double r1, double r2, double inclination=0.0) throws();

    /**
     * Create a covariance ellipse.
     * @param c the center of the ellipse
     * @param xx the (0,0) entry of the covariance matrix
     * @param yy the (1,1) entry of the covariance matrix
     * @param xy the off-diagonal entries of the covariance matrix
     * @param stddev the standard deviation to be used
     * @throws std::exception if (xx*yy - xy*xy) <= 0.0
     */
  public:
    static Ellipse createCovarianceEllipse(const Point& c, double xx, double yy, double xy=0.0, double stddev=1.0) throws (::std::exception);

    /**
     * Get the length of one of the axes.
     * @return the length of an axis
     */
  public:
    inline double majorAxis() const throws()
    { return _major; }

    /**
     * Get the length of one of the axes.
     * @return the length of an axis
     */
  public:
    inline double minorAxis() const throws()
    { return _minor; }

    /**
     * Get the angle of rotation that the major axis makes with the horizontal. The angle
     * is always normalized to the range -PI/2 and +PI/2. 
     * @return the angle (in radians) between the major axis and the positive x-axis.
     */
  public:
    inline double theta() const throws()
    { return _theta; }
    
    /**
     * Get the ellipse center.
     * @return a reference to the center point
     */
  public:
    inline const Point& center () const throws()
    { return _center; }

    /** The ellipse center */
  private:
    Point _center;

    /** The major axis length */
  private:
    double _major;

    /** The minor axis length */
  private:
    double _minor;

    /** The angle of inclination */
  private:
    double _theta;
  };
}
#endif
