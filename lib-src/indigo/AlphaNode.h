#ifndef _INDIGO_ALPHA_H
#define _INDIGO_ALPHA_H

#ifndef _INDIGO_STATENODE_H
#include <indigo/StateNode.h>
#endif


namespace indigo {
  /**
   * This node is modifies the alpha value for an material
   * rendered after this node has been encountered. If other
   * alpha nodes have been encountered during the render
   * then those are are incorporated in a multiplicative way.
   */
  class AlphaNode : public StateNode {
    
    /** Create a new alpha node. With an initial alpha value of 1. */
  public:
    AlphaNode() throws();
    
    /**
     * Create a new alpha node. If the alpha value is 
     * clamped into the interval 0 to 1.
     * @param a an alpha value between 0 and 1.
     */
  public:
    AlphaNode(double a) throws();
    
    /**
     * Create a new alpha node. If the alpha value is
     * clamped into the interval 0 to 1.
     * @param a an alpha value between 0 and 1.
     */
  public:
    AlphaNode(const AlphaNode& a) throws();

    /** Destructor */
  public:
    ~AlphaNode() throws();
    
    /**
     * Assign operator
     * @param n a node
     * @return this node
     */
  public:
    AlphaNode& operator= (const AlphaNode& n) throws();

    /**
     * Set a new alpha value. This value is applied to all nodes that follow in the scene graph.
     * If the alpha value is clamped into the interval 0 to 1.
     * @param a an alpha value between 0 and 1.
     */
  public:
    void setAlpha (double a) throws();
    
    /**
     * Get the alpha value.
     * @return a value between 0 (transparent) and 1 (opaque)
     */
  public:
    inline double alpha() const throws() { return _alpha; }
    
    /**
     * Accept the specified visitor
     * @param v a visitor
     */
  public:
    void acceptVisitor (GfxVisitor& v) const throws();
    
  private:
   double _alpha;
  };
}
#endif
