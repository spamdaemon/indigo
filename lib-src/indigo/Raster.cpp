#include <indigo/Raster.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
   Raster::Raster()
   throws()
   {}

   Raster::Raster(  ImagePtr img)
   throws()
   : _image(::std::move(img)),_corners( { {0,0}, {1, 0}, {1, -1}, {0, -1}})
   {}

   Raster::Raster(  ImagePtr img, const Point& tl, const Point& tr, const Point& bl)
   throws()
   : _image(::std::move(img)),_corners( {tl,tr,Point(tr.x() + (bl.x() - tl.x()), tr.y() + (bl.y() - tl.y())),bl})
   {}

   Raster::~Raster()
   throws()
   {
   }

   Raster::ImagePtr Raster::setImage(ImagePtr img)
   throws()
   {
      if (img!=_image) {
         _image = ::std::move(img);
         notifyChange();
      }
      return img;
   }

   void Raster::setPoints(const Point& tl, const Point& tr, const Point& bl)
   throws()
   {

      _corners[0] = tl;
      _corners[1] = tr;
      _corners[2] = Point(tr.x() + (bl.x() - tl.x()), tr.y() + (bl.y() - tl.y()));
      _corners[3] = bl;

      if (_image) {
         notifyChange();
      }
   }

   void Raster::acceptVisitor(GfxVisitor& v) const
   throws()
   {
      v.visit(*this);
   }


}
