#ifndef _INDIGO_NURBSCURVES_H
#define _INDIGO_NURBSCURVES_H

#ifndef _INDIGO_SHAPES_H
#include <indigo/Shapes.h>
#endif

#ifndef _INDIGO_NURBS_H
#include <indigo/NURBS.h>
#endif

#include <vector>

namespace indigo {
  
  /**
   * This graphic is a polyline container. All polylines
   * in this container a rendered with the same attributes.
   */
  class NurbsCurves : public Shapes<NURBS> {
    NurbsCurves&operator=(const NurbsCurves&);
    
    /** Default constructor */
  public:
    NurbsCurves () throws();
    
    /**
     * Create a polylines with a single polyline. 
     * @param p a polyline
     */
  public:
    NurbsCurves (const NURBS& p) throws();
    
    /** 
     * Copy constructor that copies the handle,
     * but keeps the same implementation.
     * @param g polylines
     */
  private:
    NurbsCurves (const NurbsCurves& g) throws();
    
    /**  Destroy this graphic handle */
  public:
    ~NurbsCurves () throws();
    
    
  protected:
    void acceptVisitor (GfxVisitor& v) const throws();
  };
}
#endif
