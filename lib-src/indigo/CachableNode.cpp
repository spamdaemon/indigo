#include <indigo/CachableNode.h>
#include <indigo/GfxObserver.h>
#include <indigo/SceneGraph.h>

using namespace ::timber;

namespace indigo {
  struct CachableNode::NodeGraph : public SceneGraph,GfxObserver {
    inline NodeGraph(CachableNode& owner) throws() : _owner(owner) {}
    
    ~NodeGraph() throws() {}
    
    void gfxChanged (const GfxEvent& e) throws()
    {
      _owner.notifyCacheDirty(); 
    }
  
    GfxObserver* createObserver (Node& node) throws()
    { return this; }
    
    CachableNode& _owner;
  };

  CachableNode::CachableNode(bool sep) throws()
  : _priority(0),_state(0),_separator(true),_attachCount(0)
  {}

  CachableNode::CachableNode(const ::timber::Pointer<Node>& n, bool separator) throws()
  : _priority(0),_node(n),_state(0),_separator(separator),_attachCount(0)
  {
  }
    
  CachableNode::~CachableNode() throws()
  {
    if (_node) {
      _node = nullptr;
      if (_root.get()) {
	_root->setRootNode(_node);
      }
    }
  }

   
  void CachableNode::setCachePriority (double p) throws()
  { _priority = p; }

  void CachableNode::setNode (const ::timber::Pointer<Node> n) throws()
  {
    if (_node!=n) {
      _node = n;
      
      // if we're attached we need to set the dirty bit
      if (_root.get()) {
	_root->setRootNode(n);
	notifyCacheDirty();
     }
    }
  }
  
  CachableNode::CacheState CachableNode::state() const throws() 
  {
    return _state; 
  }

  void CachableNode::notifyCacheDirty() throws()
  { 
    // only notify if the cache dirty bit has not been reset
    // this avoids bubbling up the same event multiple times
    // until the cache state has been at least read once
    if (_attachCount!=0) {
      ++_state;
      Node::notifyChange(); 
    }
  }

  void CachableNode::attachToSceneGraph (SceneGraph& sg) throws()
  {
    ++_attachCount;
    if (_root.get()==0) {
      _root.reset(new NodeGraph(*this));
      _root->setRootNode(_node);
    }
    Node::attachToSceneGraph(sg);
  }
  
  void CachableNode::detachFromSceneGraph (SceneGraph& sg) throws()
  {
    --_attachCount;
    Node::detachFromSceneGraph(sg);
  }


  ::timber::Pointer<Node> CachableNode::node () const throws()
  { 
    return _node;
  }
}
