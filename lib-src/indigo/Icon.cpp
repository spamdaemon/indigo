#include <indigo/Icon.h>
#include <indigo/GfxVisitor.h>

namespace indigo {
   Icon::Icon()
   throws()
   {}

   Icon::Icon(ImagePtr img)
   throws()
   : _image(::std::move(img))
   {}

   Icon::Icon(ImagePtr img, const AffineTransform2D& t)
   throws()
   : _image(::std::move(img)),_transform(t)
   {}

   Icon::~Icon()
   throws()
   {
   }

   Icon::ImagePtr Icon::setImage(ImagePtr img)
   throws()
   {
      if (img!=_image) {
         _image = ::std::move(img);
         notifyChange();
      }
      return img;
   }

   ::timber::UInt Icon::width() const
   throws()
   {  return _image ? _image->width() : 0;}

   ::timber::UInt Icon::height() const
   throws()
   {  return _image ? _image->height() : 0;}

   void Icon::acceptVisitor(GfxVisitor& v) const
   throws()
   {
      v.visit(*this);
   }
   void Icon::setTransform(const AffineTransform2D& t)
throws()
{
   _transform = t;
   if (_image) {
      notifyChange();
   }
}

}
