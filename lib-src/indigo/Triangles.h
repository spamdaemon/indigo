#ifndef _INDIGO_TRIANGLES_H
#define _INDIGO_TRIANGLES_H

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <vector>

namespace indigo {

   /**
    * This class is used to represent either a set of individual triangles,
    * a triangle strip, or a triangle fan. The vertices() method describes
    * how a triangle is defined by each type.
    *
    * @date 11 Jul 2003
    */
   class Triangles
   {

         /** The shape type */
      public:
         enum Type
         {
            TRIANGLES, //< individual triangles
            STRIP, //< a triangle strip
            FAN
         //< a triangle fan
         };

         /**
          * Create a new triangles.
          * @param n the number of points
          * @param pts an array of points
          * @param t a shape type
          * @pre REQUIRE_GREATER(n,0)
          */
      public:
         Triangles(Int n, Point* pts, Type t)throws();

         /**
          * Create a triangle string consisting of a single triangle.
          * @param a the first vertex
          * @param b the second vertex
          * @param c the third vertex
          */
      public:
         Triangles(const Point& a, const Point& b, const Point& c)throws();

         /**
          * Get the type for this triangles object.
          * @return the type of this triangles object
          */
      public:
         inline Type type() const throws()
         {  return _type;}

         /**
          * The number of triangles.
          * @return the number of triangles
          */
      public:
         inline size_t count() const throws()
         {
            if (_type==TRIANGLES) {
               return vertexCount()/3;
            }
            return vertexCount()-2;
         }

         /**
          * Get the number of points
          * @return the number of points in the triangles
          */
      public:
         inline size_t vertexCount() const throws()
         {  return _vertices.size();}

         /**
          * Get the specified vertex.
          * @param pos a vertex index
          * @return a reference to the specified vertex
          */
      public:
         inline const Point& vertex(size_t pos) const throws()
         {  return _vertices[pos];}

         /**
          * Get the vertex indicies of a triangle. This is not necessarily
          * the fastet method and is given only to be able to extract quickly
          * the vertices.<br>
          * If the type is TRIANGLES, then a triangle has
          * these vertices: <code>v0=3*i;v1=v0+1;v2=v1+1</code>.
          * If the type is FAN, then a triangle has these vertices:
          * <code>v0 = 0; v1 = t+1; v2 = v1+1;</code>
          * Finally, if the type is STRIP, then a triangle has these
          * vertices if t is an odd number: <code>v2 = t; v1=v2+1; v0=v1+1;</code>,
          * and these vertices if t is an even number: <code>v0 = t; v1=v0+1; v2=v1+1;</code>
          * @param t a triangle index
          * @param v0 the first vertex
          * @param v1 the second vertex
          * @param v2 the third vertex
          * @pre REQUIRE_RANGE(t,0,count()-1)
          */
      public:
         void vertices(size_t t, size_t& v0, size_t& v1, size_t& v2) const throws();

         /**
          * Compute vertex indices based on a triangle index.
          * @note the compiler should be able to efficiently inline this
          * method if the type is known!
          * @param t a triangle index
          * @param tp the organization of vertices
          * @param v0 the first vertex
          * @param v1 the second vertex
          * @param v2 the third vertex
          */
      public:
         static void triangleVertices(size_t t, Type tp, size_t& v0, size_t& v1, size_t& v2)throws();

         /**
          * Get  the vertices.
          * @return a vector of points
          */
      public:
         inline const ::std::vector< Point>& vertices() const throws() {return _vertices;}

         /** The point coordinates */
      private:
         ::std::vector< Point> _vertices;

         /** The type */
      private:
         Type _type;
   };

}
#endif
