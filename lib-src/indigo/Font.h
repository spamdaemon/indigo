#ifndef _INDIGO_FONT_H
#define _INDIGO_FONT_H

#ifndef _INDIGO_RECTANGLE_H
#include <indigo/Rectangle.h>
#endif

#ifndef _INDIGO_FONTDESCRIPTION_H
#include <indigo/FontDescription.h>
#endif

namespace indigo {
  /**
   * This class describes a particular
   * font that can be used to render text.

   */
  class Font : public ::timber::Counted {

    /** Default constructor */
  protected:
    inline Font () throws()
    {}
      
    /** Destroy this font */
  public:
    ~Font () throws();
    
    /**
     * Compute the size of the smallest rectangle that
     * encloses the specified string.
     * @param s a string
     * @return the rectangle that encloses the specified string
     */
  public:
    virtual Rectangle stringBounds(const ::std::string& s) const throws() = 0;
      
    /**
     * Get the font's description.
     * @return a description of this font
     */
  public:
    virtual FontDescription description() const throws() = 0;
  };
}
#endif
