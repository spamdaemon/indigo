#ifndef _INDIGO_SVG_CONTEXT_H
#define _INDIGO_SVG_CONTEXT_H

#ifndef _INDIGO_RENDER_ABSTRACTCONTEXT_H
#include <indigo/render/AbstractContext.h>
#endif

#ifndef _INDIGO_COLOR_H
#include <indigo/Color.h>
#endif

#include <iosfwd>

namespace indigo {
   namespace svg {

      /**
       * A context to render to an svg output.
       */
      class Context : public ::indigo::render::AbstractContext
      {
            /**
             * Render to the specified output stream.
             * @param stream
             */
         public:
            Context(::std::ostream& stream);

         public:
            ~Context();

            /**
             * Implemented methods
             * @{
             */
         public:
            void setAntiAliasingMode(AntiAliasingMode mode)throws();
            void setFont(const ::timber::Reference< Font>& font)throws();
            void setSolidFillColor(const Color& fillColor)throws();
            void setStrokeColor(const Color& strokeColor)throws();
            void setStrokeWidth(double width)throws();
            void drawImage(double x, double y, const ImageRef& image, ::std::int32_t w, ::std::int32_t h)throws();
            void drawImage(double x, double y, const ImageRef& image, const AffineTransform2D& t)throws();
            void drawTextUtf8(double x, double y, const ::std::string& utf8)throws();
            void paintTransformedPoints(PaintMode mode, size_t n, const Point* pts, double ptSize)throws();
            void paintTransformedPolygon(PaintMode mode, size_t n, const Point* pts, bool convex)throws();
            void paintTransformedPolyline(PaintMode mode, size_t n, const Point* pts, bool close)throws();

            /*@}*/

            /**
             * Private helper functions
             */
         private:
            void ebegin(const char* elem);
            void eend();
            void eend(const char* elem);
            void end(const char* elem);
            void attr(const char* attr, const char* value);
            void attr(const char* attr, double value);
            void emitPoints(const char* attr, const Point* pts, size_t n, bool close);
            void emitOpacity(const char* attr, double opacity);
            void emitColor(const char* attr, const Color& col);

            /** The current stroke color */
         private:
            Color _rgbaStroke;

            /** The current fill color */
         private:
            Color _rgbaFill;

            /** The stroke color */
         private:
            double _strokeWidth;

            /**
             * The output stream.
             */
         private:
            ::std::ostream& _stream;
      };
   }
}

#endif
