#ifndef _INDIGO_SVG_H
#define _INDIGO_SVG_H

#include <timber/Pointer.h>
#include <iosfwd>

namespace indigo {
   class Node;

   namespace svg {

      /**
       * Write the specified node as an SVG document to the provided output stream.
       * @param node a node
       * @param stream an output stream
       */
      void write(const ::timber::Pointer< ::indigo::Node>& node, ::std::ostream& out);
   }
}

#endif
