#include <indigo/svg/svg.h>
#include <indigo/Node.h>
#include <indigo/render/NodeRenderer.h>
#include <indigo/svg/Context.h>

namespace indigo {
   namespace svg {

      namespace {

      }

      void write(const ::timber::Pointer< ::indigo::Node>& node, ::std::ostream& out)
      {
         Context ctx(out);
         if (node) {
            auto renderer = ::indigo::render::NodeRenderer::create();
            renderer->render(node, ctx);
         }
      }

   }
}

