#include "Context.h"

#include <iostream>

#include "../AntiAliasingMode.h"
#include "../Font.h"

namespace indigo {
   namespace svg {

      static const char* SVG_HEADER = "<?xml version='1.0' standalone='no'?>"
            "\n"
            "<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>";

      Context::Context(::std::ostream& stream)
            : ::indigo::render::AbstractContext(0, 0, 1, 1, { 0, 0, 1, 1 }), _stream(stream)
      {
         _stream << SVG_HEADER << ::std::endl << "<svg xmlns='http://www.w3.org/2000/svg' version='1.1' "
               << "viewBox='0 0 1 1' " << '>' << ::std::endl;
         //    _stream << "<rect fill='white' fill-opacity='0' width='100%' height='100%'/>" << ::std::endl;
      }

      Context::~Context()
      {
         _stream << "</svg>" << ::std::endl << ::std::flush;
      }
      void Context::emitOpacity(const char* attr, double opacity)
      {
         if (opacity < 1) {
            _stream << attr << "='" << opacity << "' ";
         }
      }

      void Context::emitColor(const char* attr, const Color& col)
      {
         _stream << attr << "='";
         if (col.opacity() == 0) {
            _stream << "none";
         }
         else {
            _stream << "rgb(" << col.red() * 100.0 << "%," << col.green() * 100.0 << "%," << col.blue() * 100 << "%)";
         }
         _stream << "' ";
      }

      void Context::emitPoints(const char* attr, const Point* pts, size_t n, bool close)
      {
         char sep = '"';
         _stream << attr << '=';
         for (size_t i = 0; i < n; ++i) {
            _stream << sep << pts[i].x() << "," << pts[i].y();
            sep = ' ';
         }
         if (close && n > 1) {
            _stream << sep << pts[0].x() << "," << pts[0].y();
         }
         _stream << '"';
      }
      void Context::ebegin(const char* elem)
      {
         _stream << '<' << elem << ' ';
      }
      void Context::eend(const char*)
      {
         _stream << "/>" << ::std::endl;
      }
      void Context::eend()
      {
         _stream << '>' << ::std::endl;
      }
      void Context::end(const char* elem)
      {
         _stream << "</" << elem << '>' << ::std::endl;
      }
      void Context::attr(const char* attr, double v)
      {
         _stream << ' ' << attr << "='" << v << "' ";
      }
      void Context::attr(const char* attr, const char* v)
      {
         _stream << ' ' << attr << "='" << v << "' ";
      }

      void Context::setAntiAliasingMode(AntiAliasingMode mode)
      throws()
      {
      }

      void Context::setFont(const ::timber::Reference< Font>& font)
      throws()
      {
      }
      void Context::setSolidFillColor(const Color& fillColor)
      throws()
      {
         _rgbaFill = fillColor;
      }
      void Context::setStrokeColor(const Color& strokeColor)
      throws()
      {
         _rgbaStroke = strokeColor;
      }
      void Context::setStrokeWidth(double width)
      throws()
      {
         _strokeWidth = width;
      }

      void Context::drawImage(double x, double y, const ImageRef& image, ::std::int32_t w, ::std::int32_t h)
      throws()
      {
      }
      void Context::drawImage(double x, double y, const ImageRef& image, const AffineTransform2D& t)
      throws()
      {
      }
      void Context::drawTextUtf8(double x, double y, const ::std::string& utf8)
      throws()
      {
      }

      void Context::paintTransformedPoints(PaintMode mode, size_t n, const Point* pts, double ptSize)
      throws()
      {
      }
      void Context::paintTransformedPolygon(PaintMode mode, size_t n, const Point* pts, bool convex)
      throws()
      {
         ebegin("polygon");
         if (mode & FILL) {
            emitColor("fill", _rgbaFill);
            emitOpacity("fill-opacity", _rgbaFill.opacity());
         }
         else {
            emitOpacity("fill-opacity", 0);
         }
         if (mode & DRAW) {
            emitColor("stroke", _rgbaStroke);
            emitOpacity("stroke-opacity", _rgbaStroke.opacity());
            if (_strokeWidth != 0) {
               attr("stroke-width", _strokeWidth);
            }
         }
         emitPoints("points", pts, n, false);
         eend("polygon");
      }
      void Context::paintTransformedPolyline(PaintMode mode, size_t n, const Point* pts, bool close)
      throws()
      {
         ebegin("polyline");
         attr("fill", "none");
         if (mode & DRAW) {
            emitColor("stroke", _rgbaStroke);
            emitOpacity("stroke-opacity", _rgbaStroke.opacity());
            if (_strokeWidth != 0) {
               attr("stroke-width", _strokeWidth);
            }
         }
         else {
            emitOpacity("stroke-opacity", 0);
         }
         emitPoints("points", pts, n, false);
         eend("polyline");
      }

   }
}
