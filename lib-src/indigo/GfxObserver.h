#ifndef _INDIGO_GFXOBSERVER_H
#define _INDIGO_GFXOBSERVER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace indigo {
  /** A graphic event */
  class GfxEvent;
  
  /**
   * This class provides the interface by which 
   * changes to graphic can be propagated.
   */
  class GfxObserver {
    
    /** Destroy this observer */
  public:
    virtual ~GfxObserver() throws() = 0;
    
    /**
     * A gfx object has changed.
     * @param e a gfx event
     */
  public:
    virtual void gfxChanged (const GfxEvent& e) throws() = 0;	
  };
}
#endif
