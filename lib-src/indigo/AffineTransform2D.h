#ifndef _INDIGO_AFFINETRANSFORM2D_H
#define _INDIGO_AFFINETRANSFORM2D_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#ifndef _INDIGO_POINT_H
#include <indigo/Point.h>
#endif

#include <iosfwd>

namespace indigo {

   /**
    * This class is used to represent a transform in
    * two-dimensional space. When transforming a point (x,y),
    * it is first transformed into (x,y,1) before the transformation
    * is applied. This transform does <em>not</em> perform a homogenous
    * transform.
    */
   class AffineTransform2D
   {
         /**
          * Default constructor. Creates the
          * identity matrix and is not that useful.
          */
      public:
         constexpr AffineTransform2D()throws()
               : _matrix { 1, 0, 0, 0, 1, 0 }
         {
         }

         /**
          * Create a concatenated transform. The combined
          * transform has the same effect on a point as
          * <code>l.transformPoint(r.transformPoint(pt))</code>
          * @param l a transform on the left
          * @param r a transform on the right
          */
      public:
         AffineTransform2D(const AffineTransform2D& l, const AffineTransform2D& r)throws();

         /**
          * Create an affine transform to rotate, scale, and translate. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(tx,ty)*scale(s)*rotate(phi)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param s a uniform scale factor
          * @param tx a translation in x
          * @param ty a translation in y
          */
      public:
         inline AffineTransform2D(double phi, double s, double tx, double ty)throws()
         {
            setTransform(phi, s, tx, ty);
         }

         /**
          * Create an affine transform. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(tx,ty)*scale(sx,sy)*rotate(phi)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param sx a scale factor along x
          * @param sy a scale factor along y
          * @param tx a translation in x
          * @param ty a translation in y
          */
      public:
         inline AffineTransform2D(double phi, double sx, double sy, double tx, double ty)throws()
         {
            setTransform(phi, sx, sy, tx, ty);
         }

         /**
          * Create an affine transform. The matrix will be set like this:
          * <pre>
          *  [ m00 m01 m02 ], [m10 m11 m12 ], where m
          * </pre>
          * @param m00 first entry in the first row
          * @param m01 second entry in the first row
          * @param m02 third entry in the first row
          * @param m10 first entry in the second row
          * @param m11 second entry in the second row
          * @param m12 third entry in the second row
          */
      public:
         constexpr inline AffineTransform2D(double m00, double m01, double m02, double m10, double m11,
               double m12)throws()
               : _matrix { m00, m01, m02, m10, m11, m12 }
         {
         }

         /**
          * Create a rotation transform.
          * @param phi the rotation angle
          * @return <tt>AffineTransform2D(phi,1,0,0)</tt>
          */
      public:
         static inline AffineTransform2D createRotation(double phi)throws()
         {
            double c = ::std::cos(phi);
            double s = ::std::sin(phi);
            return AffineTransform2D(c, -s, 0, s, c, 0);
         }

         /**
          * Create a  uniform scale transform.
          * @param s the scale factor
          * @return <tt>AffineTransform2D(0,s,0,0)</tt>
          */
      public:
         static inline AffineTransform2D createScale(double s)throws()
         {
            return AffineTransform2D(s, 0, 0, 0, s, 0);
         }

         /**
          * Create a  non-uniform scale transform.
          * @param sx the scale factor
          * @param sy the scale factor
          * @return <tt>AffineTransform2D(0,sx,sy,0,0)</tt>
          */
      public:
         static inline AffineTransform2D createScale(double sx, double sy)throws()
         {
            return AffineTransform2D(sx, 0, 0, 0, sy, 0);
         }

         /**
          * Create a translation transform.
          * @param tx the translation along the x-axis
          * @param ty the translation along the y-axis
          * @return <tt>AffineTransform2D(0,1,tx,ty)</tt>
          */
      public:
         static inline AffineTransform2D createTranslation(double tx, double ty)throws()
         {
            return AffineTransform2D(1, 0, tx, 0, 1, ty);
         }

         /**
          * Determine if this transform is an identity transform. This code is equivalent
          * to <tt>equals(AffineTransform2D())</tt>.
          * @return true if this is an identity transform
          */
      public:
         bool isIdentityTransform() const throws();

         /**
          * Set the raw values of this transform. The matrix will be set like this:
          * <pre>
          *  [ m00 m01 m02 ], [m10 m11 m12 ], where m
          * </pre>
          * @param m00 first entry in the first row
          * @param m01 second entry in the first row
          * @param m02 third entry in the first row
          * @param m10 first entry in the second row
          * @param m11 second entry in the second row
          * @param m12 third entry in the second row
          * @return *this
          */
      public:
         AffineTransform2D& setRawTransform(double m00, double m01, double m02, double m10, double m11,
               double m12)throws();

         /**
          * Set the transform to rotate, scale, and translate in this order. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(tx,ty)*scale(sx,sy)*rotate(phi)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param sx a scale factor along x
          * @param sy a scale factor along y
          * @param tx a translation in x
          * @param ty a translation in y
          * @return *this
          */
      public:
         AffineTransform2D& setTransform(double phi, double sx, double sy, double tx, double ty)throws();

         /**
          * Set the transform to the inverse of the result of setTransform. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  rotate(-phi)*scale(1/sx,1/sy)*translate(-tx,-ty)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param sx a scale factor along x
          * @param sy a scale factor along y
          * @param tx a translation in x
          * @param ty a translation in y
          * @return *this
          * @see inverse()
          */
      public:
         AffineTransform2D& setInverseTransform(double phi, double sx, double sy, double tx, double ty)throws();

         /**
          * Set the transform to rotate, scale, and translate. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(tx,ty)*scale(s)*rotate(phi)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param s a uniform scale factor
          * @param tx a translation in x
          * @param ty a translation in y
          * @return *this
          */
      public:
         inline AffineTransform2D& setTransform(double phi, double s, double tx, double ty)throws()
         {
            return setTransform(phi, s, s, tx, ty);
         }

         /**
          * Set the transform to the inverse of the result of setTransform. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  rotate(-phi)*scale(1/s)*translate(-tx,-ty)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param s a uniform scale factor
          * @param tx a translation in x
          * @param ty a translation in y
          * @return *this
          */
      public:
         inline AffineTransform2D& setInverseTransform(double phi, double s, double tx, double ty)throws()
         {
            return setInverseTransform(phi, s, s, tx, ty);
         }

         /**
          * Set the matrix to translate.
          * @param tx a translation in x
          * @param ty a translation in y
          * @return *this
          */
      public:
         inline AffineTransform2D& setToTranslate(double tx, double ty)throws()
         {
            return setTransform(0, 1, tx, ty);
         }

         /**
          * Set the matrix to scale.
          * @param s uniform scale
          * @return *this
          */
      public:
         inline AffineTransform2D& setToScale(double s)throws()
         {
            return setToScale(s, s);
         }

         /**
          * Set the matrix to scale.
          * @param sx scale factor along x
          * @param sy scale factor along y
          * @return *this
          */
      public:
         inline AffineTransform2D& setToScale(double sx, double sy)throws()
         {
            return setTransform(0, sx, sy, 0, 0);
         }

         /**
          * Set the matrix to rotate.
          * @param phi the angle of rotation in radians
          * @return *this
          */
      public:
         inline AffineTransform2D& setToRotate(double phi)throws()
         {
            return setTransform(phi, 1, 0, 0);
         }

         /**
          * @name Rotating and scaling around a given center.
          * @{
          */

         /**
          * Set the transform to a rotate around a given point.This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(x,y)*rotate(phi)*translate(-x,-y)*pt
          * </pre>
          * @param phi the angle of rotation
          * @param x the x coordinate of the rotation point
          * @param y the y coordinate of the rotation point
          * @return *this
          */
      public:
         inline AffineTransform2D& setToRotate(double phi, double x, double y)throws()
         {
            return setToRotateScale(phi, 1, 1, x, y);
         }

         /**
          * Set the matrix to scale around the specified point.  This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(x,y)*scale(sx,sy)*translate(-x,-y)*pt
          * </pre>
          * @param sx scale factor in x
          * @param sy scale factor in y
          * @param x the x coordinate of the scale point
          * @param y the y coordinate of the scale point
          * @return *this
          */
      public:
         AffineTransform2D& setToScale(double sx, double sy, double x, double y)throws();

         /**
          * Set the matrix to scale around the specified point.  This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(x,y)*scale(s)*translate(-x,-y)*pt
          * </pre>
          * @param s uniform scale
          * @param x the x coordinate of the scale point
          * @param y the y coordinate of the scale point
          * @return *this
          */
      public:
         inline AffineTransform2D& setToScale(double s, double x, double y)throws()
         {
            return setToScale(s, s, x, y);
         }

         /**
          * Set the matrix to rotate and scale. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          * scale(s)*rotate(phi)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param s a scale factor
          * @return *this
          */
      public:
         inline AffineTransform2D& setToRotateScale(double phi, double s)throws()
         {
            return setTransform(phi, s, 0, 0);
         }

         /**
          * Set the matrix to rotate and scale. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          * scale(sx,sy)*rotate(phi)*pt
          * </pre>
          * @param phi the angle of rotation in radians
          * @param sx scale factor along x
          * @param sy scale factor along y
          * @return *this
          */
      public:
         inline AffineTransform2D& setToRotateScale(double phi, double sx, double sy)throws()
         {
            return setTransform(phi, sx, sy, 0, 0);
         }

         /**
          * Set the transform to a rotate and scale around a given point. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(x,y)*scale(sx,sy)*rotate(phi)*translate(-x,-y)*pt
          * </pre>
          * @param phi the angle of rotation
          * @param sx a scale factor along x
          * @param sy a scale factor along y
          * @param x the x coordinate of the rotation point
          * @param y the y coordinate of the rotation point
          * @return *this
          */
      public:
         AffineTransform2D& setToRotateScale(double phi, double sx, double sy, double x, double y)throws();

         /**
          * Set the transform to a rotate and scale around a given point. This
          * transform is equivalent to this sequence of transforms applied
          * to a point pt:
          * <pre>
          *  translate(x,y)*scale(sx)*rotate(phi)*translate(-x,-y)*pt
          * </pre>
          * @param phi the angle of rotation
          * @param s scale factor
          * @param x the x coordinate of the rotation point
          * @param y the y coordinate of the rotation point
          * @return *this
          */
      public:
         inline AffineTransform2D& setToRotateScale(double phi, double s, double x, double y)throws()
         {
            return setToRotateScale(phi, s, s, x, y);
         }

         /*@}*/

         /**
          * Rotate and scale this transform.
          * @param phi the rotation angle
          * @param s a scale factor
          * @return *this
          */
      public:
         AffineTransform2D& rotateScale(double phi, double s)throws();

         /**
          * Rotate this transform.
          * @param phi the rotation angle
          * @return *this
          */
      public:
         inline AffineTransform2D& rotate(double phi)throws()
         {
            return rotateScale(phi, 1);
         }

         /**
          * Scale this transform.
          * @param sx the scale factor along the x-axis
          * @param sy the scale factor along the y-axis
          * @return *this
          */
      public:
         AffineTransform2D& scale(double sx, double sy)throws();

         /**
          * Scale this transform.
          * @param s the scale factor
          * @return *this
          */
      public:
         inline AffineTransform2D& scale(double s)throws()
         {
            return scale(s, s);
         }

         /**
          * Translate this transform.
          * @param tx the x translation
          * @param ty the y translation
          * @return *this
          */
      public:
         inline AffineTransform2D& translate(double tx, double ty)throws()
         {
            value(0, 2) += tx;
            value(1, 2) += ty;
            return *this;
         }

         /**
          * Get the inverse of this transform. This function computes a
          * transform T such that <em> (*this) * T</em> and <em>T * (*this)</em> are
          * (ideally) the identity matrix.
          * @return an affine transform that is the inverse of this.
          * @throws std::runtime_error if the matrix is not invertible
          * or too close to degenerate matrix
          */
      public:
         AffineTransform2D inverse() const throws(::std::runtime_error);

         /**
          * Access the six matrix values.
          * @param i the row
          * @param j the column
          * @pre REQUIRE_RANGE(i,0,1)
          * @pre REQUIRE_RANGE(j,0,2)
          */
      private:
         inline double value(size_t i, size_t j) const throws()
         {
            return const_cast< AffineTransform2D&>(*this).value(i, j);
         }

         /**
          * Access the six matrix values.
          * @param i the row
          * @param j the column
          * @pre REQUIRE_RANGE(i,0,1)
          * @pre REQUIRE_RANGE(j,0,2)
          */
      private:
         inline double& value(size_t i, size_t j)throws()
         {
            assert(i <= 1);
            assert(j <= 2);
            return _matrix[i * 3 + j];
         }

         /**
          * Access a value in this matrix.
          * @param i the row
          * @param j the column
          * @pre REQUIRE_RANGE(i,0,1)
          * @pre REQUIRE_RANGE(j,0,2)
          */
      public:
         inline double operator()(size_t i, size_t j) const throws()
         {
            return value(i, j);
         }

         /**
          * Transform the specified point.
          * @param px the x-coordinate of a point
          * @param py the y-coordinate of a point
          * @return the transformed point
          */
      public:
         Point transformPoint(const double px, double py) const throws();

         /**
          * Transform the specified point.
          * @param pt a point
          * @return the transformed point
          */
      public:
         inline Point transformPoint(const Point& pt) const throws()
         {
            return transformPoint(pt.x(), pt.y());
         }

         /**
          * Transform the specified point as if it were a vector. The
          * translation component of this matrix is not applied.
          * @param vx the x-coordinate of a vector
          * @param vy the y-coordinate of a vector
          * @return the transformed vector
          */
      public:
         Point transformVector(double vx, double vy) const throws();

         /**
          * Transform the specified point as if it were a vector. The
          * translation component of this matrix is not applied.
          * @param v a point representing a vector
          * @return the transformed vector
          */
      public:
         inline Point transformVector(const Point& v) const throws()
         {
            return transformVector(v.x(), v.y());
         }

         /**
          * Compare two transforms. It is very unlikley that
          * two transforms are the same, because of numerical error.
          * @param t a transform
          * @return true if this transform is the same as t
          */
      public:
         bool equals(const AffineTransform2D& t) const throws();

         /**
          * Compute a hashvalue for this matrix
          * @return a hashvalue
          */
      public:
         Int hashValue() const throws();

         /** The transform's matrix */
      private:
         double _matrix[6];
   };
}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::AffineTransform2D& t);

#endif
