#ifndef _INDIGO_COLOR_H
#define _INDIGO_COLOR_H

#ifndef _INDIGO_H
#include <indigo/indigo.h>
#endif

#include <iosfwd>

namespace indigo {

   /**
    * This class is used to define a color
    * in the RGBA color space. Each color
    * is specified as a combination of
    * three colors red,green, and blue.
    * A fourth value, the opacity value, determines
    * if how translucent or opaque the color is.
    *
    */
   class Color
   {
         /**
          * @name Predefined colors
          * @{
          */
      public:
         static const Color RED;
         static const Color GREEN;
         static const Color BLUE;
         static const Color BLACK;
         static const Color WHITE;
         static const Color YELLOW;
         static const Color CYAN;
         static const Color MAGENTA;
         static const Color LIGHT_GREY;
         static const Color GREY;
         static const Color DARK_GREY;
         /*@}*/

         /** A transparent black */
      public:
         static const Color TRANSPARENT_BLACK;

         /**
          * Default constructor creates a opaque black.
          */
      public:
         constexpr inline Color() throws()
               : _r(0), _g(0), _b(0), _opacity(1)
         {
         }

         /**
          * Create a new color.
          * @param r the red color component [0..1]
          * @param g the green color component [0..1]
          * @param b the blue color component [0..1]
          * @param a the opacity color component [0..1]
          */
      public:
         constexpr inline Color(double r, double g, double b, double a) throws()
               : _r(r), _g(g), _b(b), _opacity(a)
         {
         }

         /**
          * Create a new color.
          * @param r the red color component [0..1]
          * @param g the green color component [0..1]
          * @param b the blue color component [0..1]
          */
      public:
         constexpr inline Color(double r, double g, double b) throws()
               : _r(r), _g(g), _b(b), _opacity(1)
         {
         }

         /** Destroy this color. */
      public:
         inline ~Color() throws()
         {
         }

         /**
          * Get the red color component.
          * @return the red color component
          */
      public:
         inline double red() const throws()
         {
            return _r;
         }

         /**
          * Get the green color component.
          * @return the green color component
          */
      public:
         inline double green() const throws()
         {
            return _g;
         }

         /**
          * Get the blue color component.
          * @return the blue color component
          */
      public:
         inline double blue() const throws()
         {
            return _b;
         }

         /**
          * Get the opacity of this color. An opacity value of 1 means is completely opaque,
          * and 0 is completely translucent.
          * @note a translucent color cannot be picked.
          * @return the opacity of this color
          */
      public:
         inline double opacity() const throws()
         {
            return _opacity;
         }

         /**
          * Create a color whose components are pre-multiplied by opacity and the given alpha value
          * @param alpha an optional alpha value (0..1)
          * @return a color whose components are pre-multiplied by opacity()*alpha, and have an alpha of opacity()*alpha
          */
      public:
         Color getPremultipliedColor (double alpha=1.0) const throws();

         /**
          * Create a new color whose color components are individually scaled. The color componts
          * are first scaled, and then clamped, which means it is possible to
          * have an individual scale value greater than 1.0.
          *
          * @param rs the red scale
          * @param gs the green scale
          * @param bs the blue scale
          * @param as the alpha scale
          * @return a new color with scaled color components.
          */
      public:
         Color getScaledColor(double rs, double gs, double bs, double as=1.0) const throws();

         /**
          * Map the color to 8 bit rgb values. Opacity is ignored by this function.
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb8(UByte& r, UByte& g, UByte& b) const throws()
         {
            r = static_cast< UByte>(_r * static_cast< UByte>(0xff));
            g = static_cast< UByte>(_g * static_cast< UByte>(0xff));
            b = static_cast< UByte>(_b * static_cast< UByte>(0xff));
         }

         /**
          * Map the color to 16 bit rgb values. Opacity is ignored by this function.
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb16(UShort& r, UShort& g, UShort& b) const throws()
         {
            r = static_cast< UShort>(_r * static_cast< UShort>(0xffff));
            g = static_cast< UShort>(_g * static_cast< UShort>(0xffff));
            b = static_cast< UShort>(_b * static_cast< UShort>(0xffff));
         }

         /**
          * Map the color to 32 bit rgb values. Opacity is ignored by this function.
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb32(UInt& r, UInt& g, UInt& b) const throws()
         {
            r = static_cast< UInt>(_r * static_cast< UInt>(0xffffffff));
            g = static_cast< UInt>(_g * static_cast< UInt>(0xffffffff));
            b = static_cast< UInt>(_b * static_cast< UInt>(0xffffffff));
         }

         /**
          * Map the color to 8 bit rgb values. Each component is multiplied
          * by the opacity. This method is primarily used by clients that
          * have no concept of opacity.
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb8a(UByte& r, UByte& g, UByte& b) const throws()
         {
            r = static_cast< UByte>(_r * _opacity * static_cast< UByte>(0xff));
            g = static_cast< UByte>(_g * _opacity * static_cast< UByte>(0xff));
            b = static_cast< UByte>(_b * _opacity * static_cast< UByte>(0xff));
         }

         /**
          * Map the color to 16 bit rgb values. Each component is multiplied
          * by the opacity. This method is primarily used by clients that
          * have no concept of opacity.
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb16a(UShort& r, UShort& g, UShort& b) const throws()
         {
            r = static_cast< UShort>(_r * _opacity * static_cast< UShort>(0xffff));
            g = static_cast< UShort>(_g * _opacity * static_cast< UShort>(0xffff));
            b = static_cast< UShort>(_b * _opacity * static_cast< UShort>(0xffff));
         }

         /**
          * Map the color to 32 bit rgb values. Each component is multiplied
          * by the opacity. This method is primarily used by clients that
          * have no concept of opacity.
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          */
      public:
         inline void rgb32a(UInt& r, UInt& g, UInt& b) const throws()
         {
            r = static_cast< UInt>(_r * _opacity * static_cast< UInt>(0xffffffff));
            g = static_cast< UInt>(_g * _opacity * static_cast< UInt>(0xffffffff));
            b = static_cast< UInt>(_b * _opacity * static_cast< UInt>(0xffffffff));
         }

         /**
          * Map the color to 8 bit rgb values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          * @param a an output parameter for the opacity (opacity) component
          */
      public:
         inline void rgba8(UByte& r, UByte& g, UByte& b, UByte& a) const throws()
         {
            r = static_cast< UByte>(_r * static_cast< UByte>(0xff));
            g = static_cast< UByte>(_g * static_cast< UByte>(0xff));
            b = static_cast< UByte>(_b * static_cast< UByte>(0xff));
            a = static_cast< UByte>(_opacity * static_cast< UByte>(0xff));
         }

         /**
          * Map the color to 16 bit rgb values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          * @param a an output parameter for the opacity (opacity) component
          */
      public:
         inline void rgba16(UShort& r, UShort& g, UShort& b, UShort& a) const throws()
         {
            r = static_cast< UShort>(_r * static_cast< UShort>(0xffff));
            g = static_cast< UShort>(_g * static_cast< UShort>(0xffff));
            b = static_cast< UShort>(_b * static_cast< UShort>(0xffff));
            a = static_cast< UShort>(_opacity * static_cast< UShort>(0xffff));
         }

         /**
          * Map the color to 32 bit rgb values
          * @param r an output parameter for the red component
          * @param g an output parameter for the green component
          * @param b an output parameter for the blue component
          * @param a an output parameter for the opacity (opacity) component
          */
      public:
         inline void rgba32(UInt& r, UInt& g, UInt& b, UInt& a) const throws()
         {
            r = static_cast< UInt>(_r * static_cast< UInt>(0xffffffff));
            g = static_cast< UInt>(_g * static_cast< UInt>(0xffffffff));
            b = static_cast< UInt>(_b * static_cast< UInt>(0xffffffff));
            a = static_cast< UInt>(_opacity * static_cast< UInt>(0xffffffff));
         }

         /**
          * Normalize this color setting multiplicy the different color channels
          * by the opacity value and then settingthe opacity to 1.
          * @return this color
          */
      public:
         Color& normalize() throws();

         /**
          * A hashcode for this color.
          * @return a hashcode for this font
          */
      public:
         Int hashValue() const throws();

         /**
          * Compare for the equality of two colors by value; the comparison
          * is done by RGB values, which are adjuste for the opacity.
          * @param c a color
          * @return true if c and this color are actually the same
          */
      public:
         bool equals(const Color& c) const throws();

         /**
          * Check the two colors are the same.
          * @param f a color
          * @return true if this and f are the same RGB color.
          */
      public:
         inline bool operator==(const Color& f) const throws()
         {
            return equals(f);
         }

         /**
          * Check the two colors are different.
          * @param f a color
          * @return true if this and f are not the same RGB color.
          */
      public:
         inline bool operator!=(const Color& f) const throws()
         {
            return !equals(f);
         }

         /** The r,g,b,a values */
      private:
         double _r, _g, _b, _opacity;
   };
}

::std::ostream& operator<<(::std::ostream& out, const ::indigo::Color& c);
#endif
