#ifndef _INDIGO_SHAPES_H
#define _INDIGO_SHAPES_H

#ifndef _INDIGO_NODE_H
#include <indigo/Node.h>
#endif

#ifndef _INDIGO_SHAPESEVENT_H
#include <indigo/ShapesEvent.h>
#endif

#include <vector>

namespace indigo {

   /**
    * This graphic is a shape container. All shapes
    * in this container a rendered with the same attributes.
    * This class should not be used directly, but only predefined subclasses
    * should be used.
    */
   template<class SHAPE> class Shapes : public Node
   {
         Shapes& operator=(const Shapes&);

         /** The shape type */
      public:
         typedef SHAPE Shape;

         /** Default constructor */
      protected:
         inline Shapes()throws()
         {}

         /**
          * Create a shapes consisting of a single shape.
          * @param sh a shape
          */
      protected:
         inline Shapes(const Shape& sh)throws()
         {
            _shapes.reserve(1);
            _shapes.push_back(sh);
         }

         /**
          * Create a shapes consisting of a single shape.
          * @param n the number of initial shapes
          * @param sh an array of shapes
          * @todo suspicious non-reference used!
          */
      protected:
         inline Shapes(size_t n, const Shape* sh)throws()
         {
            _shapes.reserve(n);
            for (size_t i=0;i<n;++i) {
               _shapes.push_back(sh[i]);
            }
         }

         /**
          * Copy constructor that copies the handle,
          * but keeps the same implementation.
          */
      protected:
         inline Shapes(const Shapes& g)throws()
         : Node(),_shapes(g._shapes) {}

         /**  Destroy this graphic handle */
      public:
         ~Shapes()throws()
         {}

         /**
          * Get the number of children.
          * @return the number of children
          */
      public:
         inline size_t count() const throws()
         {  return _shapes.size();}

         /**
          * Get the shape at the specified index.
          * @param pos an index
          * @pre REQUIRE_RANGE(pos,0,count()-1)
          * @return the shape at the specified index
          * @note the value of the returned reference must be assumed
          * undefined if a modification is made to this object
          */
      public:
         inline const Shape& shape(size_t pos) const throws()
         {  return _shapes[pos];}

         /**
          * Get the shape at the specified index.
          * @param pos a shape index
          * @pre REQUIRE_RANGE(pos,0,count()-1)
          * @return a shape
          * @note the value of the returned reference must be assumed
          * undefined if a modification is made to this object
          */
      public:
         inline const Shape& operator[](size_t pos) const throws()
         {  return shape(pos);}

         /**
          * Get the shapes.
          * @return a vector of shapes
          */
      public:
         inline const ::std::vector< Shape>& shapes() const throws() {return _shapes;}

         /**
          * Set the shape at the specified index.
          * @param p a shape
          * @param pos the new shape's position
          */
      public:
         void set(const Shape& p, size_t pos)throws()
         {
            assert(pos<=count());
            if (pos==count()) {
               _shapes.push_back(p);
               notifyAdded(pos);
            }
            else {
               _shapes[pos] = p;
               notifyModified(pos);
            }
         }

         /**
          * Insert a shape at the specified position. Calls fireEvent()
          * if a change was made.
          * @param p the new shape
          * @param pos a position
          * @pre REQUIRE_RANGE(pos,0,count())
          */
      public:
         void insert(const Shape& p, size_t pos)throws()
         {
            assert(pos>=0 && pos<=count());
            if (pos==count()) {
               _shapes.push_back(p);
            }
            else {
               _shapes.insert(_shapes.begin()+pos,p);
            }
            notifyAdded(pos);
         }

         /**
          * Add a child. Calls fireEvent().
          * @param p the new shape
          */
      public:
         inline void add(const Shape& p)throws()
         {  set(p,count());}

         /**
          * Remove the child at the specified position. Calls fireEvent().
          * @param pos a position
          * @pre REQUIRE_RANGE(pos,0,count()-1)
          * @return the shape previously at the position
          */
      public:
         Shape remove(size_t pos)throws()
         {
            Shape p (_shapes[pos]);
            _shapes.erase(_shapes.begin()+pos);
            notifyRemoved(pos);
            return p;
         }

         /** Remove all shapes. */
      public:
         void clear()throws()
         {
            if (count()>0) {
               _shapes.clear();
               notifyCleared();
            }
         }

         /**
          * Notify the observer.
          * @param pos the child position that wass added
          */
      private:
         void notifyAdded(size_t pos)throws()
         {
            checkIndex(pos);
            if (observerCount()>0) {
               fireEvent(ShapesEvent(this,ShapesEvent::ADDED,pos));
            }
         }

         /**
          * Notify the observer.
          * @param pos the child position that has changed
          */
      private:
         void notifyModified(size_t pos)throws()
         {
            checkIndex(pos);
            if (observerCount()>0) {
               fireEvent(ShapesEvent(this,ShapesEvent::MODIFIED,pos));
            }
         }

         /**
          * Notify the observer.
          * @param pos the child position that has changed
          */
      private:
         void notifyRemoved(size_t pos)throws()
         {
            checkIndex(pos);
            if (observerCount()>0) {
               fireEvent(ShapesEvent(this,ShapesEvent::REMOVED,pos));
            }
         }

         /**
          * Notify the observer. The position may be an invalid index
          * to indicate that all shapes have been removed.
          * @param pos the child position that has changed
          */
      private:
         void notifyCleared()throws()
         {
            if (observerCount()>0) {
               fireEvent(ShapesEvent(this,ShapesEvent::REMOVED,invalidIndex()));
            }
         }

         /** The children */
      private:
         ::std::vector< Shape> _shapes;
   };
}
#endif
