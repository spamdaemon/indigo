#include <indigo/svg/svg.h>
#include <indigo/nodes.h>
#include <iostream>

using namespace ::timber;
using namespace ::indigo;

static Reference< Node> createScene()
{
   Reference< Group> node(new Group());
   Reference< Group> shapes = new Group();
   node->add(shapes);
   shapes->add(new SolidFillNode(Color::YELLOW));
   shapes->add(new StrokeNode(Color::RED, 0.01));
   shapes->add(new TriangleSet(Triangles(Point(0, 0), Point(.5, 0), Point(0, .5))));
   shapes->add(new StrokeNode(Color::GREEN, 0.01));
   shapes->add(new PolyLines(PolyLine(Point(1, 1), Point(.5, 1), Point(1, .5))));
   return node;
}

int main()
{
   ::indigo::svg::write(createScene(), ::std::cout);
   return 0;
}

