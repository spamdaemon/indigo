#include <iostream>
#include <indigo/SceneGraph.h>
#include <indigo/GfxEvent.h>
#include <indigo/GfxObserver.h>
#include <indigo/ThreadSafeNode.h>
#include <indigo/AlphaNode.h>

using namespace std;
using namespace timber;
using namespace indigo;

struct Root : public SceneGraph, GfxObserver
{
      Root()
      {
      }
      ~Root()throws() {SceneGraph::setRootNode(::timber::Pointer<Node>());}

      GfxObserver* createObserver(Node& node)throws()
      {  return this;}

      void gfxChanged(const GfxEvent& e)throws()
      {
         _node = e.source();
      }

      Pointer< Node> _node;
};

// check that a swap works and results in a proper node
void utest_1()
{
   // setup
   Root root;
   Reference< ThreadSafeNode> g0 = ThreadSafeNode::create();
   root.setRootNode(g0);
   root._node = nullptr;
   Reference< AlphaNode> alpha = new AlphaNode(.5);

   // test
   g0->setChild(alpha);
   assert(g0->child() == alpha);
   assert(root._node != nullptr);
}

// verify that a change to the front buffer is propagated to the top, but the backbuffer isn't
void utest_2()
{
   // setup
   Root root;
   Reference< ThreadSafeNode> g0 = ThreadSafeNode::create();
   root.setRootNode(g0);
   Reference< AlphaNode> alpha = new AlphaNode(.5);
   g0->setChild(alpha);
   root._node = nullptr;

   // test
   alpha->setAlpha(1);
   assert(root._node == alpha);
}
