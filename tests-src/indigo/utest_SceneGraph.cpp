#include <iostream>
#include <indigo/SceneGraph.h>
#include <indigo/Group.h>
#include <indigo/GfxObserver.h>

using namespace std;
using namespace timber;
using namespace indigo;

static Int value(0);
static Int observerCount(0);

struct MyGroup : public Group {
  ~MyGroup() throws() {}
public:
  inline Int observerSize() const throws() { return observerCount(); }
};

struct MyObserver : public GfxObserver {
  
  ~MyObserver() throws()
  { --observerCount; }
  MyObserver() throws()
  {
    ++observerCount;
  }

  void gfxChanged (const GfxEvent& event) throws()
  {
    ::std::cerr << "GFX EVENT\n" << ::std::flush;
    ++value;
  }
};

struct MySceneGraph : public SceneGraph {

  ~MySceneGraph () throws() 
  {
     setRootNode(Pointer<Node>());
  }

  GfxObserver* createObserver (Node& n) throws()
  { return new MyObserver(); }
  void destroyObserver (GfxObserver* obs) throws()
  {
    delete obs;
  }
};

void utest_1()
{
  {
    Pointer<MyGroup> g0(new MyGroup());
    g0->add(new MyGroup());
    Pointer<MyGroup> g1(new MyGroup());
    g1->add(new MyGroup());
    g0->add(g1);
    
    MySceneGraph sc1,sc2;
    sc1.setRootNode(g0);
    assert(observerCount==4);
    sc2.setRootNode(g1);
    assert(observerCount==6);
    assert(g0->observerSize()==1);
    assert(g1->observerSize()==2);
    value = 0;
    MyGroup* gg = new MyGroup();
    g0->add(gg);
    assert(observerCount==7);
    assert(value==1);
    value = 0;
    g1->add(new MyGroup());
    assert(observerCount==9);
    assert(value==2);
  }
  assert(observerCount==0);
}

void utest_2()
{
  assert(observerCount==0);
  Pointer<MyGroup> g0(new MyGroup());
  Pointer<MyGroup> g1(new MyGroup());
  Pointer<MyGroup> g2(new MyGroup());
  Pointer<MyGroup> g3(new MyGroup());
  
  g0->add(g1);
  g0->add(g2);
  g1->add(g3);
  g2->add(g3);

  MySceneGraph sc1;
  sc1.setRootNode(g0);
  value = 0;
  g3->add(new Group());
  assert(value==1);
  sc1.setRootNode(Pointer<Node>());
  assert(observerCount==0);
}

