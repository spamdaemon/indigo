#include <iostream>
#include <indigo/SceneGraph.h>
#include <indigo/GfxEvent.h>
#include <indigo/GfxObserver.h>
#include <indigo/DoubleBufferedNode.h>
#include <indigo/AlphaNode.h>

using namespace std;
using namespace timber;
using namespace indigo;

struct Root : public SceneGraph, GfxObserver
{
      Root()
            : _count(0)
      {
      }
      ~Root()throws()
      {
         SceneGraph::setRootNode(::timber::Pointer< Node>());
      }

      GfxObserver* createObserver(Node& node)throws()
      {
         return this;
      }

      void gfxChanged(const GfxEvent& e)throws()
      {
         ++_count;
         _node = e.source();
         ::std::cerr << "gfxChanged " << _count << ::std::endl;
      }

      int _count;
      Pointer< Node> _node;
};

// check that a swap works and results in a proper node
void utest_1()
{
   Root root;
   Reference< DoubleBufferedNode> g0 = DoubleBufferedNode::create();
   root.setRootNode(g0);
   root._node = nullptr;

   Reference< AlphaNode> alpha = new AlphaNode(.5);
   g0->setBackBufferNode(alpha);
   assert(g0->backBufferNode() == alpha);
   assert(root._node == nullptr);

   g0->swapBuffers();
   assert(root._node != nullptr);

   assert(g0->frontBufferNode() == alpha);

}

// verify that a change to the front buffer is propagated to the top, but the backbuffer isn't
void utest_2()
{

   Root root;
   Reference< DoubleBufferedNode> g0 = DoubleBufferedNode::create();
   root.setRootNode(g0);
   root._node = nullptr;

   Reference< AlphaNode> alpha1 = new AlphaNode(.5);
   Reference< AlphaNode> alpha2 = new AlphaNode(.5);
   g0->setBackBufferNode(alpha1);
   g0->swapBuffers();
   g0->setBackBufferNode(alpha2);

   assert(g0->frontBufferNode() == alpha1);
   assert(g0->backBufferNode() == alpha2);

   root._node = nullptr;
   alpha1->setAlpha(1);
   assert(root._node == alpha1);
   root._node = nullptr;
   alpha2->setAlpha(.01);
   assert(!root._node);
}

// test use of 1 single node for both the front and back
// this is dumb, but should be dealt with correctly
void utest_3()
{
   // setup
   Root root;
   Reference< DoubleBufferedNode> g0 = DoubleBufferedNode::create();
   root.setRootNode(g0);
   root._node = nullptr;
   Reference< AlphaNode> alpha = new AlphaNode(.5);
   g0->setBackBufferNode(alpha);
   g0->swapBuffers();
   g0->setBackBufferNode(alpha);
   root._count = 0;

   // test
   ::std::cerr << "\n\n start test";
   alpha->setAlpha(1);

   assert(root._count == 1);

}
