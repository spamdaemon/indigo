#include <indigo/Color.h>

using namespace indigo;

void utest_1()
{
  Color blue1(0.,0.,.5);
  Color blue2(0.,0.,1.0,0.5);

  assert(!blue1.equals(blue2));
}

void utest_2()
{
   Color b1(.5,.5,.5,.5);
   Color b2(.5,.5,.5,.75);
   assert(b1!=b2);
}

void utest_getScaledColor()
{
   Color b1(.125,.25,.5,.25);
   Color s1 = b1.getScaledColor(10,10,10,10);
   assert (s1.red()==1.0);
   assert (s1.green()==1.0);
   assert (s1.blue()==1.0);
   assert (s1.opacity()==1.0);
}

void utest_getPremultipliedColor()
{
   Color b1(1,.5,1,.5);
   Color s1 = b1.getPremultipliedColor(.5);
   assert (s1.red()==.25);
   assert (s1.green()==.125);
   assert (s1.blue()==.25);
   assert (s1.opacity()==.25);
}

void utest_getNormalizedColor()
{
   Color b1(1,.5,.25,.5);
   Color s1 = b1.normalize();
   assert (s1.red()==.5);
   assert (s1.green()==.25);
   assert (s1.blue()==.125);
   assert (s1.opacity()==1);
}
