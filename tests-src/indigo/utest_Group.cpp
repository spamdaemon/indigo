#include <iostream>
#include <indigo/Group.h>

using namespace std;
using namespace timber;
using namespace indigo;

void utest_1()
{
  Pointer<Group> g0(new Group(2,false));
  g0->set(new Group(),0);
  g0->add(Pointer<Group>());
}

void utest_2()
{
   Reference<Node> n1(new Group());
   Reference<Node> n2(new Group());
   Reference<Node> n3(new Group());

   Reference<Group> g(new Group(n1,n2,n3));

   assert (g->node(0)==n1);
   assert (g->node(1)==n2);
   assert (g->node(2)==n3);

}
