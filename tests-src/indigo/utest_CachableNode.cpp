#include <iostream>
#include <indigo/SceneGraph.h>
#include <indigo/CachableNode.h>
#include <indigo/GfxObserver.h>
#include <indigo/Group.h>

using namespace std;
using namespace timber;
using namespace indigo;

struct MyObserver : public GfxObserver {
  
  ~MyObserver() throws()
  { }
  MyObserver() throws()
  :value(0)
  {
  }

  void gfxChanged (const GfxEvent& event) throws()
  {
    ::std::cerr << "GFX EVENT\n" << ::std::flush;
    ++value;
  }

  size_t value;
};

struct MySceneGraph : public SceneGraph {

  MySceneGraph (MyObserver& obs) : _obs(obs) {}
  
  ~MySceneGraph () throws() 
  {
    setRootNode(Pointer<Node>());
  }

  GfxObserver* createObserver (Node& n) throws()
  { return &_obs; }
  void destroyObserver (GfxObserver* obs) throws()
  { }
  
  MyObserver _obs;
};

void utest_1()
{
  MyObserver observer;
  MySceneGraph G(observer);
  
  Reference<CachableNode> cache(new CachableNode());
  G.setRootNode(cache);
  
  Pointer<Group> g0(new Group(2,false));
  g0->set(new Group(),0);

  CachableNode::CacheState state = cache->state();

  cache->setNode(g0);
  assert(state != cache->state());
  state=cache->state();
  g0->add(Pointer<Group>());
  assert(state != cache->state());
  
}
