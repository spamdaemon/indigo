#include <iostream>
#include <indigo/AffineTransform2D.h>

using namespace indigo;

void utest_test1()
{
  AffineTransform2D t;
  t.setToRotateScale(M_PI,2,1,1);
  Point p = t.transformPoint(Point(2,1));
  assert(p.equals(Point(-1,1),0.0625));
}

void utest_test2()
{
  AffineTransform2D t;
  t.setTransform(M_PI,2,4,1,1);
  
  AffineTransform2D tinv;
  tinv.setInverseTransform(M_PI,2,4,1,1);

  AffineTransform2D res(t,tinv);

  ::std::cerr << res << ::std::endl;
  assert(::std::abs(res(0,0)-1.0)<0.00625);
  assert(::std::abs(res(1,1)-1.0)<0.00625);

  assert(::std::abs(res(0,1))<0.00625);
  assert(::std::abs(res(1,0))<0.00625);
  assert(::std::abs(res(0,2))<0.00625);
  assert(::std::abs(res(1,2))<0.00625);
}

void utest_test3()
{
  AffineTransform2D t;
  t.setTransform(M_PI,2,4,1,1);
  ::std::cerr << t << ::std::endl;
  AffineTransform2D tinv(t.inverse());
  ::std::cerr << tinv << ::std::endl;
  {
    AffineTransform2D res(t,tinv);
    ::std::cerr << res << ::std::endl;
    assert(::std::abs(res(0,0)-1.0)<0.0000625);
    assert(::std::abs(res(1,1)-1.0)<0.0000625);
    
    assert(::std::abs(res(0,1))<0.0000625);
    assert(::std::abs(res(1,0))<0.0000625);
    assert(::std::abs(res(0,2))<0.0000625);
    assert(::std::abs(res(1,2))<0.0000625);

    Point pt = t.transformPoint(tinv.transformPoint(61,11));
    assert(::std::abs(pt.x()-61.0)<.000625);
    assert(::std::abs(pt.y()-11.0)<.000625);
  }
  {
    AffineTransform2D res(tinv,t);
    ::std::cerr << res << ::std::endl;
    assert(::std::abs(res(0,0)-1.0)<0.0000625);
    assert(::std::abs(res(1,1)-1.0)<0.0000625);
    
    assert(::std::abs(res(0,1))<0.0000625);
    assert(::std::abs(res(1,0))<0.0000625);
    assert(::std::abs(res(0,2))<0.0000625);
    assert(::std::abs(res(1,2))<0.0000625);


    Point pt = tinv.transformPoint(t.transformPoint(61,11));
    assert(::std::abs(pt.x()-61.0)<.000625);
    assert(::std::abs(pt.y()-11.0)<.000625);
  }

  try {
    t = AffineTransform2D(2,2,2,3,3,3);
    tinv = t.inverse();
    assert(false);
  }
  catch (const ::std::exception& e) {
  }
  
}

void utest_setToScale()
{
   Point p(0,10);

   AffineTransform2D t;
   t.setToScale(2,0.5,p.x(),p.y());

   Point q = t.transformPoint(p);
   ::std::cerr << "p: " << p << ::std::endl
         << "q: " << q << ::std::endl;
   assert (q==p);
}

void utest_setToRotate()
{
   Point p(0,10);
   AffineTransform2D t;
   t.setToRotate(M_PI/4,p.x(),p.y());

   Point q = t.transformPoint(p);
   ::std::cerr << "p: " << p << ::std::endl
         << "q: " << q << ::std::endl;
   assert (q==p);


}

void utest_Constructors()
{
   AffineTransform2D x;
   assert(x.isIdentityTransform());
   assert(x(0,0)==1);
   assert(x(0,1)==0);
   assert(x(0,2)==0);
   assert(x(1,0)==0);
   assert(x(1,1)==1);
   assert(x(1,2)==0);

   AffineTransform2D y(1,0,0,0,1,0);
   assert(x.equals(y));

   AffineTransform2D z(1,2,3,4,5,6);
   assert(z(0,0)==1);
   assert(z(0,1)==2);
   assert(z(0,2)==3);
   assert(z(1,0)==4);
   assert(z(1,1)==5);
   assert(z(1,2)==6);

}
