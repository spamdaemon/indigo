#include <indigo/Triangles.h>
#include <cassert>

using namespace indigo;

void test1()
{
  Point pts[5];
  pts[0] = Point(0,0);
  pts[1] = Point(0,1);
  pts[2] = Point(1,0);
  pts[3] = Point(1,1);
  pts[4] = Point(2,0);
  Triangles t(5,pts,Triangles::STRIP);

  Index v0,v1,v2;
  t.vertices(0,v0,v1,v2);
  assert(v0==0);
  assert(v1==1);
  assert(v2==2);
  t.vertices(1,v0,v1,v2);
  assert(v0==3);
  assert(v1==2);
  assert(v2==1);
  t.vertices(2,v0,v1,v2);
  assert(v0==2);
  assert(v1==3);
  assert(v2==4);
}

