#include <iostream>
#include <indigo/RSTTransform2D.h>

using namespace indigo;

void test1()
{
  RSTTransform2D t;
  t.setTransform(M_PI,2,1,2);
  
  RSTTransform2D tinv;
  tinv.setInverseTransform(M_PI,2,1,2);

  AffineTransform2D res(t.transform(),tinv.transform());

  ::std::cerr << res << ::std::endl;
  assert(::std::abs(res(0,0)-1.0)<0.00625);
  assert(::std::abs(res(1,1)-1.0)<0.00625);

  assert(::std::abs(res(0,1))<0.00625);
  assert(::std::abs(res(1,0))<0.00625);
  assert(::std::abs(res(0,2))<0.00625);
  assert(::std::abs(res(1,2))<0.00625);
}

