#include <iostream>
#include <indigo/view/IPixel.h>

using namespace std;
using namespace indigo::view;

void utest_Rounding_IntegerCoordinates()
{
   for (int i = -5; i <= 5; ++i) {
      for (int j = -5; j <= -5; ++j) {
         Pixel p(i, j);
         IPixel ip = IPixel::fromPixel(p);
         Pixel q = ip.toPixel();

         assert(p.equals(q, sqrt(0.5)));
      }
   }
}

void utest_Rounding_FloatCoordinates()
{
   for (double i = -5.1; i <= 5; i += .1) {
      for (double j = -5; j <= -5; j += .1) {
         Pixel p(i, j);
         IPixel ip = IPixel::fromPixel(p);
         Pixel q = ip.toPixel();

         cerr << "p : " << p << endl << "ip:" << ip << endl << "q :" << q << endl;
         assert(p.equals(q, sqrt(0.5)));
      }
   }
}

void utest_RoundingHalfway()
{
   for (double i = -5; i <= 5; i += .5) {
      for (double j = -5; j <= -5; j += .5) {
         Pixel p(i, j);
         IPixel ip = IPixel::fromPixel(p);
         Pixel q = ip.toPixel();

         cerr << "p : " << p << endl << "ip:" << ip << endl << "q :" << q << endl;
         assert(p.equals(q, sqrt(0.5)));
      }
   }
}

void utest_Halfway()
{
   {
      Pixel p(0, 0);
      IPixel ip = IPixel::fromPixel(p);
      cerr << "p : " << p << endl << "ip:" << ip << endl;
      assert (ip.x()==0 && ip.y()==0);
   }
}
