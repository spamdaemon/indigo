#include <iostream>
#include <indigo/view/ViewPort.h>

using namespace std;
using namespace indigo;
using namespace indigo::view;

//static double SMALL_DOUBLE = 1.0 / (1 << 30);
//static double LARGE_DOUBLE = 1 << 30;

static void assertDouble(double value, double expected, double eps = 1E-8)
{
   assert(::std::abs(value - expected) <= eps);
}

struct MyViewPort : public ::indigo::view::ViewPort
{
      MyViewPort()throws()
      {}

      MyViewPort(const Bounds& b, const Window& view)throws ( ::std::invalid_argument)
      : ViewPort(b,view) {}

      ~MyViewPort()throws()
      {}

      MyViewPort& operator=(const MyViewPort& src)throws()
      {
         ViewPort::operator=(src);
         return *this;
      }

   public:
      using ViewPort::resizeBounds;
      using ViewPort::resizeViewWindow;
      using ViewPort::setViewPort;
      using ViewPort::setViewWindow;
};

void utest_DefaultConstructor()
{
   MyViewPort vp;
   assert(vp.horizontalScale() == 1);
   assert(vp.verticalScale() == 1);
   assert(vp.viewWindow().width() == 1);
   assert(vp.viewWindow().height() == 1);
   assert(vp.bounds().x() == 0);
   assert(vp.bounds().y() == 0);
   assert(::std::abs(vp.bounds().rotation())<0.000001);
   assert(vp.bounds().width() == 1);
   assert(vp.bounds().height() == 1);
}

void utest_Constructor()
{
   Bounds b(-1, -1, 2, 2, M_PI);
   ViewPort::Window win(4, 8);

   MyViewPort vp(b, win);

   assert(vp.horizontalScale() == 2);
   assert(vp.verticalScale() == 4);
   assert(vp.viewWindow().width() == 4);
   assert(vp.viewWindow().height() == 8);
   assert(vp.bounds().x() == -1);
   assert(vp.bounds().y() == -1);
   assert(::std::abs(vp.bounds().rotation()-M_PI)<0.0001);
   assert(vp.bounds().width() == 2);
   assert(vp.bounds().height() == 2);

   vp = MyViewPort();

   assert(vp.horizontalScale() == 1);
   assert(vp.verticalScale() == 1);
   assert(vp.viewWindow().width() == 1);
   assert(vp.viewWindow().height() == 1);
   assert(vp.bounds().x() == 0);
   assert(vp.bounds().y() == 0);
   assert(vp.bounds().rotation() == 0);
   assert(vp.bounds().width() == 1);
   assert(vp.bounds().height() == 1);

}

void utest_setViewPort()
{
   MyViewPort vp;
   Bounds b(-1, -1, 2, 2, M_PI);
   ViewPort::Window win(4, 4);

   vp.setViewPort(b, win);
   assert(vp.horizontalScale() == 2);
   assert(vp.verticalScale() == 2);
   assert(vp.viewWindow().width() == 4);
   assert(vp.viewWindow().height() == 4);
   assert(vp.bounds().x() == -1);
   assert(vp.bounds().y() == -1);
   assert(::std::abs(vp.bounds().rotation()-M_PI)<0.0001);
   assert(vp.bounds().width() == 2);
   assert(vp.bounds().height() == 2);
}

void utest_setBounds()
{
   ViewPort::Window win(4, 4);
   MyViewPort vp(Bounds(), win);
   Bounds b(-1, -1, 2, 2, M_PI);

   vp.setBounds(b);
   assert(vp.horizontalScale() == 2);
   assert(vp.verticalScale() == 2);
   assert(vp.viewWindow().width() == 4);
   assert(vp.viewWindow().height() == 4);
   assert(vp.bounds().x() == -1);
   assert(vp.bounds().y() == -1);
   assert(::std::abs(vp.bounds().rotation()-M_PI)<0.0001);
   assert(vp.bounds().width() == 2);
   assert(vp.bounds().height() == 2);
}
void utest_setViewWindow()
{
   Bounds b(-1, -1, 2, 2, M_PI);
   MyViewPort vp(b, ViewPort::Window(1, 1));
   ViewPort::Window win(4, 4);

   vp.setViewWindow(win);
   assert(vp.horizontalScale() == 2);
   assert(vp.verticalScale() == 2);
   assert(vp.viewWindow().width() == 4);
   assert(vp.viewWindow().height() == 4);
   assert(vp.bounds().x() == -1);
   assert(vp.bounds().y() == -1);
   assert(::std::abs(vp.bounds().rotation()-M_PI)<0.0001);
   assert(vp.bounds().width() == 2);
   assert(vp.bounds().height() == 2);
}

void utest_AffineTransforms()
{
   Bounds b(-1, -1, 2, 2, M_PI);
   ViewPort::Window win(4, 4);
   MyViewPort vp(b, win);

   AffineTransform2D modelView = vp.getModelViewTransform();
   AffineTransform2D viewModel = vp.getViewModelTransform();

   // check the original point
   {
      double x = -1;
      double y = -1;
      Point viewP = modelView.transformPoint(Point(x, y));
      assertDouble(viewP.x(), 0);
      assertDouble(viewP.y(), 0);
      Point modelP = viewModel.transformPoint(viewP);
      assertDouble(modelP.x(), x);
      assertDouble(modelP.y(), y);
   }

   // check the opposite point
   {
      double x, y;
      b.getOppositePoint(x, y);
      Point viewP = modelView.transformPoint(Point(x, y));
      assertDouble(viewP.x(), 4);
      assertDouble(viewP.y(), 4);
      Point modelP = viewModel.transformPoint(viewP);
      assertDouble(modelP.x(), x);
      assertDouble(modelP.y(), y);
   }
}

// test the transformation of getPixel and getPoint; they do not
// necessarily use AffineTransform2D
void utest_PixelPointTransforms()
{
   Bounds b(-1, -1, 2, 2, M_PI);
   ViewPort::Window win(4, 4);
   MyViewPort vp(b, win);

   // check the original point
   {
      double x = -1;
      double y = -1;
      ViewPort::Pixel viewP = vp.getPixel(x, y);
      assertDouble(viewP.x(), 0);
      assertDouble(viewP.y(), 0);
      ViewPort::Point modelP = vp.getPoint(viewP);
      assertDouble(modelP.x(), x);
      assertDouble(modelP.y(), y);
   }

   {
      double x = -1;
      double y = -1;
      b.getOppositePoint(x, y);
      ViewPort::Pixel viewP = vp.getPixel(x, y);
      assertDouble(viewP.x(), 4);
      assertDouble(viewP.y(), 4);
      ViewPort::Point modelP = vp.getPoint(viewP);
      assertDouble(modelP.x(), x);
      assertDouble(modelP.y(), y);
   }
}

void utest_centerPointPixel()
{
   Bounds b(-1, -1, 2, 2, M_PI);
   ViewPort::Window win(4, 8);
   MyViewPort vp(b, win);

   {
      Pixel pixel = vp.centerPixel();
      assertDouble(pixel.x(), 2);
      assertDouble(pixel.y(), 4);

      Point point = vp.centerPoint();
      assertDouble(point.x(), -2);
      assertDouble(point.y(), -2);
   }
}

void utest_resizeViewPort()
{
   // choose a scale of 1:1
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(4, 8);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   vp.resizeViewWindow(1, 1, 1, 1);

   ::std::cerr << "ViewPort " << vp << ::std::endl;
   assertDouble(vp.horizontalScale(), 1, 0);
   assertDouble(vp.verticalScale(), 1, 0);
   assertDouble(vp.viewWindow().width(), 6, 0);
   assertDouble(vp.viewWindow().height(), 10, 0);
   assertDouble(vp.bounds().x(), 0);
   assertDouble(vp.bounds().y(), 0);
   assertDouble(vp.bounds().rotation(), M_PI, 0);
   assertDouble(vp.bounds().width(), 6, 0);
   assertDouble(vp.bounds().height(), 10, 0);
   assertDouble(orig.horizontalScale(), vp.horizontalScale());
   assertDouble(orig.verticalScale(), vp.verticalScale());
}
void utest_resizeBounds()
{
   // choose a scale of 1:1
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(4, 8);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   vp.resizeBounds(1, 1, 1, 1);
   ::std::cerr << "ViewPort " << vp << ::std::endl;

   assertDouble(vp.horizontalScale(), 1, 0);
   assertDouble(vp.verticalScale(), 1, 0);
   assertDouble(vp.viewWindow().width(), 6, 0);
   assertDouble(vp.viewWindow().height(), 10, 0);
   assertDouble(vp.bounds().x(), 0);
   assertDouble(vp.bounds().y(), 0);
   assertDouble(vp.bounds().rotation(), M_PI, 0);
   assertDouble(vp.bounds().width(), 6, 0);
   assertDouble(vp.bounds().height(), 10, 0);
   assertDouble(orig.horizontalScale(), vp.horizontalScale());
   assertDouble(orig.verticalScale(), vp.verticalScale());
}

void utest_translateBounds()
{
   // choose a scale of 1:1
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(1, 1);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   vp.translateBounds(-1, -1);
   ::std::cerr << "ViewPort " << vp << ::std::endl;
   assertDouble(vp.bounds().x(), 0);
   assertDouble(vp.bounds().y(), 0);
   assert(orig.horizontalScale() == vp.horizontalScale());
   assert(orig.verticalScale() == vp.verticalScale());
}

void utest_translateViewPort()
{
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(2, 2);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   // this will translate by 2 units in x and 4 units in y due to the scale

   vp.translateViewWindow(-1, -1);
   ::std::cerr << "ViewPort " << vp << ::std::endl;
   assertDouble(vp.bounds().x(), 1);
   assertDouble(vp.bounds().y(), 3);

   assert(orig.horizontalScale() == vp.horizontalScale());
   assert(orig.verticalScale() == vp.verticalScale());
}

void utest_rotateBounds()
{
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(2, 2);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   // this will translate by 2 units in x and 4 units in y due to the scale

   vp.rotateBounds(-M_PI, Point(0, 0));
   ::std::cerr << "ViewPort " << vp << ::std::endl;
   assertDouble(vp.bounds().x(), 1);
   assertDouble(vp.bounds().y(), 1);
   assertDouble(vp.bounds().rotation(), 0);
   assertDouble(vp.bounds().width(), orig.bounds().width());
   assertDouble(vp.bounds().height(), orig.bounds().height());

   assert(orig.horizontalScale() == vp.horizontalScale());
   assert(orig.verticalScale() == vp.verticalScale());
}

void utest_scaleBounds()
{
   // choose a scale of 1:1
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(2, 2);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   // this will translate by 2 units in x and 4 units in y due to the scale

   vp.scaleBounds(0.5, 0.25, Point(0, 0));
   ::std::cerr << "ViewPort " << vp << ::std::endl;
   assertDouble(vp.bounds().x(), -0.5);
   assertDouble(vp.bounds().y(), -0.25);
   assertDouble(vp.bounds().rotation(), M_PI);
   assertDouble(vp.bounds().width(), 2);
   assertDouble(vp.bounds().height(), 2);
   assertDouble(vp.horizontalScale(), 1);
   assertDouble(vp.verticalScale(), 1);
}

void utest_resizeBoundsToScale()
{
   // choose a scale of 1:1
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(2, 2);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   // scale the size of the viewport to achieve the specified scale value
   // the scaling is done around the specified point.
   vp.resizeBoundsToScale(1, 1, Point(0, 0));
   ::std::cerr << "ViewPort " << vp << ::std::endl;
   assertDouble(vp.bounds().x(), -0.5);
   assertDouble(vp.bounds().y(), -0.25);
   assertDouble(vp.bounds().rotation(), M_PI);
   assertDouble(vp.bounds().width(), 2);
   assertDouble(vp.bounds().height(), 2);
   assertDouble(vp.horizontalScale(), 1);
   assertDouble(vp.verticalScale(), 1);
}

void utest_movePoint2Pixel()
{
   // choose a scale of 1:1
   Bounds b(-1, -1, 4, 8, M_PI);
   ViewPort::Window win(2, 2);
   MyViewPort vp(b, win);
   const ViewPort orig(vp);

   vp.movePointToPixel(Point(0, 0), vp.centerPixel());
   ::std::cerr << "Center " << vp.centerPoint() << ::std::endl;
   ::std::cerr << "ViewPort " << vp << ::std::endl;
   assertDouble(vp.bounds().x(), 2);
   assertDouble(vp.bounds().y(), 4);
   assertDouble(vp.bounds().rotation(), M_PI);
   assertDouble(vp.bounds().width(), 4);
   assertDouble(vp.bounds().height(), 8);
   assertDouble(vp.horizontalScale(), 0.5);
   assertDouble(vp.verticalScale(), 0.25);
}
