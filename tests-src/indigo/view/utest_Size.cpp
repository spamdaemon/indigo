#include <indigo/view/Size.h>

using namespace ::indigo::view;

void utest_Size()
{
   try {
      Size(-1, 1);
      assert("Invalid size not recognized" == nullptr);
   }
   catch (...) {
   }
   try {
      Size(1, -1);
      assert("Invalid size not recognized" == nullptr);
   }
   catch (...) {
   }
}

