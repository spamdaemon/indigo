#include <indigo/view/Bounds.h>
#include <iostream>
#include <cmath>

using namespace ::std;
using ::indigo::view::Bounds;

// large and small double such that SMALL_DOUBLE + LARGE_DOUBLE == LARGE_DOUBLE
static double SMALL_DOUBLE = 1.0 / (1 << 30);
static double LARGE_DOUBLE = 1 << 30;

static void assertDouble(double value, double expected, double eps = 1E-8)
{
   assert(::std::abs(value - expected) <= eps);
}
static void assertBounds(const Bounds& b, double x, double y, double w, double h, double phi, double eps = 1E-8)
{
   assertDouble(b.x(), x, eps);
   assertDouble(b.y(), y, eps);
   assertDouble(b.width(), w, eps);
   assertDouble(b.height(), h, eps);
   assertDouble(b.rotation(), phi, eps);
}

void utest_DefaultConstructor()
{
   Bounds b;
   assertBounds(b, 0, 0, 1, 1, 0);
}

void utest_DirectConstructorsSuccess()
{
   {
      Bounds b(-1, 2, 3, -4, 0);
      assertBounds(b, -1, -2, 3, 4, 0);
   }
   {
      Bounds b(-1, 2, 3, -4, M_PI / 3);
      assertBounds(b, -1, -2, 3, 4, M_PI / 3);
   }
}

void utest_DirectConstructorsInvalidBounds()
{
   try {
      Bounds b(-LARGE_DOUBLE, 2, SMALL_DOUBLE, -4, 0);
      assert(false);
   }
   catch (const invalid_argument&) {
   }
   try {
      Bounds b(-1, 2.0 * LARGE_DOUBLE, 3, -4.0 * SMALL_DOUBLE, M_PI / 3);
      assert(false);
   }
   catch (const invalid_argument&) {
   }
}

void utest_SetBounds()
{
   Bounds b;
   b.setBounds(-1, 2, 3, -4, 0);
   assertBounds(b, -1, -2, 3, 4, 0);
   b.setBounds(-1, 2, 3, -4, M_PI / 3);
   assertBounds(b, -1, -2, 3, 4, M_PI / 3);

   try {
      b.setBounds(-LARGE_DOUBLE, 2, SMALL_DOUBLE, -4, 0);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, -1, -2, 3, 4, M_PI / 3);
   }
   try {
      b.setBounds(-1, 2.0 * LARGE_DOUBLE, 3, -4.0 * SMALL_DOUBLE, M_PI / 3);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, -1, -2, 3, 4, M_PI / 3);
   }
}

void utest_MoveTo_Success()
{
   // Success
   Bounds b(1, 2, 3, 4, 5);
   b.moveTo(11, 22);
   assertBounds(b, 11, 22, 3, 4, 5);
}

void utest_MoveTo_Failure()
{
   Bounds b(0, 0, SMALL_DOUBLE, 1, 0);
   try {
      b.moveTo(LARGE_DOUBLE, 1);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, 0, 0, SMALL_DOUBLE, 1, 0);
   }
   b = Bounds(0, 0, 1, SMALL_DOUBLE, 0);
   try {
      b.moveTo(-1, LARGE_DOUBLE);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, 0, 0, 1, SMALL_DOUBLE, 0);
   }
}

void utest_SetRotation()
{
   Bounds b(0, 0, SMALL_DOUBLE, 1, M_PI);
   b.setRotation(M_PI / 2);
   assertBounds(b, 0, 0, SMALL_DOUBLE, 1, M_PI / 2);

   // We're not testing for invalid rotations yet
}

void utest_SetSize_Success()
{
   Bounds b(1, 2, 3, 4, 5);
   b.setSize(33, 44);
   assertBounds(b, 1, 2, 33, 44, 5);

}

void utest_SetSize_Failure()
{
   Bounds b(1, 2, 3, 4, 5);

   try {
      b.setSize(-1, 1);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, 1, 2, 3, 4, 5);
   }
   try {
      b.setSize(1, -.5);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, 1, 2, 3, 4, 5);
   }
   b = Bounds(LARGE_DOUBLE, 2, 3, 4, 5);
   try {
      b.setSize(SMALL_DOUBLE, 1);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, LARGE_DOUBLE, 2, 3, 4, 5);
   }
   b = Bounds(1, LARGE_DOUBLE, 3, 4, 5);
   try {
      b.setSize(1, SMALL_DOUBLE);
      assert(false);
   }
   catch (const invalid_argument&) {
      assertBounds(b, 1, LARGE_DOUBLE, 3, 4, 5);
   }
}

void utest_fromToBounds()
{
   {
      Bounds b(1, 2 + SMALL_DOUBLE, 3, 4, 5);
      double x, y;
      b.fromBounds(0, 0, x, y);
      assertDouble(x, 1, 0);
      assertDouble(y, 2 + SMALL_DOUBLE, 0);
      b.toBounds(x, y, x, y);
      assertDouble(x, 0, 0);
      assertDouble(y, 0, 0);
   }

   {
      Bounds b(1, 2 + SMALL_DOUBLE, 3, 4, M_PI);
      double x, y;
      b.fromBounds(3, 4, x, y);
      assertDouble(x, 1 - 3);
      assertDouble(y, 2 + SMALL_DOUBLE - 4);
      b.toBounds(x, y, x, y);
      assertDouble(x, 3);
      assertDouble(y, 4);
   }
}

void utest_GetCenter()
{
   Bounds b(1, 2, 3, 4, 0);
   double x, y;
   b.getCenter(x, y);
   assertDouble(x, 2.5);
   assertDouble(y, 4);
}

void utest_GetOppositePoint()
{
   Bounds b(1, 2, 3, 4, M_PI / 2);
   double x, y;
   b.getOppositePoint(x, y);
   assertDouble(x, -3);
   assertDouble(y, 5);
}

void utest_fitBounds_Success()
{
   {
      const Bounds W(-10, -10, 20, 20, M_PI / 4);
      const Bounds b(-5, 5, 2, 3, M_PI / 4);
      Bounds f = W.fitBounds(b);
      cerr << "F: " << f << ::std::endl;
      assertBounds(f, -5, 5, 2, 3, M_PI / 4);
   }

   {
      const Bounds W(-10, -10, 20, 20, M_PI / 4);
      const Bounds b(-11, -11, 2, 2, M_PI / 4);
      Bounds f = W.fitBounds(b);
      cerr << "F: " << f << ::std::endl;
      assertBounds(f, -10, -10, 2, 2, M_PI / 4);
   }

   // use a rotation of 0 for these tests, because it's much harder to
   // determine the corner
   {
      const Bounds W(-10, -10, 20, 20, 0);
      const Bounds b(9, 9, 2, 2, 0);
      Bounds f = W.fitBounds(b);
      cerr << "F: " << f << ::std::endl;
      assertBounds(f, 8, 8, 2, 2, 0);
   }
   {
      const Bounds W(-10, -10, 20, 20, 0);
      const Bounds b(9, 9, 40, 40, 0);
      Bounds f = W.fitBounds(b);
      cerr << "F: " << f << ::std::endl;
      assertBounds(f, -20, -20, 40, 40, 0);
   }
}

void utest_fitBounds_Failed()
{
   const Bounds W(-LARGE_DOUBLE, -10, 20, 20, 0);
   {
      const Bounds b(-1, -1, 1, 1, M_PI / 2);
      try {
         W.fitBounds(b);
         assert(false);
      }
      catch (const invalid_argument&) {
      }
   }

   {
      const Bounds b(-1, -1, SMALL_DOUBLE, 1, M_PI / 2);
      try {
         W.fitBounds(b);
         assert(false);
      }
      catch (const invalid_argument&) {
      }
   }

}

void utest_Bounds_bugfix()
{
    Bounds b;

    b.setBounds(10,10,-10,-10,0);
    assert(b.x()==0);
    assert(b.y()==0);
    assert(b.width()==10);
    assert(b.height()==10);
    assert(b.rotation()==0);
}