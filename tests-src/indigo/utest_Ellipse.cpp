#include <indigo/Ellipse.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace indigo;

void utest_1()
{
  Point pt(0,0);
  Ellipse e = Ellipse::createCovarianceEllipse(pt,1,1,0);
  cerr << "Ellipse: " << e.majorAxis() << ", " << e.minorAxis() << ", " << 180.0*e.theta()/M_PI << endl;
  assert(::std::abs(e.majorAxis()-1.0) < 0.0001);
  assert(::std::abs(e.minorAxis()-1.0) < 0.0001);
  
  e = Ellipse::createCovarianceEllipse(pt,4,4,0);
  cerr << "Ellipse: " << e.majorAxis() << ", " << e.minorAxis() << ", " << 180.0*e.theta()/M_PI << endl;
  assert(e.majorAxis()==e.minorAxis());

  e = Ellipse::createCovarianceEllipse(pt,4,9,0);
  cerr << "Ellipse: " << e.majorAxis() << ", " << e.minorAxis() << ", " << 180.0*e.theta()/M_PI << endl;
  assert(::std::abs(e.majorAxis()-3)<0.0001);
  assert(::std::abs(e.minorAxis()-2)<0.0001);
  assert(::std::abs(e.theta()-M_PI/2)<0.0001);
}

void utest_2()
{
  Point pt(0,0);

  Ellipse e = Ellipse::createCovarianceEllipse(pt,25,16,9);
  cerr << "Ellipse: " << e.majorAxis() << ", " << e.minorAxis() << ", " << 180.0*e.theta()/M_PI << endl;
  
}

void utest_3()
{
  for (double angle=0;angle<2*M_PI;angle += M_PI/6) {
    double r00 = ::std::cos(angle);
    double r10 = ::std::sin(angle);
    double r01 = -r10;
    double r11 = r00;
    
    double p00 = 6;
    double p01 = 0;
    double p10 = 0;
    double p11 = 3;
    
    double x00 = r00*p00 + r01*p10;
    double x10 = r10*p00 + r11*p10;
    double x01 = r00*p01 + r01*p11;
    double x11 = r10*p01 + r11*p11;
    
    double t00 = x00;
    double t01 = x10;
    double t10 = x01;
    double t11 = x11;
    
    double q00 = x00*t00 + x01*t10;
    double q10 = x10*t00 + x11*t10;
    double q01 = x00*t01 + x01*t11;
    double q11 = x10*t01 + x11*t11;
    
    Point pt(0,0);
    
    Ellipse e = Ellipse::createCovarianceEllipse(pt,q00,q11,(q01+q10)/2);
    
    cerr << (180*angle/M_PI) << " Ellipse: " << e.majorAxis() << ", " << e.minorAxis() << ", " << 180.0*e.theta()/M_PI << endl;
    assert(::std::abs(e.majorAxis()-6)<0.0001);
    assert(::std::abs(e.minorAxis()-3)<0.0001);
  }
}

